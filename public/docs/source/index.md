---
title: API Reference

language_tabs:
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
[Get Postman Collection](http://chip-metrics.test/docs/collection.json)

<!-- END_INFO -->

#general
<!-- START_045584ae54e28ecbf610b9834e143986 -->
## This endpoint returns current time for given request. Sample response:

> Example request:

```bash
curl -X GET "http://chip-metrics.test/api/v1/time" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://chip-metrics.test/api/v1/time",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "dateTimeRFC2822": "Wed, 19 Sep 2018 11:48:16 +0200",
    "data": {
        "year": "2018",
        "month": "09",
        "day": "19",
        "hour": "11",
        "minute": "12",
        "second": "13",
        "timezone": "Europe\/Warsaw"
    },
    "time": 1537350496
}
```

### HTTP Request
`GET /api/v1/time`


<!-- END_045584ae54e28ecbf610b9834e143986 -->

<!-- START_6aaa00928a450accb4509abd638f2c3a -->
## /api/v1/user/register

> Example request:

```bash
curl -X POST "http://chip-metrics.test/api/v1/user/register" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://chip-metrics.test/api/v1/user/register",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST /api/v1/user/register`


<!-- END_6aaa00928a450accb4509abd638f2c3a -->

<!-- START_ccb7fb24e632470c4e44ddc9bff64e39 -->
## /api/v1/user/login

> Example request:

```bash
curl -X POST "http://chip-metrics.test/api/v1/user/login" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://chip-metrics.test/api/v1/user/login",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST /api/v1/user/login`


<!-- END_ccb7fb24e632470c4e44ddc9bff64e39 -->

<!-- START_04d3f75470e463e84c2f538a05db385f -->
## /api/v1/user/is-email-unique

> Example request:

```bash
curl -X POST "http://chip-metrics.test/api/v1/user/is-email-unique" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://chip-metrics.test/api/v1/user/is-email-unique",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST /api/v1/user/is-email-unique`


<!-- END_04d3f75470e463e84c2f538a05db385f -->

<!-- START_7e760eeb4eeb0dbc496e88e8eb15f234 -->
## /api/v1/user/check-token/{token}

> Example request:

```bash
curl -X GET "http://chip-metrics.test/api/v1/user/check-token/{token}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://chip-metrics.test/api/v1/user/check-token/{token}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET /api/v1/user/check-token/{token}`


<!-- END_7e760eeb4eeb0dbc496e88e8eb15f234 -->

<!-- START_802ea91cfc160f272277b687a4d5c414 -->
## /api/v1/user/refresh-token/{token}

> Example request:

```bash
curl -X GET "http://chip-metrics.test/api/v1/user/refresh-token/{token}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://chip-metrics.test/api/v1/user/refresh-token/{token}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET /api/v1/user/refresh-token/{token}`


<!-- END_802ea91cfc160f272277b687a4d5c414 -->

<!-- START_ad98b36073153b83155537d8beb0d55a -->
## /api/v1/user/logout

> Example request:

```bash
curl -X GET "http://chip-metrics.test/api/v1/user/logout" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://chip-metrics.test/api/v1/user/logout",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET /api/v1/user/logout`


<!-- END_ad98b36073153b83155537d8beb0d55a -->

<!-- START_a2d676c49f69013e404b7fae51cee9d8 -->
## Add one record to the database. Can&#039;t use Request validation here because the response may be to lenghty for the microcontroller.

> Example request:

```bash
curl -X POST "http://chip-metrics.test/api/v1/user/measurement/power" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://chip-metrics.test/api/v1/user/measurement/power",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST /api/v1/user/measurement/power`


<!-- END_a2d676c49f69013e404b7fae51cee9d8 -->

<!-- START_8ce5f31a595de5f2e4c603cd7dc9fd63 -->
## /api/v1/user/measurement/power-archive

> Example request:

```bash
curl -X POST "http://chip-metrics.test/api/v1/user/measurement/power-archive" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://chip-metrics.test/api/v1/user/measurement/power-archive",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST /api/v1/user/measurement/power-archive`


<!-- END_8ce5f31a595de5f2e4c603cd7dc9fd63 -->

