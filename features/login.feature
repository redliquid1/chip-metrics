Feature: [API] Testing login and logout.
    Test

    Scenario: Login success
        Given the following form parameters are set:
            | name | value |
            | email | redliquid1@gmail.com |
            | password | admin |
        When I request "/api/v1/user/login" using HTTP "POST"
        And The response code is 200
        And The response body contains JSON:
        """
        {
             "token": "@variableType(string)",
             "user": "@variableType(object)"
        }
        """

    Scenario: Login fail
        Given the following form parameters are set:
            | name | value |
            | email | redliquid1@gmail.com |
            | password | admin123 |
        When I request "/api/v1/user/login" using HTTP "POST"
        Then The response code is 401
        And The response body contains JSON:
        """
        {
             "error": "@variableType(string)",
             "code": "@variableType(int)"
        }
        """

    Scenario: Logout
        Given I am logged in as "redliquid1@gmail.com"
        When I request "/api/v1/user/logout" using HTTP "GET"
        Then print response
        And The response code is 200
        And The response body contains JSON:
        """
        {
             "ok": "@variableType(int)"
        }
        """

