<?php

use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Tests\TestCase;
use Behat\Behat\Tester\Exception\PendingException;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Behat\Behat\Hook\Scope\BeforeScenarioScope;
use Behat\Behat\Hook\Scope\AfterScenarioScope;
use Illuminate\Contracts\Console\Kernel;

/**
 * Defines application features from the specific context.
 */
class ApiExtensionContext extends \Imbo\BehatApiExtension\Context\ApiContext implements Context
{
    use \Laracasts\Behat\Context\Migrator;

    protected $userRepository;
    protected $jwtAuth;

    protected $token;

    public function __construct()
    {
        $this->userRepository = app(\App\Repositories\Eloquent\UserRepository::class);
        $this->jwtAuth = app(\Tymon\JWTAuth\JWTAuth::class);
    }

    /**
     * @Given /^I am logged in as "([^"]*)"$/
     */
    public function iAmLoggedInAs($email)
    {
        // Destroy the previous session
        if (Session::isStarted()) {
            Session::regenerate(true);
        } else {
            Session::start();
        }

        $user = $this->userRepository->findByColumn('email', $email); //$this->userRepository->findByColumn('email', $email);
        Auth::login($user);

        // Save the session data to disk or to memcache
        Session::save();
        $this->token = $this->jwtAuth->fromUser($user);

        echo $this->token;
        $this->request->withHeader('Authorization', 'Bearer '.$this->token);
    }


    /**
     * Prints last response body.
     *
     * @Then print response
     */
    public function printResponse()
    {
        $request = $this->request;
        $response = $this->response;

        echo sprintf(
            "%s %s => %d:\n%s",
            $request->getMethod(),
            (string) ($request instanceof \Psr\Http\Message\RequestInterface ? $request->getUri() : $request->getUrl()),
            $response->getStatusCode(),
            (string) $response->getBody()
        );
    }
}
