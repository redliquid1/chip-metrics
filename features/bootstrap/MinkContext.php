<?php

use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Tests\TestCase;
use Behat\Behat\Tester\Exception\PendingException;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Behat\Behat\Hook\Scope\BeforeScenarioScope;
use Behat\Behat\Hook\Scope\AfterScenarioScope;
use Illuminate\Contracts\Console\Kernel;

/**
 * Defines application features from the specific context.
 */
class MinkContext extends \Behat\MinkExtension\Context\MinkContext implements Context
{
    use \Laracasts\Behat\Context\Migrator;

    protected $userRepository;
    protected $jwtAuth;

    protected $token;

    public function __construct()
    {
        $this->userRepository = app(\App\Repositories\Eloquent\UserRepository::class);
        $this->jwtAuth = app(\Tymon\JWTAuth\JWTAuth::class);
    }


    /**
     * @Given /^I am logged in as "([^"]*)"$/
     */
    /*
    public function iAmLoggedInAs($email)
    {
        // Destroy the previous session
        if (Session::isStarted()) {
            Session::regenerate(true);
        } else {
            Session::start();
        }

        $user = $this->userRepository->findByColumn('email', $email); //$this->userRepository->findByColumn('email', $email);
        Auth::login($user);

        // Save the session data to disk or to memcache
        Session::save();
        $this->token = $this->jwtAuth->fromUser($user);

        // Get the session identifier for the cookie
        $encryptedSessionId = \Illuminate\Support\Facades\Crypt::encrypt(Session::getId());
        $cookieName = \Illuminate\Support\Facades\Session::getName();

        // Set the cookie
        $minkSession = $this->getSession();
        $minkSession->setCookie($cookieName, $encryptedSessionId);
        $minkSession->setRequestHeader('Authorization', 'Bearer '.$this->token);
    }
    */
}