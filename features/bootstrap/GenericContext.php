<?php

use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Tests\TestCase;
use Behat\Behat\Tester\Exception\PendingException;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Behat\Behat\Hook\Scope\BeforeScenarioScope;
use Behat\Behat\Hook\Scope\AfterScenarioScope;
use Illuminate\Contracts\Console\Kernel;
use Illuminate\Support\Facades\DB as DB;

/**
 * Defines application features from the specific context.
 */
class GenericContext implements Context
{
    use \Laracasts\Behat\Context\Migrator;

    /**
     * @BeforeFeature
     */
    public static function before($scope)
    {
        putenv('APP_ENV=acceptance');

        $tableNames = Schema::getConnection()->getDoctrineSchemaManager()->listTableNames();

        foreach ($tableNames as $name) {
            //if you don't want to truncate migrations
            if ($name == 'migrations') {
                continue;
            }
            DB::table($name)->delete();
        }

        Artisan::call('migrate');

        try {
            Artisan::call('db:seed');
        } catch (Exception $e) {
            dump('seeder failed');
        }
    }

    /**
     * @AfterFeature
     */
    public static function after($scope)
    {
        putenv('APP_ENV=acceptance');
        Artisan::call('migrate:rollback');

        $tableNames = Schema::getConnection()->getDoctrineSchemaManager()->listTableNames();

        foreach ($tableNames as $name) {
            //if you don't want to truncate migrations
            if ($name == 'migrations') {
                continue;
            }
            DB::table($name)->delete();
            //Schema::dropIfExists($name);
        }
    }

}