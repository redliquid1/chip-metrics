let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.disableNotifications();
mix.webpackConfig({
    devtool: 'source-map'
}).sourceMaps();

mix.sass('resources/assets/shared/scss/app.scss', 'public/modules/shared/css')
   .copy('resources/assets/shared/fonts', 'public/modules/shared/fonts')
   .copy('resources/assets/shared/images', 'public/modules/shared/images')
   .options({processCssUrls: false});

mix.js('resources/assets/app/main.js', 'public/modules/app/js');
mix.js('resources/assets/site/js/app.js', 'public/modules/site/js');

// copy the tinymce package.
mix.copy('node_modules/tinymce', 'public/modules/shared/js/tinymce');


/*
mix.sass('resources/assets/shared/scss/app.scss', 'public/shared/css')
   .copy('resources/assets/shared/fonts', 'public/shared/fonts')
   .copy('resources/assets/shared/images', 'public/shared/images')
   .options({processCssUrls: false});
*/

// mix.js('resources/assets/site/js/app.js', 'public/site/js').sass('resources/assets/site/sass/app.scss', 'public/site/css');
