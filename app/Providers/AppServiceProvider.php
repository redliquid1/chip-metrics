<?php

namespace App\Providers;

use App\Models\i18nKey;
use App\Models\i18nModule;
use App\Models\i18nTranslation;
use App\Observers\i18n\Key;
use App\Observers\i18n\Translation;
use App\Observers\i18n\Module;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        i18nKey::observe(Key::class);
        i18nTranslation::observe(Translation::class);
        i18nModule::observe(Module::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
