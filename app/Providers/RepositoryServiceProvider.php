<?php

namespace App\Providers;

use App\Repositories\Interfaces\i18n\i18nModuleInterface;
use App\Repositories\Interfaces\i18n\i18nTranslationInterface;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(
            \App\Repositories\Interfaces\UserInterface::class,
            \App\Repositories\Eloquent\UserRepository::class
        );

        $this->app->bind(
            \App\Repositories\Interfaces\DeviceInterface::class,
            \App\Repositories\Eloquent\DeviceRepository::class
        );

        $this->app->bind(
            \App\Repositories\Interfaces\DeviceGpioNamesInterface::class,
            \App\Repositories\Eloquent\DeviceGpioNamesRepository::class
        );

        $this->app->bind(
            \App\Repositories\Interfaces\Measurements\PowerInterface::class,
            \App\Repositories\Eloquent\Measurement\PowerRepository::class
        );

        $this->app->bind(
            \App\Repositories\Interfaces\Measurements\TemperatureInterface::class,
            \App\Repositories\Eloquent\Measurement\TemperatureRepository::class
        );

        $this->app->bind(
            \App\Repositories\Interfaces\ChartInterface::class,
            \App\Repositories\Eloquent\ChartRepository::class
        );

        $this->app->bind(
            \App\Repositories\Interfaces\DeviceNamesInterface::class,
            \App\Repositories\Eloquent\DeviceNamesRepository::class
        );

        $this->app->bind(
            \App\Repositories\Interfaces\FaqInterface::class,
            \App\Repositories\Eloquent\FaqRepository::class
        );

        $this->app->bind(
            \App\Repositories\Interfaces\StaticSiteInterface::class,
            \App\Repositories\Eloquent\StaticSiteRepository::class
        );

        $this->app->bind(
            \App\Repositories\Interfaces\RecordGalleryInterface::class,
            \App\Repositories\Eloquent\RecordGalleryRepository::class
        );

        $this->app->bind(
            \App\Repositories\Interfaces\ProductInterface::class,
            \App\Repositories\Eloquent\ProductRepository::class
        );

        $this->app->bind(
            \App\Repositories\Interfaces\MeasurementTypesInterface::class,
            \App\Repositories\Eloquent\MeasurementTypesRepository::class
        );

        $this->app->bind(
            \App\Repositories\Interfaces\i18n\i18nTranslationInterface::class,
            \App\Repositories\Eloquent\i18n\i18nTranslationRepository::class
        );

        $this->app->bind(
            \App\Repositories\Interfaces\i18n\i18nModuleInterface::class,
            \App\Repositories\Eloquent\i18n\i18nModuleRepository::class
        );

        $this->app->bind(
            \App\Repositories\Interfaces\i18n\i18nLanguageInterface::class,
            \App\Repositories\Eloquent\i18n\i18nLanguageRepository::class
        );

        $this->app->bind(
            \App\Repositories\Interfaces\i18n\i18nKeysInterface::class,
            \App\Repositories\Eloquent\i18n\i18nKeysRepository::class
        );
    }
}
