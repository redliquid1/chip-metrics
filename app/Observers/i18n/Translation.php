<?php

namespace App\Observers\i18n;

use App\Models\i18nTranslation;
use App\Services\i18n\i18nTranslationService;

class Translation
{
    public function saved(i18nTranslation $translation)
    {
        (new i18nTranslationService())->rebuildCache();
    }
}