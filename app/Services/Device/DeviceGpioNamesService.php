<?php

namespace App\Services\Device;

use App\Repositories\Eloquent\DeviceGpioNamesRepository;
use App\Repositories\Interfaces\DeviceGpioNamesInterface;
use App\Repositories\Interfaces\DeviceInterface;
use App\Services\AbstractService;

class DeviceGpioNamesService extends AbstractService
{
    public function make($model)
    {
        $this->model = $model;
    }

    /**
     * Get all user GPIOs based on type.
     *
     * @param int $userId
     * @param int $measurementTypeId
     * @return \Illuminate\Support\Collection
     */
    public function getAllUserGpios($userId, $measurementTypeId)
    {
        /** @var DeviceRepository $repository */
        $repository = app(DeviceInterface::class);
        $devices = $repository->getUserDevicesByMeasurementTypeId($userId, $measurementTypeId)->get();

        if ($devices) {
            /** @var DeviceGpioNamesRepository $repository */
            $repository = app(DeviceGpioNamesInterface::class);
            $deviceIds = $devices->pluck('id')->toArray();

            return $repository->whereIn('device_id', $deviceIds)->orderBy('id')
                ->with(['device'])
                ->all();
        }

        return collect([]);
    }
}