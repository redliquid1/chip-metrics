<?php

namespace App\Services\Device;

use App\Repositories\Eloquent\DeviceGpioNamesRepository;
use App\Repositories\Eloquent\DeviceRepository;
use App\Repositories\Eloquent\Measurement\PowerRepository;
use App\Repositories\Eloquent\Measurement\TemperatureRepository;
use App\Repositories\Interfaces\DeviceGpioNamesInterface;
use App\Repositories\Interfaces\DeviceInterface;
use App\Repositories\Interfaces\Measurements\PowerInterface;
use App\Services\AbstractService;
use App\Services\Measurement\PowerService;
use Illuminate\Database\Eloquent\Collection;

class DeviceService extends AbstractService
{
    const DEVICE_OK = 'ok';
    const DEVICE_NOT_FOUND = 'not-found';
    const DEVICE_ALREADY_ASSIGNED = 'already-assigned';
    const DEVICE_SELF = 'self';

    protected $defaultColors = [
        '#cc0001', '#e36101', '#ffcc00', '#009900', '#0066cb', '#000000'
    ];

    public function make($model)
    {
        $this->model = $model;
        return $this;
    }

    /**
     * Save data.
     *
     * @param array $params
     * @return mixed
     */
    public function save($params)
    {
        $this->model->fill($params);
        if (!$this->model->id && !isset($params['user_id'])) {
            $this->model->user_id = null;
            $this->model->name = $this->model->chip_id;
        }

        $result = $this->model->save();
        $this->getGpios(); // fill gpios.

        return $result;
    }
    /**
     * Get device name.
     *
     * @return string
     */
    public function getDeviceName()
    {
        return $this->model->deviceName()->name;
    }

    /**
     * Check if device is available for current logged user.
     *
     * @param string $chipId
     * @return int
     */
    public function isDeviceAvailable($chipId)
    {
        /** @var DeviceRepository */
        $repository = app(DeviceInterface::class);
        $device = $repository->where('chip_id', $chipId)->first();

        if (!$device) return self::DEVICE_NOT_FOUND;
        if (!$device->user_id) return self::DEVICE_OK;
        if ($device->user_id == $this->guard->user()->id) return self::DEVICE_SELF;
        return self::DEVICE_ALREADY_ASSIGNED;
    }

    /**
     * Add device.
     *
     * @param string $chipId
     * @param int $userId
     * @return bool
     */
    public function addDevice($chipId, $userId)
    {
        $repository = app(DeviceInterface::class);
        $device = $repository->where('chip_id', $chipId)->whereNull('user_id')->first();
        if ($device) {
            $device->user_id = $userId;
            $device->save();

            $this->make($device);
            $this->_fillDefaultGpios();

            return true;
        }
        return false;
    }

    /**
     * Remove device.
     *
     * @return bool
     */
    public function remove()
    {
        if ($this->model) {
            $gpioNames = app(DeviceGpioNamesInterface::class);
            $gpioNames->where('device_id', $this->model->id)->getQuery()->delete();

            $powerInterface = app(PowerInterface::class);
            $powerInterface->where('device_id', $this->model->id)->getQuery()->delete();

            $this->model->user_id = null;
            $this->model->name = $this->model->chip_id;
            $this->model->last_measurement = null;
            $this->model->save();

            return true;
        }

        return false;
    }

    /**
     * Delete record from database.
     *
     * @return bool
     */
    public function delete()
    {
        if ($this->model) {
            $gpioNames = app(DeviceGpioNamesInterface::class);
            $gpioNames->where('device_id', $this->model->id)->getQuery()->delete();

            $powerInterface = app(PowerInterface::class);
            $powerInterface->where('device_id', $this->model->id)->getQuery()->delete();

            $this->model->delete();
            return true;
        }

        return false;
    }

    /**
     * @return Collection
     */
    public function getGpios()
    {
        if (!$this->model) {
            return [];
        }

        $gpios = $this->model->gpios()->get();
        if (!$gpios->count()) {
            $this->_fillDefaultGpios();
            $gpios = $this->model->gpios()->get();
        }
        return $gpios;
    }

    protected function _fillDefaultGpios()
    {
        /** @var DeviceGpioNamesRepository $repository */
        $repository = app(DeviceGpioNamesInterface::class);
        if ($this->model && $repository->findByColumn('device_id', $this->model->id)) {
            // no need to add gpios.
            return;
        }

        switch($this->model->deviceName->identifier)
        {
            case PowerRepository::DEVICE_NAME:
                foreach(range(1, 6) as $k => $number) {
                    $repository->create([
                        'device_id' => $this->model->id,
                        'gpio' => $number,
                        'name' => 'Input '.$number,
                        'ppu' => '1000',
                        'type' => 'power',
                        'tariff' => 'g11',
                        'color' => $this->defaultColors[$k]
                    ]);
                }
                break;
            case TemperatureRepository::DEVICE_NAME:
                $repository->create([
                    'device_id' => $this->model->id,
                    'gpio' => 1,
                    'name' => 'Input',
                    'ppu' => '0',
                    'type' => 'temperature',
                    'tariff' => '',
                    'color' => $this->defaultColors[0]
                ]);
                break;
        }
    }

    public function saveGpios($data)
    {
        $repository = app(DeviceGpioNamesInterface::class);
        foreach($data as $row) {
            $gpio = $repository->initQuery()->where('device_id', $this->model->id)->where('gpio', $row['gpio'])->first();
            if ($gpio) {
                $gpio->name = $row['name'];
                $gpio->color = $row['color'];
                $gpio->ppu = $row['ppu'];
                $gpio->type = $row['type'];
                $gpio->tariff = $row['tariff'];
                $gpio->price = str_replace([','], ['.'], $row['price']);
                $gpio->save();
            }
        }
        return true;
    }

    /**
     * Save data to the CSV file.
     *
     * @return array ['fileName']
     */
    public function exportData()
    {
        $out = [
            'fileName' => false
        ];

        switch($this->model->deviceName->identifier)
        {
            case PowerRepository::DEVICE_NAME:
                $out['fileName'] = app(PowerService::class)->make($this->model)->exportData();
                break;
        }

        return $out;
    }
}