<?php

namespace App\Services\Device;

use App\Models\DeviceName;
use App\Services\AbstractService;

class DeviceNamesService extends AbstractService
{
    public function make($model)
    {
        $this->model = $model;
        return $this;
    }

    /**
     * Save model.
     *
     * @param array $data
     * @return bool
     */
    public function save($data)
    {
        if (!$this->model) {
            $this->model = new DeviceName();
        }

        unset($data['id']);

        $this->model->fill($data);
        $this->model->save();
        $this->model->measurementTypes()->sync($data['measurement_types_ids']);
        $this->model->refresh();

        return $this->model;
    }


    /**
     * Delete record.
     *
     * @return bool
     */
    public function delete()
    {
        if ($this->model) {
            $this->model->delete();
            return true;
        }
        return false;
    }
}