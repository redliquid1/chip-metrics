<?php

namespace App\Services\RecordGallery;

use App\Models\RecordGallery;
use App\Repositories\Eloquent\RecordGalleryRepository;
use App\Repositories\Interfaces\RecordGalleryInterface;
use App\Services\AbstractService;
use Illuminate\Support\Collection;
use Intervention\Image\ImageManagerStatic as Image;

class RecordGalleryService extends AbstractService
{
    /** @var array */
    protected $data;

    public function make($model)
    {
        $this->model = $model;
        return $this;
    }

    public function setRawData($data)
    {
        $this->data = $data;
        return $this;
    }

    public function saveRawData()
    {
        if (!count($this->data['gallery'])) {
            $this->resetRawGallery();
        }
        else {
            $currentGalleryIds = $this->getRawGallery()->pluck('id')->toArray();
            $path = $this->createDirectory();

            /** @var RecordGalleryRepository $recordGalleryRepository */
            $recordGalleryRepository = app(RecordGalleryInterface::class);

            foreach($this->data['gallery'] as $k => $datum) {
                if (!empty($datum['model_id'])) {
                    $currentGalleryIds = array_diff($currentGalleryIds, [$datum['id']]);
                    $row = $recordGalleryRepository->initQuery()->find($datum['id']);
                    $row->sort = $k + 1;
                    $row->save();
                    continue;
                }

                $targetFilename = $this->getTargetFileName($path, $datum['fileName']);

                $image = Image::make($datum['file']);
                $image->save($path.'/'.$targetFilename, 90);

                $recordGallery = new RecordGallery();
                $recordGallery->gallery_name = $this->data['gallery_name'];
                $recordGallery->model_name = $this->data['model_name'];
                $recordGallery->model_id = $this->data['model_id'];
                $recordGallery->file_name = $targetFilename;
                $recordGallery->title = '';
                $recordGallery->description = '';
                $recordGallery->data = '';
                $recordGallery->sort = $k + 1;
                $recordGallery->save();
            }

            /** @var RecordGalleryRepository $repository */
            $repository = app(RecordGalleryInterface::class);
            foreach($currentGalleryIds as $id) {
                $this->make($repository->initQuery()->find($id))->delete();
            }
        }

        $className = $this->data['model_name'];
        return $className::find($this->data['model_id']);
    }

    protected function getTargetFileName($path, $sourceFilename)
    {
        $t = $sourceFilename;
        $i = 0;
        do {
            $new = explode('.', $t);
            $ext = array_pop($new);
            $hash = md5(time().$i);

            $t = str_slug(implode('.', $new)).'-'.$hash.'.'.$ext;

            if (!is_file($path.'/'.$t)) {
                break;
            }

            $i++;
            if ($i > 10) {
                throw new \Exception('Something is wrong with new file name generator');
            }
        }
        while(true);

        return $t;
    }

    protected function createDirectory()
    {
        $path = public_path('uploads/'.$this->getModelNameSimple().'/'.$this->data['gallery_name']);
        @mkdir($path, 0755, true);
        $pathThumb = public_path('uploads/'.$this->getModelNameSimple().'/'.$this->data['gallery_name'].'/thumbnails');
        @mkdir($pathThumb, 0755, true);

        return $path;
    }

    public function getModelNameSimple()
    {
        if (!empty($this->model)) {
            return strtolower(str_replace('App\\Models\\', '', $this->model->model_name));
        }
        return strtolower(str_replace('App\\Models\\', '', $this->data['model_name']));
    }

    protected function resetRawGallery()
    {
        foreach($this->getRawGallery() as $row) {
            $this->make($row)->delete();
        }
    }

    public function resetGallery($name = false)
    {
        foreach($this->getGallery($name) as $row) {
            $this->make($row)->delete();
        }
    }

    /**
     * @return Collection
     */
    protected function getRawGallery()
    {
        $repository = app(RecordGalleryInterface::class);
        return $repository->where('gallery_name', $this->data['gallery_name'])
            ->where('model_name', $this->data['model_name'])
            ->where('model_id', $this->data['model_id'])
            ->get();
    }

    /**
     * @param string $name
     * @return Collection
     */
    public function getGallery($name = false)
    {
        $repository = app(RecordGalleryInterface::class);
        $query = $repository->where('model_name', $this->model->model_name)
            ->where('model_id', $this->model->model_id);

        if ($name) {
            $query->where('gallery_name', $name);
        }

        return $query->get();
    }

    public function getMiniature($width = false, $height = false)
    {
        if (!$width && !$height) {
            return null;
        }

        $thumbFile = $this->getThumbPath().'/'.$this->getThumbFile($width, $height);

        if (!is_file(public_path($thumbFile)) && is_file(public_path($this->getImagePath()))) {
            $image = Image::make(public_path($this->getImagePath()));
            if ($width && $height) {
                $image->fit($width, $height, function ($constraint) {
                    $constraint->upsize();
                });
            }
            else {
                if ($width && !$height) {
                    $image->widen($width, function ($constraint) {
                        $constraint->upsize();
                    });
                }
                else {
                    $image->heighten($height, function ($constraint) {
                        $constraint->upsize();
                    });
                }
            }
            $image->save(public_path($thumbFile));
            unset($image);
        }

        return $thumbFile;
    }

    public function getImagePath()
    {
        return 'uploads/'.$this->getModelNameSimple().'/'.$this->model->gallery_name.'/'.$this->model->file_name;
    }

    public function getThumbPath()
    {
        @mkdir(public_path('uploads/'.$this->getModelNameSimple().'/'.$this->model->gallery_name.'/thumbnails'), 0755, true);
        return 'uploads/'.$this->getModelNameSimple().'/'.$this->model->gallery_name.'/thumbnails';
    }

    protected function getThumbFile($width, $height)
    {
        return $this->model->id.'_'.(int)$width.'_'.(int)$height.'_'.$this->model->file_name;
    }

    public function delete()
    {
        // remove main file
        @unlink(public_path($this->getImagePath()));

        // remove thumbnails
        foreach(glob($this->getThumbPath().'/'.$this->model->id.'_*') as $fileName) {
            @unlink($fileName);
        }

        $this->model->delete();
    }
}