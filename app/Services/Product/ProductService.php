<?php

namespace App\Services\Product;

use App\Models\Chart;
use App\Models\Faq;
use App\Models\Product;
use App\Models\RecordGallery;
use App\Models\User;
use App\Services\AbstractService;
use App\Services\RecordGallery\RecordGalleryService;

class ProductService extends AbstractService
{
    public function make($model)
    {
        $this->model = $model;
        return $this;
    }

    /**
     * Save model.
     *
     * @param array $data
     * @return bool
     */
    public function save($data)
    {
        if (!$this->model) {
            $this->model = new Product();
        }

        unset($data['id'], $data['sort']);

        $this->model->fill($data);
        $this->model->save();
        $this->model->refresh();

        return $this->model;
    }

    /**
     * Delete record.
     *
     * @return bool
     */
    public function delete()
    {
        if ($this->model) {
            (new RecordGalleryService())->make($this->model)->resetGallery();
            $this->model->delete();
            return true;
        }
        return false;
    }
}