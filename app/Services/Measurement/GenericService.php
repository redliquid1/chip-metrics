<?php

namespace App\Services\Measurement;

use App\Models\Device;
use App\Models\MeasurePower;
use App\Repositories\Eloquent\DeviceRepository;
use App\Repositories\Interfaces\DeviceInterface;
use App\Repositories\Interfaces\Measurements\PowerInterface;
use App\Services\AbstractService;
use Carbon\Carbon;

class GenericService extends AbstractService
{
    const MIN_YEAR = 946684800; //2000-01-01T00:00:00+00:00

    /** @var DeviceRepository */
    protected $deviceRepository;

    public function __construct(DeviceInterface $deviceRepository)
    {
        $this->deviceRepository = $deviceRepository;
        parent::__construct();
    }

    public function make(Device $model)
    {
        $this->model = $model;
        return $this;
    }

    /**
     * Get time added.
     *
     * @param int $timestamp
     * @return Carbon
     */
    protected function _getTimeAdded($timestamp)
    {
        if ($timestamp < self::MIN_YEAR) {
            return Carbon::now()->addSeconds($timestamp);
        }

        return Carbon::createFromTimestamp($timestamp);
    }
}
