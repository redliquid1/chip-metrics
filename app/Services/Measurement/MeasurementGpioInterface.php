<?php

namespace App\Services\Measurement;

interface MeasurementGpioInterface
{
    public function getDefaultGpioNames();
    public function exportData();
}
