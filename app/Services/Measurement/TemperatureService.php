<?php

namespace App\Services\Measurement;

use App\Models\MeasureTemperature;
use App\Repositories\Interfaces\Measurements\TemperatureInterface;

class TemperatureService extends GenericService implements MeasurementGpioInterface
{
    /**
     * Add single record.
     *
     * @param array $data
     */
    public function add($data)
    {
        $device = $this->deviceRepository->findByColumn('chip_id', $data['device_id']);
        if ($device) {
            $measureTemperature = new MeasureTemperature();
            $measureTemperature->fill($data['data']);
            $measureTemperature->user_id = $device->user_id;
            $measureTemperature->device_id = $device->id;
            if ($data['timestamp']) {
                $measureTemperature->time_added = $this->_getTimeAdded($data['timestamp']);
            }
            $measureTemperature->save();

            $device->last_measurement = $measureTemperature->time_added;
            $device->save();
        }
    }

    /**
     * Add archive data.
     *
     * @param array $data
     */
    public function addArchive($data)
    {
        $device = $this->deviceRepository->findByColumn('chip_id', $data['device_id']);
        if ($device) {
            $rows = explode("\n", $data['data']);
            foreach ($rows as $row) {
                $row = trim($row);
                if (!$row) continue;

                $insert = [];
                $gpios = explode(',', $row);
                foreach ($gpios as $gpio) {
                    $pulses = explode('!', $gpio);
                    switch (strtolower($pulses[0])) {
                        case 'g1':
                            $insert['gpio1'] = $pulses[1];
                            break;
                        case 'ts':
                            if ($pulses[1]) {
                                $insert['time_added'] = $this->_getTimeAdded($pulses[1]);
                            }
                            break;
                    }
                }

                try {
                    $measureTemperature = new MeasureTemperature();
                    $measureTemperature->fill($insert);
                    $measureTemperature->user_id = $device->user_id;
                    $measureTemperature->device_id = $device->id;
                    $measureTemperature->save();
                }
                catch(\Exception $e) {

                }
            }
        }
    }

    /**
     * Get default GPIO names.
     *
     * @return array
     */
    public function getDefaultGpioNames()
    {
        return [
            ['gpio' => 'gpio1', 'name' => 'GPIO1'],
        ];
    }

    public function exportData()
    {
        if (!$this->model) {
            throw new \Exception('No model found.');
        }

        $gpios = $this->model->gpios;
        $measureTemperatureRepository = app(TemperatureInterface::class);

        $data = $measureTemperatureRepository->where('device_id', $this->model->id)->orderBy('time_added', 'asc')->get();
        $header = [
            'Time added'
        ];
        foreach($gpios as $gpio) {
            $header[] = $gpio->name;
        }

        $fileName = str_slug($this->model->name).'-'.date('Y-m-d-H-i').'.csv';

        $handle = fopen(storage_path($fileName), 'wb');
        fputcsv($handle, $header, ';');
        foreach($data as $row) {
            $csv = [
                $row->time_added ? $row->time_added->format('Y-m-d H:i:s') : '',
                $row->gpio1,
            ];
            fputcsv($handle, $csv, ';');
        }
        fclose($handle);

        return $fileName;
    }
}
