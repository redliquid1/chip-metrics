<?php

namespace App\Services\Measurement;

use App\Models\MeasurePower;
use App\Repositories\Interfaces\Measurements\PowerInterface;

class PowerService extends GenericService implements MeasurementGpioInterface
{
    /**
     * Add single record.
     *
     * @param array $data
     */
    public function add($data)
    {
        $device = $this->deviceRepository->findByColumn('chip_id', $data['device_id']);
        if ($device) {
            $measurePower = new MeasurePower();
            $measurePower->fill($data['data']);
            $measurePower->user_id = $device->user_id;
            $measurePower->device_id = $device->id;
            if ($data['timestamp']) {
                $measurePower->time_added = $this->_getTimeAdded($data['timestamp']);
            }
            $measurePower->save();

            $device->last_measurement = $measurePower->time_added;
            $device->save();
        }
    }

    /**
     * Add archive data.
     *
     * @param array $data
     */
    public function addArchive($data)
    {
        $device = $this->deviceRepository->findByColumn('chip_id', $data['device_id']);
        if ($device) {
            $rows = explode("\n", $data['data']);
            foreach ($rows as $row) {
                $row = trim($row);
                if (!$row) continue;

                $insert = [];
                $gpios = explode(',', $row);
                foreach ($gpios as $gpio) {
                    $pulses = explode('!', $gpio);
                    switch (strtolower($pulses[0])) {
                        case 'g1':
                            $insert['gpio1'] = $pulses[1];
                            break;
                        case 'g2':
                            $insert['gpio2'] = $pulses[1];
                            break;
                        case 'g3':
                            $insert['gpio3'] = $pulses[1];
                            break;
                        case 'g4':
                            $insert['gpio4'] = $pulses[1];
                            break;
                        case 'g5':
                            $insert['gpio5'] = $pulses[1];
                            break;
                        case 'g6':
                            $insert['gpio6'] = $pulses[1];
                            break;
                        case 'ts':
                            if ($pulses[1]) {
                                $insert['time_added'] = $this->_getTimeAdded($pulses[1]);
                            }
                            break;
                    }
                }

                try {
                    $measurePower = new MeasurePower();
                    $measurePower->fill($insert);
                    $measurePower->user_id = $device->user_id;
                    $measurePower->device_id = $device->id;
                    $measurePower->save();
                }
                catch(\Exception $e) {

                }
            }
        }
    }

    /**
     * Get default GPIO names.
     *
     * @return array
     */
    public function getDefaultGpioNames()
    {
        return [
            ['gpio' => 'gpio1', 'name' => 'GPIO1'],
            ['gpio' => 'gpio2', 'name' => 'GPIO2'],
            ['gpio' => 'gpio3', 'name' => 'GPIO3'],
            ['gpio' => 'gpio4', 'name' => 'GPIO4'],
            ['gpio' => 'gpio5', 'name' => 'GPIO5'],
            ['gpio' => 'gpio6', 'name' => 'GPIO6'],
        ];
    }

    public function exportData()
    {
        if (!$this->model) {
            throw new \Exception('No model found.');
        }

        $header = [
            'Time added'
        ];
        foreach($this->model->gpios as $gpio) {
            $header[] = $gpio->name;
        }

        $date = date('Y-m-d-H-i');

        $fileName = str_slug($this->model->name).'-'.$date.'.csv';
        $fileNameData = str_slug($this->model->name).'-'.$date.'-data.csv';

        $sqlOutfile = addslashes(storage_path($fileNameData));

        $query = 'SELECT time_added, gpio1, gpio2, gpio3, gpio4, gpio5, gpio6 INTO OUTFILE "'.$sqlOutfile.'" ';
        $query.= 'FIELDS TERMINATED BY \';\' OPTIONALLY ENCLOSED BY \'"\' ';
        $query.= 'LINES TERMINATED BY \'\n\' ';
        $query.= 'FROM measure_power WHERE device_id = '.$this->model->id.' ';
        $query.= 'ORDER BY time_added ASC';

        \DB::statement($query);

        $handle = fopen(storage_path($fileName), 'wb');
        fputcsv($handle, $header, ';');
        fputs($handle, file_get_contents(storage_path($fileNameData)));
        fclose($handle);

        @unlink(storage_path($fileNameData));

        return $fileName;
    }
}
