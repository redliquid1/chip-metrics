<?php

namespace App\Services\Time;

use GeoIp2\Database\Reader;

class TimeService
{
    /**
     * Get User IP from request.
     *
     * @return mixed
     */
    protected function getUserIP() {
        $client  = @$_SERVER['HTTP_CLIENT_IP'];
        $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
        $remote  = $_SERVER['REMOTE_ADDR'];

        if(filter_var($client, FILTER_VALIDATE_IP)) {
            $ip = $client;
        }
        elseif(filter_var($forward, FILTER_VALIDATE_IP)) {
            $ip = $forward;
        }
        else {
            $ip = $remote;
        }

        return $ip;
    }

    /**
     * Get time.
     *
     * @param array $data Request data.
     * @return array
     * @throws \GeoIp2\Exception\AddressNotFoundException
     * @throws \MaxMind\Db\Reader\InvalidDatabaseException
     */
    public function getTime($data)
    {
        if (!empty($data['ip'])) {
            $ip = $data['ip'];
            if (!filter_var($ip, FILTER_VALIDATE_IP)) {
                die(json_encode(['error' => 'Invalid IP.']));
            }
        }
        else {
            $ip = $this->getUserIP();
        }

        $timezone = date_default_timezone_get();
        if ($ip != '127.0.0.1') {
            $reader = new Reader(storage_path('geo_database/GeoLite2-City.mmdb'));
            $record = $reader->city($ip);
            try{
                $timezone = $record->location->timeZone;
                date_default_timezone_set($timezone);
            }
            catch(\Exception $e) {}
        }

        $dateTime = new \DateTime();
        $dateTimeString = $dateTime->format(\DateTime::RFC2822);

        $out = [
            'dateTimeRFC2822' => $dateTimeString,
            'data' => [
                'year' => $dateTime->format('Y'),
                'month' => $dateTime->format('m'),
                'day' => $dateTime->format('d'),
                'hour' => $dateTime->format('H'),
                'minute' => $dateTime->format('i'),
                'second' => $dateTime->format('s'),
                'timezone' => $timezone,
            ],
            'time' => $dateTime->getTimestamp()
        ];

        return $out;
    }
}