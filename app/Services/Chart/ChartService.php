<?php

namespace App\Services\Chart;

use App\Models\Chart;
use App\Models\User;
use App\Services\AbstractService;

class ChartService extends AbstractService
{
    public function make($model)
    {
        $this->model = $model;
        return $this;
    }

    /**
     * Save model.
     *
     * @param array $data
     * @return bool
     */
    public function save($data)
    {
        if (!$this->model) {
            $this->model = new Chart();
        }

        unset($data['id'], $data['user_id']);

        if ($data['selectedGpios']) {
            $data['selectedGpios'] = collect($data['selectedGpios'])->pluck('id')->toArray();
        }

        $row = [
            'name' => $data['name'],
            'data' => json_encode($data)
        ];

        if (!$this->model->user_id) {
            $this->model->user_id = $this->guard->user()->id;
        }

        $this->model->fill($row);
        $this->model->save();
        $this->model->refresh();

        return [
            'chart' => $this->model,
            'dashboard_item' => $this->addModelToDashboard()
        ];
    }

    protected function addModelToDashboard()
    {
        $user = $this->guard->user();

        if (!$user->dashboard) {
            $user->dashboard = '{"layout":[]}';
        }

        $data = json_decode($user->dashboard, true);
        $found = false;
        $maxY = 0;

        foreach($data['layout'] as $row) {
            if ($row['chartId'] == $this->model->id) {
                $found = true;
                break;
            }

            if ($row['y'] + $row['h'] > $maxY) {
                $maxY = $row['y'] + $row['h'];
            }
        }

        $row = false;
        if (!$found) {
            $y = count($data['layout']) > 0 ? $maxY + 1 : 0;
            $row = [
                'x' => 0,
                'y' => $y,
                'w' => 12,
                'h' => 3,
                'i' => (string)count($data['layout']),
                'chartId' => $this->model->id,
            ];
            $data['layout'][] = $row;
        }

        $user->dashboard = json_encode($data);
        $user->save();
        $user->refresh();

        return $row;
    }

    /**
     * Delete record.
     *
     * @return bool
     */
    public function delete()
    {
        if ($this->model) {
            $user = $this->guard->user();
            $dashboard = json_decode($user->dashboard, true);

            $found = false;
            foreach($dashboard['layout'] as $i => $row) {
                if ($row['chartId'] == $this->model->id) {
                    $found = true;
                    break;
                }
            }

            if ($found) {
                unset($dashboard['layout'][$i]);
            }

            $user->dashboard = json_encode($dashboard);
            $user->save();

            $this->model->delete();
            return true;
        }
        return false;
    }
}