<?php

namespace App\Services\i18n;

use App\Models\i18nTranslation;
use App\Repositories\Eloquent\i18n\i18nKeysRepository;
use App\Repositories\Eloquent\i18n\i18nTranslationRepository;
use App\Repositories\Interfaces\i18n\i18nKeysInterface;
use App\Repositories\Interfaces\i18n\i18nLanguageInterface;
use App\Repositories\Interfaces\i18n\i18nTranslationInterface;
use App\Services\AbstractService;
use App\Services\i18n\Adapters\Vuei18nAdapter;
use Cache;

class i18nTranslationService extends AbstractService
{
    const TRANSLATION_KEY = 'translation';

    public function make(i18nTranslation $model)
    {
        $this->model = $model;
        return $this;
    }

    /**
     * Save model.
     *
     * @param array $data
     * @return bool
     */
    public function save($data)
    {
        if (!$this->model) {
            $this->model = new i18nTranslation();
        }

        unset($data['id']);
        if (!$data['key'])

        $this->model->fill($data);
        $this->model->save();
        $this->model->refresh();

        return $this->model;
    }

    public function setTranslation($data)
    {
        $repositoryLanguages = app(i18nLanguageInterface::class);
        /** @var i18nTranslationRepository $repositoryTranslations */
        $repositoryTranslations = app(i18nTranslationInterface::class);
        /** @var i18nKeysRepository $repositoryKeys */
        $repositoryKeys = app(i18nKeysInterface::class);
        $key = $repositoryKeys->where('key', $data['key'])->where('module_id', $data['module_id'])->first();

        $language = $repositoryLanguages->where('locale', $data['locale'])->first();
        $translation = $repositoryTranslations->where('key_id', $key->id)->where('language_id', $language->id)->first();
        if (!$translation) {
            // get module by other key
            $translation = $repositoryTranslations->create([
                'language_id' => $language->id,
                'key_id' => $key->id,
                'translation' => $data['translation']
            ]);
            return $translation;
        }
        $translation->translation = $data['translation'];
        $translation->save();
        return $translation;
    }

    public function changeKey($data)
    {
        $repositoryKeys = app(i18nKeysInterface::class);
        $row = $repositoryKeys->getQuery()->where('key', $data['oldKey'])->where('module_id', $data['module_id'])->first();
        $row->key = $data['newKey'];
        $row->save();
    }

    public function getSiteTranslations($forceRebuild = false)
    {
        if ($forceRebuild || !Cache::store('file')->has(self::TRANSLATION_KEY)) {
            $this->rebuildCache();
        }

        return Cache::store('file')->get(self::TRANSLATION_KEY);
    }

    public function rebuildCache()
    {
        $repository = app(i18nTranslationInterface::class);
        Cache::store('file')->forget(self::TRANSLATION_KEY);
        Cache::store('file')->forever(self::TRANSLATION_KEY, (new Vuei18nAdapter($repository->all()))->get());
    }
}