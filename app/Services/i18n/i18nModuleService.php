<?php

namespace App\Services\i18n;

use App\Models\i18nModule;
use App\Repositories\Eloquent\i18n\i18nModuleRepository;
use App\Repositories\Interfaces\i18n\i18nModuleInterface;
use App\Services\AbstractService;

class i18nModuleService extends AbstractService
{
    public function make(i18nModule $model)
    {
        $this->model = $model;
        return $this;
    }

    /**
     * Save model.
     *
     * @param array $data
     * @return bool
     */
    public function save($data)
    {
        if (!$this->model) {
            $this->model = new i18nModule();
        }

        unset($data['id']);

        $this->model->fill($data);
        $this->model->save();
        $this->model->refresh();

        return $this->model;
    }

    public function saveRaw($data)
    {
        $idsToKeep = [];
        /** @var i18nModuleRepository $repository */
        $repository = app(i18nModuleInterface::class);

        foreach($data as $datum) {
            $repository->initQuery();
            $row = empty($datum['id']) ? $repository->create(['name' => $datum['name']]) : $repository->find($datum['id']);
            $row->name = $datum['name'];
            $row->save();

            $idsToKeep[] = $row->id;
        }

        if ($idsToKeep) {
            $repository->initQuery()->getQuery()->whereNotIn('id', $idsToKeep)->delete();
        }
        else {
            $repository->initQuery()->getQuery()->delete();
        }

        return $repository->initQuery()->all();
    }

    /**
     * Delete record.
     *
     * @return bool
     */
    public function delete()
    {
        if ($this->model) {
            $this->model->delete();
            return true;
        }
        return false;
    }
}
