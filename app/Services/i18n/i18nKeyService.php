<?php

namespace App\Services\i18n;

use App\Models\i18nKey;
use App\Services\AbstractService;

class i18nKeyService extends AbstractService
{
    public function make($model)
    {
        $this->model = $model;
        return $this;
    }

    /**
     * Save model.
     *
     * @param array $data
     * @return bool
     */
    public function save($data)
    {
        if (!$this->model) {
            $this->model = new i18nKey();
        }

        unset($data['id']);

        $this->model->fill($data);
        $this->model->save();
        $this->model->refresh();

        return $this->model;
    }
}