<?php

namespace App\Services\i18n\Adapters;

use App\Models\i18nLanguage;
use App\Models\i18nTranslation;
use App\Repositories\Eloquent\i18n\i18nLanguageRepository;
use App\Repositories\Eloquent\i18n\i18nTranslationRepository;
use App\Repositories\Interfaces\i18n\i18nKeysInterface;
use App\Repositories\Interfaces\i18n\i18nLanguageInterface;
use App\Repositories\Interfaces\i18n\i18nTranslationInterface;
use Illuminate\Database\Eloquent\Collection;

class Vuei18nAdapter
{
    /** @var array */
    protected $data;

    /** @var \Illuminate\Support\Collection */
    protected $languages;

    /** @var i18nLanguageRepository */
    protected $languagesRepository;

    /** @var i18nTranslationRepository */
    protected $translationRepository;

    /** @var array */
    protected $additionalQueryData;

    /** @var array */
    protected $translations;

    public function __construct($data)
    {
        $this->data = $data;

        /** @var i18nLanguageRepository $languagesRepository */
        $this->languagesRepository = app(i18nLanguageInterface::class);
        $this->languages = $this->languagesRepository->all();

        $this->translationRepository = app(i18nTranslationInterface::class);
        $this->translations = $this->translationRepository->all();
    }

    public function get()
    {
        $out = [];

        $this->languages->each(function(i18nLanguage $element) use (&$out) {
            $out[$element->id] = [];
        });

        $this->data->each(function(i18nTranslation $element) use (&$out) {
            array_set($out[$element->language_id], $element->key->module->name.'.'.$element->key->key, $element->translation);
        });

        $final = [];

        foreach($out as $languageId => $item) {
            $final[$this->languages->where('id', $languageId)->first()->locale] = $item;
        }

        unset($out);

        return $final;
    }
}