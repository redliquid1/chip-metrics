<?php

namespace App\Services\i18n\Adapters;

use App\Repositories\Eloquent\i18n\i18nKeysRepository;
use App\Repositories\Eloquent\i18n\i18nLanguageRepository;
use App\Repositories\Eloquent\i18n\i18nTranslationRepository;
use App\Repositories\Interfaces\i18n\i18nKeysInterface;
use App\Repositories\Interfaces\i18n\i18nLanguageInterface;
use App\Repositories\Interfaces\i18n\i18nTranslationInterface;

class DatatablesAdapter
{
    /** @var array */
    protected $data;

    /** @var \Illuminate\Support\Collection */
    protected $languages;

    /** @var i18nLanguageRepository */
    protected $languagesRepository;

    /** @var i18nTranslationRepository */
    protected $translationRepository;

    /** @var i18nKeysRepository */
    protected $keysRepository;

    /** @var array */
    protected $additionalQueryData;

    /** @var array */
    protected $translations;

    public function __construct($data)
    {
        $this->data = $data;

        /** @var i18nLanguageRepository $languagesRepository */
        $this->languagesRepository = app(i18nLanguageInterface::class);
        $this->languages = $this->languagesRepository->all();

        $this->translationRepository = app(i18nTranslationInterface::class);
        $this->keysRepository = app(i18nKeysInterface::class);

        $this->translations = $this->translationRepository->all();
    }

    public function get($additionalQueryData = [])
    {
        $this->additionalQueryData = $additionalQueryData;

        $out = $this->data;
        $out['data'] = $this->processRows();
        $out['recordsTotal'] = $this->processRecordsTotal();
        $out['recordsFiltered'] = $this->processRecordsFiltered();
        return $out;
    }

    protected function getQuery()
    {
        return $this->keysRepository->initQuery()->getQuery()->select('key');
    }

    protected function processRecordsTotal()
    {
        return $this->getQuery()->get()->count();
    }

    protected function processRecordsFiltered()
    {
        $query = $this->getQuery();
        if (!empty($this->additionalQueryData['module_id'])) {
            $query->where('module_id', $this->additionalQueryData['module_id']);
        }
        return $query->get()->count();
    }

    protected function processRows()
    {
        $data = [];

        $locales = $this->languages->pluck('locale', 'id')->toArray();

        foreach($this->data['data'] as $datum) {
            $d = $datum;
            unset($d['created_at'], $d['updated_at']);

            foreach($locales as $languageId => $locale) {
                $d[$locale] = $this->getTranslation($languageId, $datum['id']);
            }

            $data[] = $d;
        }

        foreach($data as &$datum) {
            foreach($locales as $locale) {
                if (!isset($datum[$locale])) {
                    $datum[$locale] = '';
                }
            }
        }

        return $data;
    }

    protected function getTranslation($languageId, $keyId)
    {
        foreach($this->translations as $translation) {
            if ($translation->language_id == $languageId && $translation->key_id == $keyId) {
                return $translation->translation;
            }
        }
        return '';
    }
}