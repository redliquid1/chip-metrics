<?php

namespace App\Services\User;

use App\Http\Requests\Request;
use App\Models\User;
use App\Repositories\Eloquent\ImageRepository;
use App\Repositories\Eloquent\Measurement\PowerRepository;
use App\Repositories\Eloquent\UserRepository;
use App\Repositories\Interfaces\DeviceInterface;
use App\Repositories\Interfaces\DeviceNamesInterface;
use App\Repositories\Interfaces\ImageInterface;
use App\Repositories\Interfaces\Measurements\PowerInterface;
use App\Repositories\Interfaces\UserInterface;
use App\Services\AbstractService;
use App\Services\Image\Image;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use JWTAuth;
use Auth;
use Intervention\Image\ImageManagerStatic as ImageManager;

class UserService extends AbstractService
{
    /**
     * @var UserRepository
     */
    protected $userRepository;

    public function __construct(UserInterface $userRepository)
    {
        $this->userRepository = $userRepository;

        parent::__construct();
    }

    /**
     * Make service usable by given user.
     *
     * @param User $user
     * @return $this
     */
    public function make($user)
    {
        $this->model = $user;
        return $this;
    }

    /**
     * Register user.
     *
     * @param array $data
     * @return User|null
     */
    public function register($data)
    {
        $data['password'] = \Hash::make($data['password']);
        $data['dashboard'] = '{"layout":[]}';
        $user = $this->userRepository->create($data);
        $user->assignRole('client');
        return $user;
    }

    /**
     * Login user.
     *
     * @param array $credentials
     * @return array
     */
    public function login($credentials)
    {
        $user = $this->userRepository->findByColumn('email', $credentials['email']);
        if (!$user) {
            return [
                'error' => 'User "'.$credentials['email'].'" not found.',
                'code' => 404
            ];
        }

        if (!$token = Auth::attempt($credentials)) {
            return [
                'error' => 'invalid_credentials',
                'code' => 401
            ];
        }

        $out = [
            'user' => $this->getLoggedUser(),
            'token' => JWTAuth::fromUser($this->getLoggedUser())
        ];

        \Session::put('authToken', $out['token']);

        return $out;
    }

    /**
     * Login User via simply uer model.
     *
     * @param User $user
     * @return array
     */
    public function loginUser(User $user)
    {
        Auth::login($user);

        $out = [
            'user' => $this->getLoggedUser(),
            'token' => JWTAuth::fromUser($user)
        ];

        \Session::put('authToken', $out['token']);

        return $out;
    }

    /**
     * Save data.
     *
     * @param array $data
     * @return array
     */
    public function save($data)
    {
        /** @var User $user */
        $user = $this->guard->user();
        if (!$user->hasRole('super-admin') && $user->id != $this->model->id) {
            unset($data['password'], $data['role']);
        }

        if (!empty($this->model)) {
            $user = $this->model;
        }
        else {
            $user = new User();
        }

        if (empty($data['dashboard'])) {
            $data['dashboard'] = '{"layout":[]}';
        }

        if (empty($data['password'])) {
            unset($data['password']);
        }

        $user->fill($data);
        if (!empty($data['password']) && $data['password'] === $data['repeatPassword']) {
            $user->password = \Hash::make($data['password']);
        }

        $user->save();

        if (!empty($data['role'])) {
            $user->removeRole('super-admin');
            $user->removeRole('client');
            $user->assignRole($data['role']);
        }

        return [
            'user' => $user,
            'token' => JWTAuth::fromUser($user)
        ];
    }

    /**
     * Logout user.
     */
    public function logout()
    {
        if (\Session::has('__logged_id')) {
            // user impersonated.
            $user = $this->userRepository->find(\Session::get('__logged_id'));
            \Session::forget('__logged_id');

            return $this->loginUser($user);
        }
        else {
            $this->guard->logout();
            \Session::forget('authToken');
        }
    }

    /**
     * Is exists?
     *
     * @param string $email
     * @return bool
     */
    public function isExists($email)
    {
        return (bool)$this->repository->findByColumn('email', $email);
    }

    /**
     * Save layout.
     *
     * @param array $layout
     * @return bool
     */
    public function saveLayout($layout)
    {
        $user = $this->guard->user();
        $dashboard = json_decode($user->dashboard, true);
        $dashboard['layout'] = $layout;
        $user->dashboard = json_encode($dashboard);
        $user->save();

        return $user->dashboard;
    }

    /**
     * Remove user.
     *
     * @param User $user
     * @return bool
     */
    public function delete()
    {
        $devices = $this->model->devices();
        foreach($devices as $device) {
            $device->delete();
        }

        /** @var PowerRepository $repository */
        $repository = app(PowerInterface::class);

        // Delete all measurement.
        $repository->getQuery()->where('user_id', $this->model->id)->delete();

        $this->model->delete();
        return true;
    }

    /**
     * Impersonate user.
     *
     * @return bool
     */
    public function impersonate()
    {
        \Session::put('__logged_id', $this->getLoggedUser()->id);

        return $this->loginUser($this->model);
    }

    /**
     * Get all user device names.
     *
     * @return \Illuminate\Support\Collection
     */
    public function allDeviceNames()
    {
        $user = $this->model;
        $deviceNamesIds = array_unique($user->devices()->orderBy('device_name_id')->pluck('device_name_id')->toArray());
        if ($deviceNamesIds) {
            return app(DeviceNamesInterface::class)->whereIn('id', $deviceNamesIds)->get();
        }
        return collect([]);
    }

    /**
     * Get all user device types.
     *
     * @return \Illuminate\Support\Collection
     */
    public function allDeviceTypes()
    {
        $user = $this->model;
        $deviceNamesIds = array_unique($user->devices()->orderBy('device_name_id')->pluck('device_name_id')->toArray());
        if ($deviceNamesIds) {
            return app(DeviceNamesInterface::class)->whereIn('id', $deviceNamesIds)->get();
        }
        return collect([]);
    }

    /**
     * Save avatar file.
     *
     * @param UploadedFile $file
     * @return bool
     */
    public function saveAvatar($file)
    {
        if (!$file->isValid()) return false;


        return true;
    }


    /**
     * Get logged user.
     *
     * @return User
     */
    protected function getLoggedUser()
    {
        return Auth::user();
    }
}
