<?php

namespace App\Services\Faq;

use App\Models\Chart;
use App\Models\Faq;
use App\Models\User;
use App\Services\AbstractService;

class FaqService extends AbstractService
{
    public function make($model)
    {
        $this->model = $model;
        return $this;
    }

    /**
     * Save model.
     *
     * @param array $data
     * @return bool
     */
    public function save($data)
    {
        if (!$this->model) {
            $this->model = new Faq();
        }

        unset($data['id'], $data['sort']);

        $this->model->fill($data);
        $this->model->save();
        $this->model->refresh();

        return $this->model;
    }

    /**
     * Delete record.
     *
     * @return bool
     */
    public function delete()
    {
        if ($this->model) {
            $this->model->delete();
            return true;
        }
        return false;
    }
}