<?php

namespace App\Services\StaticSite;

use App\Models\Chart;
use App\Models\Faq;
use App\Models\StaticSite;
use App\Models\User;
use App\Services\AbstractService;

class StaticSiteService extends AbstractService
{
    public function make($model)
    {
        $this->model = $model;
        return $this;
    }

    /**
     * Save model.
     *
     * @param array $data
     * @return bool
     */
    public function save($data)
    {
        if (!$this->model) {
            $this->model = new StaticSite();
        }

        unset($data['id']);

        $this->model->fill($data);
        $this->model->save();
        $this->model->refresh();

        return $this->model;
    }

    /**
     * Delete record.
     *
     * @return bool
     */
    public function delete()
    {
        if ($this->model) {
            $this->model->delete();
            return true;
        }
        return false;
    }
}