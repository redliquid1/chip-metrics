<?php

namespace App\Repositories\Eloquent;

use App\Models\User;
use App\Repositories\EloquentRepository;
use App\Repositories\Interfaces\UserInterface;
use Illuminate\Contracts\Auth\Guard;
use Yajra\DataTables\DataTables;

class UserRepository extends EloquentRepository implements UserInterface
{
    protected $dataTables;
    protected $guard;

    public function __construct(User $model, Guard $guard)
    {
        $this->model = $model;
        $this->guard = $guard;

        parent::__construct();
    }
}