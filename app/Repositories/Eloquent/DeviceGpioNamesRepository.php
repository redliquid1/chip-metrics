<?php

namespace App\Repositories\Eloquent;

use App\Models\DeviceGpioName;
use App\Repositories\Eloquent\Measurement\PowerRepository;
use App\Repositories\EloquentRepository;
use App\Repositories\Interfaces\DeviceInterface;
use App\Services\Measurement\PowerService;

class DeviceGpioNamesRepository extends EloquentRepository implements DeviceInterface
{
    public function __construct(DeviceGpioName $model)
    {
        $this->model = $model;
        parent::__construct();
    }

    /**
     * Get all user devices.
     *
     * @param int $userId
     * @param itn $deviceId
     * @return
     */
    public function getGpioNames($userId, $deviceId)
    {
        $data = $this->model->newQuery()->where('user_id', $userId)->where('device_id', $deviceId)->orderBy('name', 'asc')->get();
        if (!count($data)) {
            $this->setDefaultGpioNames($userId, $deviceId);
            $data = $this->getGpioNames($userId, $deviceId);
        }
        return $data;
    }

    /**
     * Set default GPIO names.
     *
     * @param int $userId
     * @param int $deviceId
     * @return void
     */
    public function setDefaultGpioNames($userId, $deviceId)
    {
        $deviceRepository = app(DeviceInterface::class);
        $device = $deviceRepository->find($deviceId);

        switch ($device->deviceName->identifier) {
            case PowerRepository::DEVICE_NAME:
                $rows = app(PowerService::class)->getDefaultGpioNames();
                break;
        }

        foreach ($rows as $row) {
            $row['user_id'] = $userId;
            $row['device_id'] = $deviceId;
            (new DeviceGpioName($row))->save();
        }
    }
}