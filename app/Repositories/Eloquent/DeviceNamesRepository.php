<?php

namespace App\Repositories\Eloquent;

use App\Models\DeviceName;
use App\Repositories\EloquentRepository;
use App\Repositories\Interfaces\DeviceNamesInterface;

class DeviceNamesRepository extends EloquentRepository implements DeviceNamesInterface
{
    public function __construct(DeviceName $model)
    {
        $this->model = $model;
        parent::__construct();
    }
}