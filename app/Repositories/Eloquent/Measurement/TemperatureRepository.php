<?php

namespace App\Repositories\Eloquent\Measurement;

use App\Models\MeasurePower;
use App\Models\MeasureTemperature;
use App\Repositories\Eloquent\DeviceRepository;
use App\Repositories\EloquentRepository;
use App\Repositories\Interfaces\DeviceInterface;
use App\Repositories\Interfaces\Measurements\PowerInterface;
use App\Repositories\Interfaces\Measurements\TemperatureInterface;
use App\Services\Device\DeviceService;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\JWTGuard;

class TemperatureRepository extends GenericRepository implements TemperatureInterface
{
    const DEVICE_NAME = 'temperature_meter';

    public function __construct(MeasureTemperature $model, Guard $guard, DeviceInterface $deviceRepository)
    {
        $this->guard = $guard;
        $this->deviceRepository = $deviceRepository;
        $this->model = $model;

        parent::__construct();
    }


    protected function getColumns()
    {
        return [
            \DB::raw('SUM(gpio1) as gpio1'),
        ];
    }
}