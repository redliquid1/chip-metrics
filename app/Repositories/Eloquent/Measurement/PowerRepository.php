<?php

namespace App\Repositories\Eloquent\Measurement;

use App\Models\MeasurePower;
use App\Repositories\Eloquent\DeviceRepository;
use App\Repositories\EloquentRepository;
use App\Repositories\Interfaces\DeviceInterface;
use App\Repositories\Interfaces\Measurements\PowerInterface;
use App\Services\Device\DeviceService;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\JWTGuard;

class PowerRepository extends GenericRepository implements PowerInterface
{
    const DEVICE_NAME = 'power_meter';

    public function __construct(MeasurePower $model, Guard $guard, DeviceInterface $deviceRepository)
    {
        $this->guard = $guard;
        $this->deviceRepository = $deviceRepository;
        $this->model = $model;

        parent::__construct();
    }

    // deviceIds=&chartPeriod=monthly&includeCurrentPeriod=1&periodTime=4&measurementTypeId=1
    public function getSampleMeasurements($params)
    {

    }



    protected function getColumns()
    {
        return [
            // 'device_id',
            \DB::raw('SUM(gpio1) as gpio1'),
            \DB::raw('SUM(gpio2) as gpio2'),
            \DB::raw('SUM(gpio3) as gpio3'),
            \DB::raw('SUM(gpio4) as gpio4'),
            \DB::raw('SUM(gpio5) as gpio5'),
            \DB::raw('SUM(gpio6) as gpio6')
        ];
    }
}