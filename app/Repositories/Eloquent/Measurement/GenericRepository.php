<?php

namespace App\Repositories\Eloquent\Measurement;

use App\Repositories\EloquentRepository;
use App\Repositories\Interfaces\DeviceInterface;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\Guard;

class GenericRepository extends EloquentRepository
{
    const TYPE_YEARLY = 'yearly';
    const TYPE_MONTHLY = 'monthly';
    const TYPE_WEEKLY = 'weekly';
    const TYPE_DAILY = 'daily';
    const TYPE_HOURLY = 'hourly';

    const MAX_PERIOD_YEARLY = 3;
    const MAX_PERIOD_MONTHLY = 12;
    const MAX_PERIOD_WEEKLY = 12;
    const MAX_PERIOD_DAILY = 30;
    const MAX_PERIOD_HOURLY = 48;

    /** @var Guard */
    protected $guard;

    /** @var DeviceInterface */
    protected $deviceRepository;

    /**
     * Get amount of unit used.
     *
     * @param array $params
     * @return array
     */
    public function getMeasurements($params) //$type, $deviceIds = [])
    {
        if (!$params['deviceIds']) {
            $params['deviceIds'] = $this->deviceRepository->getUserDevices($this->guard->user()->id, self::DEVICE_NAME)->pluck('id');
        }

        if (!$params['deviceIds']) {
            return collect([]);
        }

        $data = $this->getQueryData();

        $query = $this->getMeasurementsQuery();
        $query->whereIn('device_id', $params['deviceIds']);
        $this->limitQuery($query, $params);

        $binaryNumber = $this->getBinaryNumber($params['type']);
        $i = 0;
        $columns = $this->getColumns();
        $query->groupBy('device_id');
        while($binaryNumber)
        {
            $query->groupBy($data[$i]['group']);
            $columns[] = $data[$i]['column'];
            $i++;
            $binaryNumber >>= 1;
        }

        $columns[] = 'device_id';
        $query->select($columns);

        return $query->get();
    }

    protected function limitQuery(&$query, $params)
    {
        $minimum = Carbon::now(); // cut down results.
        $currentPeriod = Carbon::now();
        switch($params['type'])
        {
            case self::TYPE_YEARLY:
                $minimum->subYear(self::MAX_PERIOD_YEARLY);
                $query->where('time_added', '>=', $minimum->format('Y-01-01 00:00:00'));

                $currentPeriod->subYear($params['period']);
                $query->where('time_added', '>=', $currentPeriod->format('Y-01-01 00:00:00'));
                if (!$params['includeCurrentPeriod']) {
                    $year = date('Y') - 1;
                    $query->where('time_added', '<=', $year.'-12-31 23:59:59');
                }
                break;
            case self::TYPE_MONTHLY:
                $minimum->subMonth(self::MAX_PERIOD_MONTHLY);
                $query->where('time_added', '>=', $minimum->format('Y-m-01 00:00:00'));

                $currentPeriod->subMonth($params['period']);
                $query->where('time_added', '>=', $currentPeriod->format('Y-m-01 00:00:00'));
                if (!$params['includeCurrentPeriod']) {
                    $now = Carbon::now();
                    $query->where('time_added', '<', $now->format('Y-m-01 00:00:00'));
                }
                break;
            case self::TYPE_WEEKLY:
                $minimum->subWeek(self::MAX_PERIOD_WEEKLY)->startOfWeek();
                $query->where('time_added', '>=', $minimum->format('Y-m-d 00:00:00'));

                $startOfWeek = $currentPeriod->subWeek($params['period'])->startOfWeek();
                $query->where('time_added', '>', $startOfWeek->format('Y-m-d 00:00:00'));

                if (!$params['includeCurrentPeriod']) {
                    $date = Carbon::now()->subWeek()->endOfWeek();
                    $query->where('time_added', '<', $date->format('Y-m-d 23:59:59'));
                }
                break;
            case self::TYPE_DAILY:
                $minimum->subDays(self::MAX_PERIOD_DAILY);
                $query->where('time_added', '>=', $minimum->format('Y-m-d 00:00:00'));

                $day = $currentPeriod->subDays($params['period']);
                $query->where('time_added', '>=', $day->format('Y-m-d 00:00:00'));
                if (!$params['includeCurrentPeriod']) {
                    $day = Carbon::now()->subDay(1);
                    $query->where('time_added', '<=', $day->format('Y-m-d 23:59:59'));
                }
                break;
            case self::TYPE_HOURLY:
                $minimum->subHour(self::MAX_PERIOD_HOURLY);
                $query->where('time_added', '>=', $minimum->format('Y-m-d H:00:00'));

                $hours = $currentPeriod->subHours($params['period']);
                $query->where('time_added', '>=', $hours->format('Y-m-d H:00:00'));
                if (!$params['includeCurrentPeriod']) {
                    $day = Carbon::now()->subHour(1);
                    $query->where('time_added', '<=', $day->format('Y-m-d H:59:59'));
                }
                break;
        }
    }

    protected function getBinaryNumber($type)
    {
        switch($type)
        {
            case self::TYPE_YEARLY:
                return 0b00001;
            case self::TYPE_MONTHLY:
                return 0b00011;
            case self::TYPE_WEEKLY:
                return 0b00111;
            case self::TYPE_DAILY:
                return 0b01111;
            case self::TYPE_HOURLY:
                return 0b11111;
        }
    }

    protected function getQueryData()
    {
        return [
            [
                'column' => \DB::raw('YEAR(time_added) as year_added'),
                'group' => \DB::raw('YEAR(time_added)')
            ],
            [
                'column' => \DB::raw('MONTH(time_added) as month_added'),
                'group' => \DB::raw('MONTH(time_added)')
            ],
            [
                'column' => \DB::raw('WEEK(time_added) as week_added'),
                'group' => \DB::raw('WEEK(time_added)')
            ],
            [
                'column' => \DB::raw('DAY(time_added) as day_added'),
                'group' => \DB::raw('DAY(time_added)')
            ],
            [
                'column' => \DB::raw('HOUR(time_added) as hour_added'),
                'group' => \DB::raw('HOUR(time_added)')
            ],
        ];
    }

    protected function getColumns()
    {
        return [
            // 'device_id',
            \DB::raw('SUM(gpio1) as gpio1'),
            \DB::raw('SUM(gpio2) as gpio2'),
            \DB::raw('SUM(gpio3) as gpio3'),
            \DB::raw('SUM(gpio4) as gpio4'),
            \DB::raw('SUM(gpio5) as gpio5'),
            \DB::raw('SUM(gpio6) as gpio6')
        ];
    }

    /**
     * Group data by type.
     *
     * @param $query
     * @param $type
     *
     */
    protected function groupByType($query, $type)
    {
        switch($type)
        {
            case self::TYPE_HOURLY:
                $query->groupBy('DATE(time_added)');
                $query->groupBy('HOUR(time_added)');
                break;
        }
    }

    protected function getMeasurementsQuery()
    {
        $query = $this->model->newQuery();
        $query->select($this->getColumns());
        return $query;
    }
}