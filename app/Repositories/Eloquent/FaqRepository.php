<?php

namespace App\Repositories\Eloquent;

use App\Models\Faq;
use App\Repositories\EloquentRepository;
use App\Repositories\Interfaces\FaqInterface;

class FaqRepository extends EloquentRepository implements FaqInterface
{
    public function __construct(Faq $model)
    {
        $this->model = $model;
        parent::__construct();
    }

    public function getFaqs()
    {
        return $this->orderBy('sort', 'asc')->where('language', 'pl')->get();
    }

    public function saveSort($sortIds)
    {
        foreach ($sortIds as $i => $id) {
            $this->scopeReset();
            $row = $this->find($id);
            $row->sort = $i + 1;
            $row->save();
        }
        return true;
    }
}