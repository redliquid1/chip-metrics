<?php

namespace App\Repositories\Eloquent;

use App\Models\Faq;
use App\Models\RecordGallery;
use App\Repositories\EloquentRepository;
use App\Repositories\Interfaces\FaqInterface;

class RecordGalleryRepository extends EloquentRepository implements FaqInterface
{
    public function __construct(RecordGallery $model)
    {
        $this->model = $model;
        parent::__construct();
    }
}