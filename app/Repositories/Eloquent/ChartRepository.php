<?php

namespace App\Repositories\Eloquent;

use App\Models\Chart;
use App\Repositories\EloquentRepository;
use App\Repositories\Interfaces\ChartInterface;
use App\Repositories\Interfaces\DeviceGpioNamesInterface;
use Illuminate\Database\Eloquent\Collection;

class ChartRepository extends EloquentRepository implements ChartInterface
{
    public function __construct(Chart $model)
    {
        $this->model = $model;
        parent::__construct();
    }

    /**
     * Get all user devices.
     *
     * @param int $userId
     * @return Collection
     */
    public function getUserCharts($userId)
    {
        $query = $this->model->where('user_id', $userId);
        $charts = $query->get();
        $this->_expandCharts($charts);

        return $charts;
    }

    /**
     * @param Collection $charts
     */
    protected function _expandCharts(&$charts)
    {
        /** @var DeviceGpioNamesRepository $deviceGpioNamesRepository */
        $deviceGpioNamesRepository = app(DeviceGpioNamesInterface::class);

        foreach($charts as &$chart)
        {
            $data = json_decode($chart->data, true);
            // $t2 = array_sum($t['selectedGpios']);
            if (array_sum($data['selectedGpios']) && $data['selectedGpios']) {
                $data['selectedGpios'] = $deviceGpioNamesRepository->initQuery()
                    ->whereIn('id', $data['selectedGpios'])
                    ->with('device')
                    ->get();

                $chart->data = json_encode($data);
            }
        }
    }
}