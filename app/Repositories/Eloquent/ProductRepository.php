<?php

namespace App\Repositories\Eloquent;

use App\Models\Faq;
use App\Models\Product;
use App\Repositories\EloquentRepository;
use App\Repositories\Interfaces\FaqInterface;
use App\Repositories\Interfaces\ProductInterface;

class ProductRepository extends EloquentRepository implements ProductInterface
{
    public function __construct(Product $model)
    {
        $this->model = $model;
        parent::__construct();
    }

    public function getProducts()
    {
        return $this->orderBy('sort', 'asc')->where('language', 'pl')->get();
    }

    public function saveSort($sortIds)
    {
        foreach ($sortIds as $i => $id) {
            $this->scopeReset();
            $row = $this->find($id);
            $row->sort = $i + 1;
            $row->save();
        }
        return true;
    }
}