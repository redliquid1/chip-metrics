<?php

namespace App\Repositories\Eloquent;

use App\Models\Device;
use App\Repositories\EloquentRepository;
use App\Repositories\Interfaces\DeviceInterface;

class DeviceRepository extends EloquentRepository implements DeviceInterface
{
    public function __construct(Device $model)
    {
        $this->model = $model;
        parent::__construct();
    }

    /**
     * Get all user devices.
     *
     * @param int $userId
     * @param string $type
     * @return
     */
    public function getUserDevices($userId, $type = null)
    {
        $query = $this->model->newQuery()->where('user_id', $userId);
        if ($type) {
            $query->whereHas('deviceName', function($query) use ($type) {
                $query->where('identifier', $type);
            });
        }
        return $query;
    }

    public function getUserDevicesByMeasurementTypeId($userId, $measurementTypeId)
    {
        $query = $this->model->newQuery()->where('user_id', $userId);
        $query->select('devices.*');
        $query->join('device_names', 'device_names.id', '=', 'devices.device_name_id');
        $query->join('device_name_measurement_type', 'device_name_measurement_type.device_name_id',
            '=', 'device_names.id');
        $query->join('measurement_type', 'device_name_measurement_type.measurement_type_id',
            '=', 'measurement_type.id');

        $query->where('measurement_type.id', $measurementTypeId);

        return $query;
    }
}