<?php

namespace App\Repositories\Eloquent\i18n;

use App\Models\i18nModule;
use App\Repositories\EloquentRepository;
use App\Repositories\Interfaces\i18n\i18nModuleInterface;

class i18nModuleRepository extends EloquentRepository implements i18nModuleInterface
{
    public function __construct(i18nModule $model)
    {
        $this->model = $model;
        parent::__construct();
    }

    public function all($columns = ['*'])
    {
        return $this->orderBy('name')->getQuery()->get();
    }
}