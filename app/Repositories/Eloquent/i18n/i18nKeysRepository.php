<?php

namespace App\Repositories\Eloquent\i18n;

use App\Models\i18nKey;
use App\Repositories\EloquentRepository;
use App\Repositories\Interfaces\i18n\i18nKeysInterface;

class i18nKeysRepository extends EloquentRepository implements i18nKeysInterface
{
    protected $additionalData;

    public function __construct(i18nKey $model)
    {
        $this->model = $model;
        parent::__construct();
    }

    public function setAdditionalQueryData($data)
    {
        $this->additionalData = $data;
        return $this;
    }

    public function all($columns = array('*'))
    {
        return $this->allQuery()->get();
    }

    public function allQuery()
    {
        $query = $this->orderBy('key', 'asc')->getQuery();
        if (!empty($this->additionalData['module_id'])) {
            $query->where('module_id', $this->additionalData['module_id']);
        }
        return $query->join('i18n_modules', 'i18n_modules.id', '=', 'i18n_keys.module_id')
            ->select(['i18n_keys.*', 'i18n_modules.name AS module_name']);
    }

    public function isKeyExist($params) {

        $query = $this->where('key', $params['key'])->where('module_id', $params['moduleId']);
        if (!empty($params['ignoreId'])) {
            $query->where('id', '<>', $params['ignoreId']);
        }
        return (bool)$this->first();
    }
}