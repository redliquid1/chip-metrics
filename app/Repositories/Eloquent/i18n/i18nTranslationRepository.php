<?php

namespace App\Repositories\Eloquent\i18n;

use App\Models\i18nTranslation;
use App\Repositories\EloquentRepository;
use App\Repositories\Interfaces\i18n\i18nTranslationInterface;

class i18nTranslationRepository extends EloquentRepository implements i18nTranslationInterface
{
    protected $additionalData;

    public function __construct(i18nTranslation $model)
    {
        $this->model = $model;
        parent::__construct();
    }
}