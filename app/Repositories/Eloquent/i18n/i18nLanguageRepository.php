<?php

namespace App\Repositories\Eloquent\i18n;

use App\Models\i18nLanguage;
use App\Repositories\EloquentRepository;
use App\Repositories\Interfaces\i18n\i18nLanguageInterface;

class i18nLanguageRepository extends EloquentRepository implements i18nLanguageInterface
{
    public function __construct(i18nLanguage $model)
    {
        $this->model = $model;
        parent::__construct();
    }
}