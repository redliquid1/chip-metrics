<?php

namespace App\Repositories\Eloquent;

use App\Models\MeasurementType;
use App\Repositories\EloquentRepository;
use App\Repositories\Interfaces\MeasurementTypesInterface;
use Illuminate\Support\Collection;

class MeasurementTypesRepository extends EloquentRepository implements MeasurementTypesInterface
{
    public function __construct(MeasurementType $model)
    {
        $this->model = $model;
        parent::__construct();
    }

    /**
     * User all user types.
     *
     * @param User $user
     * @return Collection
     */
    public function userTypes($user)
    {
        $query = $this->getQuery();
        $query->distinct();
        $query->select('measurement_type.id');
        $query->join('device_name_measurement_type', 'measurement_type.id',
            '=', 'device_name_measurement_type.measurement_type_id');
        $query->join('device_names', 'device_names.id',
            '=', 'device_name_measurement_type.device_name_id');
        $query->join('devices', 'devices.device_name_id',
            '=', 'device_names.id');
        $query->where('user_id', $user->id);

        $ids = $query->get()->pluck('id')->toArray();
        if ($ids) {
            return $this->model->newQuery()->whereIn('id', $ids)->get();
        }

        return collect([]);
    }
}