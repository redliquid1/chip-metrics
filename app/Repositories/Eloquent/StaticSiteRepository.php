<?php

namespace App\Repositories\Eloquent;

use App\Models\StaticSite;
use App\Repositories\EloquentRepository;
use App\Repositories\Interfaces\StaticSiteInterface;

class StaticSiteRepository extends EloquentRepository implements StaticSiteInterface
{
    public function __construct(StaticSite $model)
    {
        $this->model = $model;
        parent::__construct();
    }

    public function getStaticSites()
    {
        return $this->orderBy('name', 'asc')->where('language', 'pl')->get();
    }
}