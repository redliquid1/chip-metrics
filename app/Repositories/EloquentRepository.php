<?php namespace App\Repositories;

use App\Repositories\Interfaces\RepositoryInterface;
use Illuminate\Database\Query\Builder;

abstract class EloquentRepository implements RepositoryInterface
{
    /** @var Builder */
    protected $query;
    protected $model;

    public function __construct()
    {
        $this->scopeReset();
    }

    public function initQuery()
    {
        $this->model = $this->model->newInstance();
        $this->query = $this->model->newQuery();

        return $this;
    }

    public function getQuery()
    {
        return $this->query;
    }

    public function all($columns = array('*'))
    {
        return $this->query->get($columns);
    }

    public function get($columns = array('*'))
    {
        return $this->all($columns);
    }

    public function allByUser($user_id, $columns = array('*'))
    {
        return $this->where('user_id', '=', $user_id)->all($columns);
    }

    public function paginate($perPage, $columns = array('*'), $page = null)
    {
        return $this->query->paginate($perPage, $columns, 'page', $page);
    }

    public function create(array $attributes, $parent = false)
    {
        return $this->model->create($attributes);
    }

    public function find($id, $columns = array('*'))
    {
        return $this->query->find($id);
    }

    public function findOrFail($id, $columns = array('*'))
    {
        return $this->query->findOrFail($id);
    }

    public function findBySlug($slug, $columns = array('*'))
    {
        return $this->where([
            'slug'  => $slug
        ])->firstOrFail($columns);
    }

    public function findByColumn($column, $text)
    {
        return $this->query->where($column, $text)->first();
    }

    public function first(array $columns = array('*'))
    {
        return $this->query->first($columns);
    }

    public function pluck($columns, $key = null)
    {
        return $this->query->pluck($columns, $key);
    }

    public function firstOrFail(array $columns = array('*'))
    {
        return $this->query->firstOrFail($columns);
    }

    public function where($arguments, $operator = null, $value = null)
    {
        $this->query->where($arguments, $operator, $value);

        return $this;
    }

    public function whereRaw(string $sql, $bindings = [], string $boolean = 'and')
    {
        $this->query->whereRaw($sql, $bindings, $boolean);

        return $this;
    }

    public function whereIn(string $column, array $values) 
    {
        $this->query->whereIn($column, $values);
        
        return $this;
    }
    
    public function whereBetween(string $column, array $values, string $boolean = 'and', bool $not = false)
    {
        $this->query->whereBetween($column, $values, $boolean, $not);

        return $this;
    }

    public function with($relations): self
    {
        $this->query->with($relations);

        return $this;
    }

    public function withCount($relations): self
    {
        $this->query->withCount($relations);

        return $this;
    }

    public function orderBy($column, string $direction = 'asc')
    {
        if (is_array($column)) {
            $this->query->orderBy($column[0], $column[1]);
        } else {
            $this->query->orderBy($column, $direction);
        }

        return $this;
    }

    public function update($id, array $attributes)
    {
        $object = $this->where('id', '=', $id)->first();

        $object->update($attributes);

        return $object;
    }

    public function attach($object, string $relation, array $related_ids)
    {
        $object->$relation()->attach($related_ids);

        return true;
    }

    public function detach($object, string $relation, array $related_ids)
    {
        $object->$relation()->detach($related_ids);

        return true;
    }

    public function delete($id)
    {
        $this->scopeReset();

        $object = $this->find($id);

        return $object->delete();
    }

    public function count()
    {
        return $this->query->count();
    }

    public function updateOrCreate(array $attributes, array $values = [])
    {
        return $this->model->updateOrCreate($attributes, $values);
    }

    public function whereHas(string $relation, \Closure $callback = null, string $operator = '>=', int $count = 1)
    {
        $this->query->whereHas($relation, $callback, $operator, $count);

        return $this;
    }

    public function whereNull($column)
    {
        $this->query->whereNull($column);
        return $this;
    }

    public function whereNotNull($column)
    {
        $this->query->whereNotNull($column);
        return $this;
    }

    public function join($table, $first, $operator = null, $second = null)
    {
        $this->query->join($table, $first, $operator, $second);
        return $this;
    }

    protected function scopeReset()
    {
        $this->initQuery();
    }
}
