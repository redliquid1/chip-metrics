<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class i18nExport extends Command
{
    protected $signature = 'i18n:export';
    protected $description = 'Export data to seed file.';

    protected $tables = [
        'i18n_keys', 'i18n_languages', 'i18n_modules', 'i18n_translations'
    ];

    public function handle()
    {
        $command = 'mysqldump -u'.env('DB_USERNAME').' -p'.env('DB_PASSWORD').' '.env('DB_DATABASE').' ';
        $command.= implode(' ', $this->tables).' > '.database_path('seeds/sources/i18n.sql');
        exec($command);

        $this->comment('Dump saved.');
    }
}