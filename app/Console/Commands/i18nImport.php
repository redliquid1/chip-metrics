<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Eloquent;
use DB;

class i18nImport extends Command
{
    protected $signature = 'i18n:import';
    protected $description = 'Import data from seed file.';

    public function handle()
    {
        Eloquent::unguard();
        DB::unprepared(file_get_contents(database_path('seeds/sources/i18n.sql')));
    }
}
