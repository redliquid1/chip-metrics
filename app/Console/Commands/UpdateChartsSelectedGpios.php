<?php

namespace App\Console\Commands;

use App\Repositories\Eloquent\ChartRepository;
use App\Repositories\Interfaces\ChartInterface;
use Illuminate\Console\Command;

class UpdateChartsSelectedGpios extends Command
{
    protected $signature = 'charts:update-selected-gpios';
    protected $description = 'Update selected gpios.';

    public function handle()
    {
        /** @var ChartRepository $chartsRepository */
        $chartsRepository = app(ChartInterface::class);
        $data = $chartsRepository->all();

        foreach($data as $row) {
            $cData = json_decode($row->data, true);
            $gpios = $cData['selectedGpios'];
            $sum = array_sum($gpios);
            if (!$sum && $gpios) {
                $ids = [];
                foreach($gpios as $row2) {
                    $ids[] = $row2['id'];
                }
                $cData['selectedGpios'] = $ids;
            }
            $row->data = json_encode($cData);
            $row->save();
        }

        $this->comment('Gpios updated.');
    }
}
