<?php

namespace App\Console;

use App\Console\Commands\UpdateChartsSelectedGpios;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Console\Commands\i18nExport;
use App\Console\Commands\i18nImport;

class Kernel extends ConsoleKernel
{
    protected $commands = [
        i18nImport::class,
        i18nExport::class,
        UpdateChartsSelectedGpios::class
    ];

    protected function schedule(Schedule $schedule)
    {

    }
}
