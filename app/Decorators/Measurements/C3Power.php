<?php

namespace App\Decorators\Measurements;

use App\Repositories\Eloquent\DeviceGpioNamesRepository;
use App\Repositories\Interfaces\DeviceInterface;
use Illuminate\Contracts\Auth\Guard;

class C3Power
{
    protected $data;

    protected $guard;
    protected $deviceGpioNamesRepository;
    protected $deviceRepository;

    public function __construct(
        Guard $guard,
        DeviceGpioNamesRepository $deviceGpioNamesRepository,
        DeviceInterface $deviceRepository)
    {
        $this->guard = $guard;
        $this->deviceGpioNamesRepository = $deviceGpioNamesRepository;
        $this->deviceRepository = $deviceRepository;
    }

    /**
     * Set raw data from repository.
     *
     * @param array $data
     * @return $this
     */
    public function setData($data)
    {
        $this->data = $data;
        return $this;
    }

    /**
     * Get data for charts.
     *
     * @return array
     */
    public function get()
    {
        $output = [];

        foreach($this->data as $row)
        {
            $out = [];
            $out['names'] = $this->deviceGpioNamesRepository->getGpioNames($this->guard->user()->id, $row['device_id']);
            $out['data'] = $row['data'];
            $output[] = $out;
        }

        return $output;
    }
}