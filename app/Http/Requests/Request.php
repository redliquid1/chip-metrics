<?php

namespace App\Http\Requests;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;

abstract class Request extends FormRequest
{
    public function isSuperAdminLogged()
    {
        /** @var User $user */
        $user = \Auth::user();
        if ($user) {
            return $user->hasRole('super-admin');
        }

        return false;
    }
}
