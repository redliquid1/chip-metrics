<?php

namespace App\Http\Requests\Api\V1;

use App\Http\Requests\Request;

class FaqRequest extends Request
{
    public function authorize()
    {
        return $this->isSuperAdminLogged();
    }

    public function rules()
    {
        return [];
    }

}