<?php

namespace App\Http\Requests\Api\V1\Measurement;

use App\Http\Requests\Request;
use App\Repositories\Interfaces\DeviceInterface;
use Illuminate\Validation\Rule;

class GetPowerRequest extends Request
{
    public function authorize()
    {
        $deviceIds = explode(',', $this->request->get('deviceIds', ''));
        $user = \Auth::user();
        if ($user) {
            $deviceRepository = app(DeviceInterface::class);
            $devices = $deviceRepository->whereIn('id', $deviceIds)->all();
            foreach($devices as $device) {
                if ($device->user_id != $user->id) {
                    return false;
                }
            }
            return true;
        }

        return false;
    }

    public function rules()
    {
        return [];
    }

}