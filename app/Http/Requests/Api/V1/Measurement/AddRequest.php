<?php

namespace App\Http\Requests\Api\V1\Measurement;

use App\Http\Requests\Request;
use Illuminate\Validation\Rule;

class AddRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'device_id' => 'required',
            'timestamp' => 'required',
        ];

        return $rules;
    }

}