<?php

namespace App\Http\Requests\Api\V1;

use App\Http\Requests\Request;

class DeviceNameRequest extends Request
{
    public function authorize()
    {
        return $this->isSuperAdminLogged();
    }

    public function rules()
    {
        return [];
    }

}