<?php

namespace App\Http\Requests\Api\V1;

use App\Http\Requests\Request;
use App\Repositories\Interfaces\DeviceInterface;

class DeviceUpdateDataRequest extends Request
{
    public function authorize()
    {
        $deviceId = $this->request->get('id');
        $user = \Auth::user();
        if ($user) {
            $deviceRepository = app(DeviceInterface::class);
            $device = $deviceRepository->find($deviceId);
            return $device->user_id == $user->id;
        }

        return false;
    }

    public function rules()
    {
        $rules = [
            'name' => 'required',
        ];

        return $rules;
    }

}