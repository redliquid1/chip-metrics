<?php

namespace App\Http\Requests\Api\V1;

use App\Http\Requests\Request;
use App\Repositories\Interfaces\ChartInterface;

class UserChartRequest extends Request
{
    public function authorize()
    {
        $chartId = $this->request->get('id');
        $user = \Auth::user();

        if (!$chartId && $user) return true;

        if ($user) {
            $chartRepository = app(ChartInterface::class);
            $chart = $chartRepository->find($chartId);
            return $chart->user_id == $user->id;
        }

        return false;
    }

    public function rules()
    {
        $rules = [];

        return $rules;
    }

}