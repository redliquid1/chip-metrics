<?php

namespace App\Http\Requests\Api\V1;

use App\Http\Requests\Request;
use App\Repositories\Interfaces\DeviceInterface;

class SaveGpiosRequest extends Request
{
    protected function getDevice()
    {
        $deviceId = $this->request->get('id');
        $deviceRepository = app(DeviceInterface::class);
        return $deviceRepository->find($deviceId);
    }

    public function authorize()
    {
        $device = $this->getDevice();
        $user = \Auth::user();
        if ($user && $device) {
            return $device->user_id == $user->id;
        }

        return false;
    }

    public function rules()
    {
        $device = $this->getDevice();

        $rules = [];
        foreach(range(0, 5) as $number) {
            $rules['data.'.$number.'.name'] = 'required';
            if ($device->deviceName->identifier == 'power_meter') {
                $rules['data.'.$number.'.ppu'] = 'required|numeric';
                $rules['data.'.$number.'.color'] = 'required';
            }
            else {
                break;
            }
        }
        return $rules;
    }

}