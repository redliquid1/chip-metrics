<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenBlacklistedException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Facades\JWTAuth;

class CheckTokenWeb
{
    public function handle($request, Closure $next)
    {
        $token = \Session::get('authToken');
        if ($token) {
            $request->request->add(['token' => $token]);
        }

        if (!$request->token) {
            return $next($request);
        }

        try
        {
            // JWTAuth::setToken($request->token);
            if (! $user = JWTAuth::parseToken()->authenticate() )
            {
                return response()->json([
                    'code' => 101,
                    'response' => null
                ]);
            }
        }
        catch (TokenBlacklistedException $e)
        {
            \Session::forget('authToken');
            return $next($request);
        }
        catch (TokenExpiredException $e)
        {
            try
            {
                $refreshed = JWTAuth::refresh(JWTAuth::getToken());
                $user = JWTAuth::setToken($refreshed)->toUser();
                header('Authorization: Bearer ' . $refreshed);
            }
            catch (JWTException $e)
            {
                if ($e instanceof TokenBlacklistedException) {
                    return $next($request);
                }

                return response()->json([
                    'code' => 103,
                    'response' => null
                ]);
            }
        }
        catch (JWTException $e)
        {
            dd($e);
            return response()->json([
                'code' => 101,
                'response' => null
            ]);
        }

        // Login the user instance for global usage
        Auth::login($user, false);

        return $next($request);
    }
}
