<?php

namespace App\Http\Controllers;

use App\Repositories\Eloquent\StaticSiteRepository;
use App\Repositories\Interfaces\StaticSiteInterface;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /** @var StaticSiteRepository */
    protected $staticSiteRepository;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(StaticSiteInterface $staticSiteRepository)
    {
        $this->staticSiteRepository = $staticSiteRepository;
    }

    /**
     * Show home page.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('site.welcome', [
            'homePage' => $this->staticSiteRepository->findByColumn('name', 'home-page')
        ]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function app()
    {
        return view('app.home');
    }


}
