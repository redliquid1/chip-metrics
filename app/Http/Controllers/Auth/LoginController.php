<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\UserInterface;
use App\Services\User\UserService;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    protected $redirectTo = '/app';

    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * @var UserService
     */
    protected $userService;

    public function __construct(UserInterface $userRepository, UserService $userService, Guard $guard)
    {
        $this->middleware('guest')->except('logout');

        $this->userRepository = $userRepository;
        $this->userService = $userService;
        $this->guard = $guard;
    }

    public function login()
    {
        return view('auth.login');
    }

    public function loginPost(Request $request)
    {
        $credentials = $request->only('email', 'password');
        $this->userService->login($credentials);
        return response()->redirectToRoute('app.index')->withSuccess('User logged out successfuly.');
    }

    public function logout()
    {
        $this->userService->logout();
        return response()->redirectToRoute('home');
    }


    public function isEmailUnique(Request $request)
    {
        return response()->json([
            'exists' => (bool)$this->userRepository->findByColumn('email', $request->email)
        ]);
    }

    public function register(UserRegisterRequest $request)
    {
        $this->userAccountService->register($request->all());
        $data = $this->userAccountService->login($request->only('email', 'password'));

        return response()->json($data);
    }

    public function saveAvatar(Request $request)
    {
        return response()->api([
            'image' => $this->userAccountService->make($this->guard->user())->saveAvatar($request->all())
        ]);

    }


    public function save(Request $request)
    {
        return response()->api(
            $this->userAccountService->save($request->all())
        );
    }

    public function checkToken($token)
    {
        $user = false;
        try {
            $user = JWTAuth::setToken($token)->authenticate();
        } catch (TokenInvalidException $e) {
            return $this->responseError([
                'error' => 'Invalid token'
            ], 422);
        } catch (TokenExpiredException $e) {
            return $this->responseError([
                'error' => 'Token expired'
            ], 422);
        } catch (TokenMismatchException $e) {
            return $this->responseError([
                'error' => 'Token mismatch'
            ], 422);
        } catch (JWTException $e) {
            return $this->responseError([
                'error' => $e->getMessage()
            ], 422);
        }

        return $this->responseSuccess([
            'user' => $user,
            'expiration' => JWTAuth::payload()->exp
        ]);
    }

    public function refreshToken($token)
    {
        try {
            $newToken = JWTAuth::setToken($token)->refresh();
            $user = JWTAuth::setToken($newToken)->authenticate();
            $payload = JWTAuth::payload();
        } catch (TokenInvalidException $e) {
            return $this->responseError([
                'error' => 'Invalid token'
            ]);
        } catch (TokenExpiredException $e) {
            return $this->responseError([
                'error' => 'Token expired'
            ]);
        } catch (TokenMismatchException $e) {
            return $this->responseError([
                'error' => 'Token mismatch'
            ]);
        } catch (TokenBlacklistedException $e) {
            return $this->responseError([
                'error' => 'Token blacklisted'
            ]);
        } catch (JWTException $e) {
            return $this->responseError([
                'error' => $e->getMessage()
            ]);
        }

        return $this->responseSuccess([
            'newToken' => $newToken,
            'user' => $user,
            'expiration' => $payload->get('exp')
        ]);
    }

}
