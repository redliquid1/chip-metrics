<?php

namespace App\Http\Controllers;

use App\Repositories\Eloquent\FaqRepository;
use App\Repositories\Interfaces\FaqInterface;
use Illuminate\Http\Request;

class FaqController extends Controller
{
    protected $faqRepository;

    public function __construct(FaqInterface $faqRepository)
    {
        $this->faqRepository = $faqRepository;
    }

    public function index()
    {
        return view('site.faq.index', [
            'data' => $this->faqRepository->orderBy('sort', 'asc')->orderBy('id', 'asc')->get()
        ]);
    }
}
