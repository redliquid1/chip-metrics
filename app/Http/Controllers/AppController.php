<?php

namespace App\Http\Controllers;

class AppController extends Controller
{
    /**
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('app.index');
    }


}
