<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\Device;
use App\Repositories\Eloquent\DeviceRepository;
use App\Repositories\Interfaces\DeviceInterface;
use App\Services\Device\DeviceService;
use Illuminate\Http\Request;
use App\Http\Requests\UserRegisterRequest;
use App\Services\Time\TimeService;

class SystemDevicesController extends Controller
{
    /** @var DeviceRepository */
    protected $deviceRepository;

    /** @var DeviceService */
    protected $deviceService;

    public function __construct(DeviceInterface $deviceRepository, DeviceService $deviceService)
    {
        $this->deviceRepository = $deviceRepository;
        $this->deviceService = $deviceService;
    }

    public function getAll()
    {
        return response()->api(
            $this->deviceRepository->with('user')->all()
        );
    }

    public function save(Request $request, Device $device)
    {
        return response()->api(
            $this->deviceService->make($device)->save($request->only(['chip_id', 'device_name_id']))
        );
    }

    public function remove(Device $device)
    {
        return response()->api(
            $this->deviceService->make($device)->delete()
        );
    }
}