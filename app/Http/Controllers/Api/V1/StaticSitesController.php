<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\V1\FaqRequest;
use App\Http\Requests\Api\V1\StaticSiteRequest;
use App\Models\Faq;
use App\Models\StaticSite;
use App\Repositories\Eloquent\FaqRepository;
use App\Repositories\Eloquent\StaticSiteRepository;
use App\Repositories\Interfaces\FaqInterface;
use App\Repositories\Interfaces\StaticSiteInterface;
use App\Services\Faq\FaqService;
use App\Services\StaticSite\StaticSiteService;

class StaticSitesController extends Controller
{
    /** @var StaticSiteRepository */
    protected $staticSiteRepository;

    /** @var StaticSiteService */
    protected $staticSiteService;

    public function __construct(StaticSiteInterface $staticSiteRepository, StaticSiteService $staticSiteService)
    {
        $this->staticSiteRepository = $staticSiteRepository;
        $this->staticSiteService = $staticSiteService;
    }

    public function all()
    {
        return response()->api(
            $this->staticSiteRepository->getStaticSites()
        );
    }

    public function save(StaticSiteRequest $request, StaticSite $staticSite = null)
    {
        $model = $this->staticSiteService->make($staticSite)->save($request->all());

        return response()->api(
            $staticSite ? $model : $this->staticSiteRepository->getStaticSites()
        );
    }

    public function remove(StaticSiteRequest $request, StaticSite $staticSite)
    {
        return response()->api(
            $this->staticSiteService->make($staticSite)->delete()
        );
    }
}