<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRegisterRequest;
use App\Repositories\Eloquent\DeviceNamesRepository;
use App\Repositories\Eloquent\MeasurementTypesRepository;
use App\Repositories\Interfaces\DeviceNamesInterface;
use App\Repositories\Interfaces\MeasurementTypesInterface;
use App\Services\User\UserAccountService;
use Auth;
use Illuminate\Contracts\Auth\Guard;

class MeasurementTypesController extends Controller
{
    /** @var MeasurementTypesRepository */
    protected $measurementTypesRepository;

    /** @var Guard */
    protected $guard;

    public function __construct(MeasurementTypesInterface $measurementTypesRepository, Guard $guard)
    {
        $this->measurementTypesRepository = $measurementTypesRepository;
        $this->guard = $guard;
    }

    public function all()
    {
        return response()->api(
            $this->measurementTypesRepository->all()
        );
    }

    public function user()
    {
        return response()->api(
            $this->measurementTypesRepository->userTypes($this->guard->user())
        );
    }
}