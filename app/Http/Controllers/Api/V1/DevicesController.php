<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\V1\DeviceUpdateDataRequest;
use App\Http\Requests\Api\V1\SaveGpiosRequest;
use App\Http\Requests\UserRegisterRequest;
use App\Models\Device;
use App\Repositories\Eloquent\DeviceRepository;
use App\Repositories\Eloquent\UserRepository;
use App\Repositories\Interfaces\DeviceInterface;
use App\Repositories\Interfaces\UserInterface;
use App\Services\Device\DeviceGpioNamesService;
use App\Services\Device\DeviceService;
use App\Services\User\UserAccountService;
use Auth;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;

class DevicesController extends Controller
{
    /** @var DeviceRepository */
    protected $deviceRepository;

    /** @var Guard */
    protected $guard;

    /** @var DeviceService */
    protected $deviceService;

    public function __construct(DeviceInterface $deviceRepository, Guard $guard, DeviceService $deviceService)
    {
        $this->deviceRepository = $deviceRepository;
        $this->guard = $guard;
        $this->deviceService = $deviceService;
    }

    public function getDevices(Request $request)
    {
        return response()->api(
            $this->deviceRepository->getUserDevices($this->guard->user()->id, $request->get('type'))->get()
        );
    }

    public function isDeviceAvailable(Request $request)
    {
        return response()->api(
            $this->deviceService->isDeviceAvailable($request->get('chipId'))
        );
    }

    public function addDevice(Request $request)
    {
        return response()->api(
            $this->deviceService->addDevice($request->get('chipId'), $this->guard->user()->id)
        );
    }

    public function removeDevice(Device $device)
    {
        return response()->api(
            $this->deviceService->make($device)->remove()
        );
    }

    public function updateData(DeviceUpdateDataRequest $request, Device $device)
    {
        return response()->api(
            $this->deviceService->make($device)->updateData($request->only(['name']))
        );
    }

    public function getGpios(Request $request, Device $device)
    {
        return response()->api(
            $this->deviceService->make($device)->getGpios()
        );
    }

    public function saveGpios(SaveGpiosRequest $request, Device $device)
    {
        return response()->api(
            $this->deviceService->make($device)->saveGpios($request->get('data'))
        );
    }

    public function getAllUserGpios(Request $request, DeviceGpioNamesService $deviceGpioNamesService)
    {
        return response()->api(
            $deviceGpioNamesService->getAllUserGpios($this->guard->user()->id, $request->get('measurement_type_id'))
        );
    }

    public function exportData(Request $request, Device $device)
    {
        $data = $this->deviceService->make($device)->exportData();
        return response()->file(
            storage_path($data['fileName'])
        );
    }
}
