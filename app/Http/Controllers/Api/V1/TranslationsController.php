<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\V1\FaqRequest;
use App\Models\Faq;
use App\Models\i18nKey;
use App\Models\i18nTranslation;
use App\Repositories\Eloquent\i18n\i18nKeysRepository;
use App\Repositories\Eloquent\i18n\i18nLanguageRepository;
use App\Repositories\Eloquent\i18n\i18nModuleRepository;
use App\Repositories\Eloquent\i18n\i18nTranslationRepository;
use App\Repositories\Interfaces\i18n\i18nKeysInterface;
use App\Repositories\Interfaces\i18n\i18nLanguageInterface;
use App\Repositories\Interfaces\i18n\i18nModuleInterface;
use App\Repositories\Interfaces\i18n\i18nTranslationInterface;
use App\Services\DataTables\DataTablesService;
use App\Services\i18n\Adapters\DatatablesAdapter;
use App\Services\i18n\Adapters\Vuei18nAdapter;
use App\Services\i18n\i18nKeyService;
use App\Services\i18n\i18nModuleService;
use App\Services\i18n\i18nTranslationService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;

class TranslationsController extends Controller
{
    /** @var i18nTranslationRepository */
    protected $i18nTranslationRepository;

    /** @var i18nModuleRepository */
    protected $i18nModuleRepository;

    /** @var i18nLanguageRepository */
    protected $i18nLanguageRepository;

    /** @var i18nTranslationService */
    protected $i18nTranslationService;

    /** @var i18nModuleService */
    protected $i18nModuleService;

    /** @var i18nKeysRepository */
    protected $i18nKeysRepository;

    /** @var DataTablesService */
    protected $dataTablesService;

    /** @var i18nKeyService */
    protected $i18nKeyService;

    public function __construct(i18nTranslationInterface $i18nTranslationRepository,
                                i18nTranslationService $i18nTranslationService,
                                i18nModuleInterface $i18nModuleRepository,
                                i18nModuleService $i18nModuleService,
                                i18nLanguageInterface $i18nLanguageRepository,
                                i18nKeysInterface $i18nKeysRepository,
                                i18nKeyService $i18nKeyService,
                                DataTablesService $dataTablesService)
    {
        $this->i18nTranslationRepository = $i18nTranslationRepository;
        $this->i18nTranslationService = $i18nTranslationService;
        $this->i18nModuleRepository = $i18nModuleRepository;
        $this->i18nModuleService = $i18nModuleService;
        $this->i18nLanguageRepository = $i18nLanguageRepository;
        $this->i18nKeysRepository = $i18nKeysRepository;
        $this->i18nKeyService = $i18nKeyService;
        $this->dataTablesService = $dataTablesService;
    }

    public function all()
    {
        return response()->api($this->i18nTranslationService->getSiteTranslations());
    }

    public function datatables(Request $request)
    {
        $additionalData = [
            'module_id' => $request->get('module_id'),
        ];

        return response()->api(
            (new DatatablesAdapter(
                $this->dataTablesService->setMode(DataTablesService::MODE_i18n)->get(
                    $this->i18nKeysRepository->setAdditionalQueryData($additionalData)->allQuery()
                )
            ))->get($additionalData)
        );
    }

    public function setTranslation(Request $request, i18nTranslationService $translationService)
    {
        return response()->api(
            $translationService->setTranslation($request->get('data'))
        );
    }

    public function changeKey(Request $request, i18nTranslationService $translationService)
    {
        return response()->api(
            $translationService->changeKey($request->get('data'))
        );
    }

    public function save(Request $request)
    {
        $data = $request->get('data');
        $key = !empty($data['id']) ? $this->i18nKeysRepository->find($data['id']) : null;

        return response()->api(
            $model = $this->i18nKeyService->make($key)->save($request->get('data'))
        );
    }

    public function saveModules(Request $request)
    {
        return response()->api(
            $this->i18nModuleService->saveRaw($request->get('data'))
        );
    }

    public function remove(Request $request, i18nKey $key)
    {

        return response()->api(
            $model = $this->i18nKeyService->make($key)->delete()
        );
    }

    public function languages()
    {
        return response()->api(
            $this->i18nLanguageRepository->all()
        );
    }

    // --------------------------
    public function modules()
    {
        return response()->api(
            $this->i18nModuleRepository->all()
        );
    }

    public function keyExist(Request $request)
    {
        return response()->api(
            $this->i18nKeysRepository->isKeyExist($request->all())
        );
    }

    public function importData()
    {
        Artisan::call('i18n:import');
        return response()->api([
            'ok' => 1
        ]);
    }

    public function exportData()
    {
        Artisan::call('i18n:export');
        return response()->api([
            'ok' => 1
        ]);
    }
}
