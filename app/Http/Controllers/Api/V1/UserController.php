<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRegisterRequest;
use App\Models\User;
use App\Providers\SocialMedia\SocialMedia;
use App\Repositories\Eloquent\UserRepository;
use App\Repositories\Interfaces\UserInterface;
use App\Services\User\UserAccountService;
use App\Services\User\UserService;
use Auth;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Illuminate\Session\TokenMismatchException;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenBlacklistedException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Facades\JWTAuth;

class UserController extends Controller
{
    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * @var UserService
     */
    protected $userService;

    public function __construct(UserInterface $userRepository, UserService $userService, Guard $guard)
    {
        $this->userRepository = $userRepository;
        $this->userService = $userService;
        $this->guard = $guard;
    }

    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');
        $userData = $this->userService->login($credentials);
        if (!isset($userData['code'])) {
            $userData['code'] = 200;
        }
        $response = response()->json($userData);
        return $response;
    }

    public function isEmailUnique(Request $request)
    {
        return response()->json([
            'exists' => (bool)$this->userRepository->findByColumn('email', $request->email)
        ]);
    }

    public function register(Request $request)
    {
        $this->userService->register($request->all());
        $data = $this->userService->login($request->only('email', 'password'));

        return response()->api($data);
    }

    public function logout()
    {
        return response()->json([
            'ok' => 1,
            'impersonated_from' => $this->userService->logout()
        ]);
    }

    public function save(Request $request)
    {
        return response()->api(
            $this->userService->make($this->guard->user())->save($request->only(['name', 'password']))
        );
    }

    public function checkToken($token)
    {
        $user = false;
        try {
            $user = JWTAuth::setToken($token)->authenticate();
        } catch (TokenInvalidException $e) {
            return response()->api([
                'error' => 'Invalid token'
            ], 422);
        } catch (TokenExpiredException $e) {
            return response()->api([
                'error' => 'Token expired'
            ], 422);
        } catch (TokenMismatchException $e) {
            return response()->api([
                'error' => 'Token mismatch'
            ], 422);
        } catch (JWTException $e) {
            return response()->api([
                'error' => $e->getMessage()
            ], 422);
        }

        return response()->api([
            'user' => $user,
            'expiration' => JWTAuth::payload()->exp
        ]);
    }

    public function refreshToken($token)
    {
        try {
            $newToken = JWTAuth::setToken($token)->refresh();
            $user = JWTAuth::setToken($newToken)->authenticate();
        } catch (TokenInvalidException $e) {
            return response()->api([
                'error' => 'Invalid token'
            ]);
        } catch (TokenExpiredException $e) {
            return response()->api([
                'error' => 'Token expired'
            ]);
        } catch (TokenMismatchException $e) {
            return response()->api([
                'error' => 'Token mismatch'
            ]);
        } catch (TokenBlacklistedException $e) {
            return response()->api([
                'error' => 'Token blacklisted'
            ]);
        } catch (JWTException $e) {
            return response()->api([
                'error' => $e->getMessage()
            ]);
        }

        return response()->json([
            'code' => 200,
            'token' => $newToken,
            'user' => $user
        ]);
    }

    public function hash()
    {
        return \Hash::make('admin');
    }

    public function getAll(Request $request)
    {
        return response()->api(
            $this->userRepository->all()
        );
    }

    public function saveUser(Request $request, User $user = null)
    {
        return response()->api(
            $this->userService->make($user)->save($request->all())
        );
    }

    public function removeUser(User $user)
    {
        return response()->api(
            $this->userService->make($user)->delete()
        );
    }

    public function impersonate(User $user)
    {
        return response()->api(
            $this->userService->make($user)->impersonate()
        );
    }

    public function getAllDeviceNames()
    {
        return response()->api(
            $this->userService->make($this->guard->user())->allDeviceNames()
        );
    }

    public function getAllDeviceTypes()
    {
        return response()->api(
            $this->userService->make($this->guard->user())->allDeviceTypes()
        );
    }

    public function saveAvatar(Request $request, User $user)
    {
        return response()->api(
            $this->userService->make($user)->saveAvatar($request->file('avatar'))
        );
    }
}