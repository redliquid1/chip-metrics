<?php

namespace App\Http\Controllers\Api\V1\Measurement;

use App\Decorators\Measurements\C3Power;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\V1\Measurement\GetPowerRequest;
use App\Repositories\Eloquent\Measurement\PowerRepository;
use App\Repositories\Interfaces\Measurements\PowerInterface;
use Illuminate\Support\Facades\Validator;
use App\Services\Device\DeviceService;
use App\Services\Measurement\PowerService;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use Tymon\JWTAuth\JWTGuard;

class PowerController extends Controller
{
    /** @var PowerService */
    protected $powerService;

    /** @var DeviceService */
    protected $deviceService;

    /** @var PowerRepository */
    protected $powerRepository;

    public function __construct(PowerService $powerService, DeviceService $deviceService, PowerInterface $powerRepository)
    {
        $this->powerService = $powerService;
        $this->deviceService = $deviceService;
        $this->powerRepository = $powerRepository;
    }

    public function getPower(GetPowerRequest $request)
    {
        $params = [
            'type' => $request->get('chartPeriod', PowerRepository::TYPE_DAILY),
            'deviceIds' => array_unique(explode(',', trim($request->get('deviceIds', '')))),
            'period' => $request->get('periodTime', 1),
            'includeCurrentPeriod' => $request->get('includeCurrentPeriod', 1),
        ];

        return response()->api(
            $this->powerRepository->getMeasurements($params)
        );
    }



    /**
     * Add one record to the database. Can't use Request validation here because the response may be to lenghty for the microcontroller.
     *
     * @param Request $request
     * @return mixed
     */
    public function add(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'device_id' => 'required',
            'timestamp' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'result' => 0,
                'error-code' => 0
            ])->setStatusCode(422);
        }

        try{
            $this->powerService->add($request->all());
        }
        catch(\Exception $e) {
            return response()->json([
                'result' => 0,
                'error-code' => 0
            ])->setStatusCode(422);
        }

        return response()->json([
            'result' => 1,
            'error-code' => 0
        ]);
    }

    public function addArchive(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'device_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'result' => 0,
                'error-code' => 0
            ])->setStatusCode(422);
        }

        try{
            $this->powerService->addArchive($request->all());
        }
        catch(\Exception $e) {
            return response()->json([
                'result' => 0,
                'error-code' => 0
            ])->setStatusCode(422);
        }

        return response()->json([
            'result' => 1,
            'error-code' => 0
        ]);
    }

}