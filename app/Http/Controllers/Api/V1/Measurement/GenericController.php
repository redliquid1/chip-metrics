<?php

namespace App\Http\Controllers\Api\V1\Measurement;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\V1\Measurement\GetPowerRequest;
use App\Repositories\Eloquent\DeviceRepository;
use App\Repositories\Eloquent\Measurement\PowerRepository;
use App\Repositories\Eloquent\Measurement\TemperatureRepository;
use App\Repositories\Eloquent\MeasurementTypesRepository;
use App\Repositories\Interfaces\DeviceInterface;
use App\Repositories\Interfaces\Measurements\PowerInterface;
use App\Repositories\Interfaces\Measurements\TemperatureInterface;
use App\Repositories\Interfaces\MeasurementTypesInterface;
use App\Services\Device\DeviceService;
use App\Services\Measurement\PowerService;
use App\Services\Measurement\TemperatureService;
use Auth;

class GenericController extends Controller
{
    /** @var PowerService */
    protected $powerService;

    /** @var DeviceService */
    protected $deviceService;

    /** @var DeviceRepository */
    protected $deviceRepository;

    /** @var PowerRepository */
    protected $powerRepository;

    /** @var TemperatureService */
    protected $temperatureService;

    /** @var TemperatureRepository */
    protected $temperatureRepository;

    /** @var MeasurementTypesRepository */
    protected $measurementTypesRepository;

    public function __construct(PowerService $powerService, DeviceService $deviceService, PowerInterface $powerRepository,
                                TemperatureService $temperatureService, TemperatureInterface $temperatureRepository,
                                DeviceInterface $deviceRepository, MeasurementTypesInterface $measurementTypesRepository)
    {
        $this->deviceService = $deviceService;
        $this->deviceRepository = $deviceRepository;
        $this->powerService = $powerService;
        $this->powerRepository = $powerRepository;
        $this->temperatureService = $temperatureService;
        $this->temperatureRepository = $temperatureRepository;
        $this->measurementTypesRepository = $measurementTypesRepository;
    }

    public function getMeasurement(GetPowerRequest $request)
    {
        $params = [
            'type' => $request->get('chartPeriod', PowerRepository::TYPE_DAILY),
            'deviceIds' => array_unique(explode(',', trim($request->get('deviceIds', '')))),
            'period' => $request->get('periodTime', 1),
            'includeCurrentPeriod' => $request->get('includeCurrentPeriod', 1),
            'measurementTypeId' => $request->get('measurementTypeId')
        ];

        if (empty($params['measurementTypeId'])) {
            $params['measurementTypeId'] = 1;
        }

        if (empty($params['deviceIds'][0])){
            return response()->api([]);
        }

        $measurementType = $this->measurementTypesRepository->find($params['measurementTypeId']);
        $deviceName = $measurementType->deviceNames->first();

        switch($deviceName->identifier) {
            case TemperatureRepository::DEVICE_NAME;
                $repository = $this->temperatureRepository;
                break;
            default:
            case PowerRepository::DEVICE_NAME:
                $repository = $this->powerRepository;
                break;
        }

        $data = $repository->getMeasurements($params);
        if (!$data) {
            $data = $repository->getSampleMeasurements($params);
        }

        return response()->api($data);
    }
}