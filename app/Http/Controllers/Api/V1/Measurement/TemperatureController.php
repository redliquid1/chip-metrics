<?php

namespace App\Http\Controllers\Api\V1\Measurement;

use App\Decorators\Measurements\C3Power;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\V1\Measurement\GetPowerRequest;
use App\Http\Requests\Api\V1\Measurement\GetTemperatureRequest;
use App\Repositories\Eloquent\Measurement\PowerRepository;
use App\Repositories\Eloquent\Measurement\TemperatureRepository;
use App\Repositories\Interfaces\Measurements\PowerInterface;
use App\Repositories\Interfaces\Measurements\TemperatureInterface;
use App\Services\Measurement\TemperatureService;
use Illuminate\Support\Facades\Validator;
use App\Services\Device\DeviceService;
use App\Services\Measurement\PowerService;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use Tymon\JWTAuth\JWTGuard;

class TemperatureController extends Controller
{
    /** @var TemperatureService */
    protected $temperatureService;

    /** @var DeviceService */
    protected $deviceService;

    /** @var TemperatureRepository */
    protected $temperatureRepository;

    public function __construct(TemperatureService $temperatureService, DeviceService $deviceService, TemperatureInterface $temperatureRepository)
    {
        $this->temperatureService = $temperatureService;
        $this->deviceService = $deviceService;
        $this->temperatureRepository = $temperatureRepository;
    }

    public function getTemperature(GetTemperatureRequest $request)
    {
        $params = [
            'type' => $request->get('chartPeriod', TemperatureRepository::TYPE_DAILY),
            'deviceIds' => array_unique(explode(',', trim($request->get('deviceIds', '')))),
            'period' => $request->get('periodTime', 1),
            'includeCurrentPeriod' => $request->get('includeCurrentPeriod', 1),
        ];

        return response()->api(
            $this->temperatureRepository->getMeasurements($params)
        );
    }



    /**
     * Add one record to the database. Can't use Request validation here because the response may be to lenghty for the microcontroller.
     *
     * @param Request $request
     * @return mixed
     */
    public function add(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'device_id' => 'required',
            'timestamp' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'result' => 0,
                'error-code' => 0
            ])->setStatusCode(422);
        }

        try{
            $this->temperatureService->add($request->all());
        }
        catch(\Exception $e) {
            return response()->json([
                'result' => 0,
                'error-code' => 0
            ])->setStatusCode(422);
        }

        return response()->json([
            'result' => 1,
            'error-code' => 0
        ]);
    }

    public function addArchive(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'device_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'result' => 0,
                'error-code' => 0
            ])->setStatusCode(422);
        }

        try{
            $this->temperatureService->addArchive($request->all());
        }
        catch(\Exception $e) {
            return response()->json([
                'result' => 0,
                'error-code' => 0
            ])->setStatusCode(422);
        }

        return response()->json([
            'result' => 1,
            'error-code' => 0
        ]);
    }

}