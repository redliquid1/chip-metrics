<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\V1\SaveChartRequest;
use App\Http\Requests\Api\V1\UserChartRequest;
use App\Http\Requests\UserRegisterRequest;
use App\Models\Chart;
use App\Repositories\Eloquent\ChartRepository;
use App\Repositories\Interfaces\ChartInterface;
use App\Services\Chart\ChartService;
use App\Services\User\UserAccountService;
use App\Services\User\UserService;
use Auth;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;

class ChartsController extends Controller
{
    /** @var Guard */
    protected $guard;

    /** @var ChartService */
    protected $chartService;

    /** @var ChartRepository */
    protected $chartRepository;

    /** @var UserService */
    protected $userService;

    public function __construct(Guard $guard, ChartService $chartService, ChartInterface $chartRepository, UserService $userService)
    {
        $this->guard = $guard;
        $this->chartService = $chartService;
        $this->chartRepository = $chartRepository;
        $this->userService = $userService;
    }

    public function getAll()
    {
        return response()->api([
            'charts' => $this->chartRepository->getUserCharts($this->guard->user()->id),
            'dashboard' => json_decode($this->guard->user()->dashboard, true)
        ]);
    }

    public function saveChart(UserChartRequest $request, Chart $chart = null)
    {
        return response()->api(
            $this->chartService->make($chart)->save($request->all())
        );
    }

    public function removeChart(UserChartRequest $request, Chart $chart)
    {
        return response()->api(
            $this->chartService->make($chart)->delete()
        );
    }

    public function saveLayout(Request $request)
    {
        return response()->api(
            $this->userService->saveLayout($request->get('layout'))
        );
    }
}