<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\V1\FaqRequest;
use App\Models\Faq;
use App\Repositories\Eloquent\FaqRepository;
use App\Repositories\Interfaces\FaqInterface;
use App\Services\Faq\FaqService;

class FaqController extends Controller
{
    /** @var FaqRepository */
    protected $faqRepository;

    /** @var FaqService */
    protected $faqService;

    public function __construct(FaqInterface $faqRepository, FaqService $faqService)
    {
        $this->faqRepository = $faqRepository;
        $this->faqService = $faqService;
    }

    public function getAll()
    {
        return response()->api(
            $this->faqRepository->getFaqs()
        );
    }

    public function save(FaqRequest $request, Faq $faq = null)
    {
        $model = $this->faqService->make($faq)->save($request->all());

        return response()->api(
            $faq ? $model : $this->faqRepository->getFaqs()
        );
    }

    public function saveSort(FaqRequest $request)
    {
        return response()->api(
            $this->faqRepository->saveSort($request->get('sort'))
        );
    }

    public function remove(FaqRequest $request, Faq $faq)
    {
        return response()->api(
            $this->faqService->make($faq)->delete()
        );
    }
}