<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\UserRegisterRequest;
use App\Services\Time\TimeService;

class TimeController extends Controller
{
    /**
     * This endpoint returns current time for given request.
     *
     * @response {
     *  "dateTimeRFC2822": "Wed, 19 Sep 2018 11:48:16 +0200",
     *  "data": {
     *     "year": "2018",
     *     "month": "09",
     *     "day": "19",
     *     "hour": "11",
     *     "minute": "12",
     *     "second": "13",
     *     "timezone": "Europe/Warsaw"
     *  },
     *  "time":1537350496
     * }
     * @param TimeService $timeService
     * @return \Illuminate\Http\JsonResponse
     * @throws \GeoIp2\Exception\AddressNotFoundException
     * @throws \MaxMind\Db\Reader\InvalidDatabaseException
     */
    public function getTime(Request $request, TimeService $timeService)
    {
        return response()->json($timeService->getTime($request->all()));
    }
}