<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\V1\FaqRequest;
use App\Http\Requests\Api\V1\ProductRequest;
use App\Models\Faq;
use App\Models\Product;
use App\Repositories\Eloquent\FaqRepository;
use App\Repositories\Eloquent\ProductRepository;
use App\Repositories\Interfaces\FaqInterface;
use App\Repositories\Interfaces\ProductInterface;
use App\Services\Faq\FaqService;
use App\Services\Product\ProductService;

class ProductsController extends Controller
{
    /** @var ProductRepository */
    protected $productRepository;

    /** @var ProductService */
    protected $productService;

    public function __construct(ProductInterface $productRepository, ProductService $productService)
    {
        $this->productRepository = $productRepository;
        $this->productService = $productService;
    }

    public function all()
    {
        return response()->api(
            $this->productRepository->getProducts()
        );
    }

    public function save(ProductRequest $request, Product $product = null)
    {
        $model = $this->productService->make($product)->save($request->all());

        return response()->api([
            'data' => $product ? $model : $this->productRepository->getProducts(),
            'model' => $model
        ]);
    }

    public function saveSort(ProductRequest $request)
    {
        return response()->api(
            $this->productRepository->saveSort($request->get('sort'))
        );
    }

    public function remove(ProductRequest $request, Product $product)
    {
        return response()->api(
            $this->productService->make($product)->delete()
        );
    }
}