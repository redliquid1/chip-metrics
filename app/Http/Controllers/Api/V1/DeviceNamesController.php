<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\V1\DeviceNameRequest;
use App\Http\Requests\UserRegisterRequest;
use App\Models\DeviceName;
use App\Repositories\Eloquent\DeviceNamesRepository;
use App\Repositories\Interfaces\DeviceNamesInterface;
use App\Services\Device\DeviceNamesService;
use App\Services\User\UserAccountService;
use Auth;

class DeviceNamesController extends Controller
{
    /** @var DeviceNamesRepository */
    protected $deviceNamesRepository;

    /** @var DeviceNamesService */
    protected $deviceNamesService;

    public function __construct(DeviceNamesInterface $deviceNamesRepository, DeviceNamesService $deviceNamesService)
    {
        $this->deviceNamesRepository = $deviceNamesRepository;
        $this->deviceNamesService = $deviceNamesService;
    }

    public function getAll()
    {
        return response()->api(
            $this->deviceNamesRepository->all()
        );
    }

    public function save(DeviceNameRequest $request, DeviceName $deviceName = null)
    {
        $model = $this->deviceNamesService->make($deviceName)->save($request->all());

        return response()->api(
            $deviceName ? $model : $this->deviceNamesRepository->all()
        );
    }

    public function remove(DeviceNameRequest $request, DeviceName $deviceName)
    {
        return response()->api(
            $this->deviceNamesService->make($deviceName)->delete()
        );
    }
}