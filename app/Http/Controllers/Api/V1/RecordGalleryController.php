<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRegisterRequest;
use App\Repositories\Eloquent\RecordGalleryRepository;
use App\Services\RecordGallery\RecordGalleryService;
use App\Services\User\UserAccountService;
use Auth;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;

class RecordGalleryController extends Controller
{
    /** @var Guard */
    protected $guard;

    /** @var RecordGalleryService */
    protected $recordGalleryService;

    /** @var RecordGalleryRepository */
    protected $recordGalleryRepository;

    public function __construct(Guard $guard, RecordGalleryService $recordGalleryService, RecordGalleryRepository $recordGalleryRepository)
    {
        $this->guard = $guard;
        $this->recordGalleryService = $recordGalleryService;
        $this->recordGalleryRepository = $recordGalleryRepository;
    }

    public function save(Request $request)
    {
        return response()->api([
            'model' => $this->recordGalleryService->setRawData($request->all())->saveRawData()
        ]);
    }
}