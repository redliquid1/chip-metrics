<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Repositories\Eloquent\ProductRepository;
use App\Repositories\Interfaces\ProductInterface;

class ProductsController extends Controller
{
    /** @var ProductRepository */
    protected $productRepository;

    public function __construct(ProductInterface $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function index()
    {
        return view('site.products.index', [
            'data' => $this->productRepository->getProducts()
        ]);
    }

    public function show(Product $product)
    {
        return view('site.products.show', [
            'data' => $product
        ]);
    }
}
