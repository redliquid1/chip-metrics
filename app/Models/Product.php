<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';

    protected $fillable = [
        'language', 'title', 'content', 'sort', 'price', 'allegro_link'
    ];

    protected $appends = [
        'gallery'
    ];

    public function getGalleryAttribute()
    {
        return $this->hasMany(RecordGallery::class, 'model_id')
            ->where('gallery_name', 'gallery')
            ->where('model_name', get_class($this))
            ->orderBy('sort')
            ->get()
            ->each(function(RecordGallery $item) {
                $item->setAttribute('thumbnail', asset($item->getMiniature(100, 100)));
                $item->setAttribute('thumbnail_large', asset($item->getMiniature(600)));
            });
    }
}
