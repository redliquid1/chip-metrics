<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class i18nLanguage extends Model
{
    protected $table = 'i18n_languages';

    protected $fillable = [
        'name', 'locale'
    ];
}
