<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MeasureTemperature extends Model
{
    protected $table = 'measure_temperature';

    protected $fillable = [
        'gpio1', 'time_added'
    ];

    protected $dates = [
        'time_added'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function device()
    {
        return $this->belongsTo(Device::class);
    }
}
