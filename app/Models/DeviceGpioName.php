<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeviceGpioName extends Model
{
    protected $table = 'device_gpio_names';

    protected $fillable = [
        'user_id', 'device_id', 'gpio', 'name', 'color', 'ppu', 'type', 'tariff', 'price'
    ];

    public function device()
    {
        return $this->belongsTo(Device::class);
    }
}
