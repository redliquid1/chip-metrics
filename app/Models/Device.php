<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Device extends Model
{
    protected $table = 'devices';

    protected $fillable = [
        'chip_id', 'device_name_id', 'name', 'last_measurement'
    ];

    protected $with = [
        'deviceName'
    ];

    protected $casts = [
        'peripheral' => 'boolean'
    ];


    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function deviceName()
    {
        return $this->belongsTo(DeviceName::class);
    }

    public function gpios()
    {
        return $this->hasMany(DeviceGpioName::class)->orderBy('gpio', 'asc');
    }
}
