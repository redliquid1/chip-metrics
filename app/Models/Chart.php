<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Chart extends Model
{
    protected $table = 'charts';

    protected $fillable = [
        'user_id', 'name', 'data'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
