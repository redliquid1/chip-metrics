<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MeasurementType extends Model
{
    protected $table = 'measurement_type';

    protected $fillable = [
        'type', 'active'
    ];

    protected $casts = [
        'active' => 'boolean'
    ];

    public function deviceNames()
    {
        return $this->belongsToMany(DeviceName::class, 'device_name_measurement_type');
    }
}
