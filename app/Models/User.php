<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;
    use HasRoles;

    protected $guard_name = 'api';

    protected $appends = [
        'user_roles', 'small_avatar'
    ];

    protected $with = [
        'avatar'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'dashboard'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get user devices.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function devices()
    {
        return $this->hasMany(Device::class);
    }

    public function avatar()
    {
        return $this->hasOne(RecordGallery::class, 'model_id')
            ->where('gallery_name', 'avatar')
            ->where('model_name', get_class($this));
    }

    public function getSmallAvatarAttribute()
    {
        return $this->avatar ?
            asset($this->avatar->getMiniature(100, 100)) : null;
    }

    /**
     * Get full name.
     *
     * @return string
     */
    public function getFullNameAttribute()
    {
        return $this->name;
    }

    /**
     * Get user roles as simple array.
     *
     * @return \Illuminate\Support\Collection
     */
    public function getUserRolesAttribute()
    {
        return $this->roles()->pluck('name');
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
