<?php

namespace App\Models;

use App\Services\RecordGallery\RecordGalleryService;
use Illuminate\Database\Eloquent\Model;

class RecordGallery extends Model
{
    protected $table = 'record_gallery';
    protected $fillable = [
        'gallery_name', 'model_name', 'model_id', 'sort', 'file_name', 'title', 'description', 'data'
    ];

    protected $appends = [
        'url'
    ];

    public function getUrlAttribute()
    {
        $model = (new RecordGalleryService())->make($this)->getModelNameSimple();
        return asset('uploads/'.$model.'/'.$this->gallery_name.'/'.$this->file_name);
    }

    public function getMiniature($width = false, $height = false)
    {
        return (new RecordGalleryService())->make($this)->getMiniature($width, $height);
    }
}
