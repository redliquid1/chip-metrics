<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class i18nModule extends Model
{
    protected $table = 'i18n_modules';

    protected $fillable = [
        'name'
    ];
}
