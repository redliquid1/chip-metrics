<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MeasurePowerSample extends Model
{
    protected $table = 'measure_power_samples';

    protected $fillable = [
        'gpio1', 'gpio2', 'gpio3', 'gpio4', 'gpio5', 'gpio6', 'time_added'
    ];

    protected $dates = [
        'time_added'
    ];
}
