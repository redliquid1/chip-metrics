<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class i18nKey extends Model
{
    const TYPE_SIMPLE = 1;
    const TYPE_COMPLEX = 2;

    protected $table = 'i18n_keys';

    protected $fillable = [
        'module_id', 'key', 'type'
    ];

    public function module()
    {
        return $this->belongsTo(i18nModule::class);
    }
}
