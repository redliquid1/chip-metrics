<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StaticSite extends Model
{
    protected $table = 'static_sites';

    protected $fillable = [
        'language', 'name', 'content'
    ];
}
