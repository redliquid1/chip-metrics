<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MeasurePower extends Model
{
    protected $table = 'measure_power';

    protected $fillable = [
        'gpio1', 'gpio2', 'gpio3', 'gpio4', 'gpio5', 'gpio6', 'time_added'
    ];

    protected $dates = [
        'time_added'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function device()
    {
        return $this->belongsTo(Device::class);
    }
}
