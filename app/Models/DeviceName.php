<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeviceName extends Model
{
    protected $table = 'device_names';

    protected $fillable = [
        'identifier', 'name'
    ];

    protected $appends = [
        'measurement_types_ids'
    ];

    public function measurementTypes()
    {
        return $this->belongsToMany(MeasurementType::class, 'device_name_measurement_type');
    }

    public function getMeasurementTypesIdsAttribute()
    {
        return $this->measurementTypes()->pluck('id')->toArray();
    }
}
