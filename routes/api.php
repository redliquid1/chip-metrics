<?php

$api = app('Dingo\Api\Routing\Router');
$api->version('v1', ['prefix' => 'api/v1', 'namespace' => 'App\Http\Controllers\Api\V1', 'middleware' => ['encrypt.cookies', 'start.session', 'bindings']], function ($api) {
    $api->get('/time', 'TimeController@getTime');

    $api->group(['middleware' => ['check.token', 'api.auth']], function ($api) {
        $api->group(['prefix' => '/faq'], function ($api) {
            $api->get('/', 'FaqController@getAll');
            $api->post('/', 'FaqController@save');
            $api->post('/save-sort', 'FaqController@saveSort');
            $api->put('/{faq}', 'FaqController@save');
            $api->delete('/{faq}', 'FaqController@remove');
        });
        $api->group(['prefix' => '/static-sites'], function ($api) {
            $api->get('/', 'StaticSitesController@all');
            $api->post('/', 'StaticSitesController@save');
            $api->put('/{staticSite}', 'StaticSitesController@save');
            $api->delete('/{staticSite}', 'StaticSitesController@remove');
        });
        $api->group(['prefix' => '/device-names'], function ($api) {
            $api->get('/', 'DeviceNamesController@getAll');
            $api->post('/', 'DeviceNamesController@save');
            $api->put('/{deviceName}', 'DeviceNamesController@save');
            $api->delete('/{deviceName}', 'DeviceNamesController@remove');
        });
        $api->group(['prefix' => '/record-gallery'], function ($api) {
            $api->post('/save', 'RecordGalleryController@save');
        });
        $api->group(['prefix' => '/products'], function ($api) {
            $api->get('/', 'ProductsController@all');
            $api->post('/', 'ProductsController@save');
            $api->post('/save-sort', 'ProductsController@saveSort');
            $api->put('/{product}', 'ProductsController@save');
            $api->delete('/{product}', 'ProductsController@remove');
        });
        $api->group(['prefix' => '/measurement-types'], function ($api) {
            $api->get('/', 'MeasurementTypesController@all');
            $api->get('/user', 'MeasurementTypesController@user');
        });
    });

    $api->group(['prefix' => '/translations'], function ($api) {
        $api->get('/all', 'TranslationsController@all');
        $api->group(['middleware' => ['check.token', 'api.auth']], function ($api) {
            $api->get('/modules', 'TranslationsController@modules');
            $api->get('/datatables', 'TranslationsController@datatables');
            $api->get('/languages', 'TranslationsController@languages');
            $api->post('/set-translation', 'TranslationsController@setTranslation');
            $api->post('/change-key', 'TranslationsController@changeKey');
            $api->post('/save', 'TranslationsController@save');
            $api->post('/save-modules', 'TranslationsController@saveModules');
            $api->post('/key-exist', 'TranslationsController@keyExist');
            $api->delete('/{key}', 'TranslationsController@remove');

            $api->get('/import-data', 'TranslationsController@importData');
            $api->get('/export-data', 'TranslationsController@exportData');
        });
    });

    $api->group(['prefix' => '/user'], function ($api) {
        $api->get('/check-token/{token}', 'UserController@checkToken');
        $api->get('/refresh-token/{token}', 'UserController@refreshToken');
        $api->put('/', 'UserController@save');
        $api->post('/register', 'UserController@register');
        $api->post('/login', 'UserController@login');
        $api->post('/is-email-unique', 'UserController@isEmailUnique');
        $api->group(['middleware' => ['api.auth', 'check.token']], function ($api) {
            $api->get('/logout', 'UserController@logout');
            $api->get('/device-names', 'UserController@getAllDeviceNames');
            $api->get('/device-types', 'UserController@getAllDeviceTypes');
            $api->post('/save-avatar/{user?}', 'UserController@saveAvatar');
            $api->group(['prefix' => '/devices'], function ($api) {
                $api->group(['prefix' => '/gpios'], function ($api) {
                    $api->get('/all', 'DevicesController@getAllUserGpios');
                    $api->get('/{device}', 'DevicesController@getGpios');
                    $api->put('/{device}', 'DevicesController@saveGpios');
                });
                $api->get('/', 'DevicesController@getDevices');
                $api->get('/is-available', 'DevicesController@isDeviceAvailable');
                $api->post('/add', 'DevicesController@addDevice');
                $api->get('/{device}/export-data', 'DevicesController@exportData');
                $api->delete('/{device}', 'DevicesController@removeDevice');
                $api->put('/{device}', 'DevicesController@updateData');
            });
            $api->group(['prefix' => '/charts'], function ($api) {
                $api->get('/', 'ChartsController@getAll');
                $api->post('/', 'ChartsController@saveChart');
                $api->put('/save-layout', 'ChartsController@saveLayout');
                $api->put('/{chart}', 'ChartsController@saveChart');
                $api->delete('/{chart}', 'ChartsController@removeChart');
            });

            $api->group(['prefix' => '/system-devices', 'middleware' => ['role:super-admin']], function ($api) {
                $api->get('/', 'SystemDevicesController@getAll');
                $api->post('/', 'SystemDevicesController@save');
                $api->put('/{device}', 'SystemDevicesController@save');
                $api->delete('/{device}', 'SystemDevicesController@remove');
            });

            $api->group(['prefix' => '/users', 'middleware' => ['role:super-admin']], function ($api) {
                $api->get('/', 'UserController@getAll');
                $api->put('/', 'UserController@saveUser');
                $api->post('/{user}', 'UserController@saveUser');
                $api->delete('/{user}', 'UserController@removeUser');
                $api->get('/impersonate/{user}', 'UserController@impersonate');
            });

        });

        // measurements
        $api->group(['prefix' => '/measurement', 'namespace' => 'Measurement'], function ($api) {

            $api->get('/generic', 'GenericController@getMeasurement');

            $api->get('/power', 'PowerController@getPower');
            $api->post('/power', 'PowerController@add');
            $api->post('/power-archive', 'PowerController@addArchive');

            $api->get('/temperature', 'TemperatureController@getTemperature');
            $api->post('/temperature', 'TemperatureController@add');
            $api->post('/temperature-archive', 'TemperatureController@addArchive');
        });

    });
});
