<?php


Route::get('/', 'HomeController@index')->name('home');

Route::get('/pass', function () {
    return \Hash::make('admin');
})->name('pass');

Route::post('/register', 'Auth\UserController@register');

Route::get('/login', 'Auth\LoginController@login')->name('login');
Route::get('/logout', 'Auth\LoginController@logout')->name('logout');
Route::post('/login', 'Auth\LoginController@loginPost');

Route::get('/reset-password', 'Auth\ResetPasswordController@login')->name('password.request');

Route::group(['prefix' => '/app'], function() {
    Route::get('/{any?}', 'AppController@index')->name('app.index');
});

Route::get('/faq', 'FaqController@index')->name('site.faq.index');
Route::group(['prefix' => '/produkty'], function() {
    Route::get('/', 'ProductsController@index')->name('site.products.index');
    Route::get('/{product},{title}', 'ProductsController@show')->name('site.products.show');
});


// Route::get('/login', 'ImageController@getThumbnail')->name('image-thumbnail');
