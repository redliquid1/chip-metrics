@extends('site._layout')

@section('content')
    <div class="page-header">
        <h1 class="page-title">
            Czym jest Chip-Metrics?
        </h1>
    </div>
    <div>
        {!! $homePage ? $homePage->content : '' !!}
    </div>
@endsection