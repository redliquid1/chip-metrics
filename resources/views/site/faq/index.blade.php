@extends('site._layout')

@section('content')
    <div class="page-header">
        <h1 class="page-title">
            FAQ, czyli najczęściej zadawane pytania
        </h1>
    </div>
    <div>
        <div class="accordion" id="faq">
            @foreach($data as $faq)
                <div class="card mb-0">
                    <div class="card-header" id="heading-{{$faq->id}}">
                        <h5 class="mb-0">
                            <button class="btn btn-link pl-0" type="button" data-toggle="collapse" data-target="#collapse-{{$faq->id}}" aria-expanded="true" aria-controls="collapse-{{$faq->id}}">
                               {{ $faq->title }}
                            </button>
                        </h5>
                    </div>
                    <div id="collapse-{{$faq->id}}" class="collapse" aria-labelledby="heading-{{$faq->id}}" data-parent="#faq">
                        <div class="card-body">
                            {!! $faq->content !!}
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection