@extends('site._layout')

@section('content')
    <div class="page-header">
        <h1 class="page-title">
            Nasze produkty
        </h1>
    </div>
    <div>
        <div class="row">
            @foreach($data as $k => $product)
                <div class="@if ($k < 3) col-md-6 @else col-md-4 @endif ">
                    <div class="card">
                        <div class="card-body">
                            @if (isset($product->gallery[0]))
                                <div class="mb-4 text-center">
                                    <a href="{{route('site.products.show', ['product' => $product->id, 'title' => str_slug($product->title)])}}"
                                       title="{{ $product->title }}">
                                        <img src="{{$product->gallery[0]->thumbnail_large}}" alt="{{ $product->title }}" class="img-fluid" />
                                    </a>
                                </div>
                            @endif
                            <h4 class="card-title">
                                <a href="{{route('site.products.show', ['product' => $product->id, 'title' => str_slug($product->title)])}}">
                                    {{  $product->title }}
                                </a>
                            </h4>
                            <!--
                            <div class="mt-5 d-flex align-items-center">
                                <div class="product-price">
                                    <strong>$499</strong>
                                </div>
                                <div class="ml-auto">
                                    <a href="javascript:void(0)" class="btn btn-primary"><i class="fe fe-plus"></i> Add to cart</a>
                                </div>
                            </div>
                            -->
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection