@extends('site._layout')

@section('content')
    <div class="page-header">
        <h1 class="page-title">
            {{ $data->title }}
        </h1>
    </div>
    <div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        {!! $data->content !!}
                        @if ($data->gallery->count())
                            <h4>Galeria</h4>
                            <div class="row row-cards">
                                @foreach($data->gallery as $image)
                                    <div class="col-sm-6 col-lg-3">
                                        <div class="card p-3">
                                            <a href="{{asset($image->url)}}" data-fancybox="gallery">
                                                <img src="{{$image->thumbnail_large}}" alt="" class="rounded img-responsive">
                                            </a>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection