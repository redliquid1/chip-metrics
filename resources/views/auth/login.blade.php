@extends('auth._layout')

@section('content')
    <div class="row">
        <div class="col col-login mx-auto">
            <div class="text-center mb-6">
                <p>APP NAME</p>
            </div>
            <form class="card" method="POST" action="" aria-label="{{ __('Login') }}">
                <div class="card-body p-6">
                    @csrf
                    <div class="card-title">{{ __('Login') }}</div>
                    <div class="form-group">
                        <label class="form-label">{{ __('E-Mail Address') }}</label>
                        <input name="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                               id="email" aria-describedby="emailHelp" placeholder="Enter email"
                               value="{{ old('email') }}" required autofocus />

                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label class="form-label">
                            {{ __('Password') }}
                            <a href="{{ route('password.request') }}" class="float-right small">{{ __('Forgot Your Password?') }}</a>
                        </label>
                        <input name="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                               id="exampleInputPassword1" placeholder="Password" required />

                        @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}/>
                            <span class="custom-control-label">{{ __('Remember Me') }}</span>
                        </label>
                    </div>
                    <div class="form-footer">
                        <button type="submit" class="btn btn-primary btn-block">{{ __('Login') }}</button>
                    </div>
                </div>
            </form>
            <div class="text-center text-muted">
                Don't have account yet? <a href="./register.html">Sign up</a>
            </div>
        </div>
    </div>

@endsection
