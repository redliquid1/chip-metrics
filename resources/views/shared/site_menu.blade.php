@inject('products', 'App\Repositories\Interfaces\ProductInterface')

<div class="row align-items-center">
    <div class="col-lg order-lg-first">
        <ul class="nav nav-tabs border-0 flex-column flex-lg-row">
            <li class="nav-item">
                <a href="/" class="nav-link @if (\Request::is('/')) active @endif"><i class="fe fe-home"></i> Strona domowa</a>
            </li>
            @if ($products->getProducts()->count())
                <li class="nav-item">
                    <a href="{{route('site.products.index')}}" class="nav-link @if (\Request::is('produkty*')) active @endif"><i class="fe fe-grid"></i> Produkty</a>
                </li>
            @endif
            <li class="nav-item">
                <a href="{{route('site.faq.index')}}" class="nav-link @if (\Request::is('faq*')) active @endif"><i class="fe fe-help-circle"></i> FAQ</a>
            </li>
        </ul>
    </div>
</div>