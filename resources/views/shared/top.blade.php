<div class="d-flex">
    <a class="header-brand" href="/">
        <img src="{{ asset('modules/shared/images/logo/chip-metrics.png') }}" class="header-brand-img" alt="CHIP METRICS">
    </a>
    <div class="d-flex order-lg-2 ml-auto">
        <div class="dropdown">
            <a @if (Auth::user()) href data-toggle="dropdown" @else href="/app" @endif class="nav-link pr-0 leading-none">
                <span class="avatar" style="background-image: url({{ Auth::user() ? Auth::user()->small_avatar : '/modules/shared/images/default-avatar.png' }})"></span>
                <span class="ml-2 d-none d-lg-block">
                    <span class="text-default">{{ Auth::user() ? Auth::user()->full_name : trans('global.top.helloGuest') }}</span>
                    @if (Auth::user())
                        <small class="text-muted d-block mt-1">
                            @if (Auth::user()->hasRole('super-admin'))
                                {{ trans('global.role.admin') }}
                            @else
                                {{ trans('global.role.client') }}
                            @endif
                        </small>
                    @endif
                </span>
            </a>
            @if (Auth::user())
                <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                    <a class="dropdown-item" href="/app">
                        <i class="dropdown-icon fe fe-settings"></i> {{ trans('global.menu.myDashboard') }}
                    </a>
                    <a class="dropdown-item" href="/app#/my-devices">
                        <i class="dropdown-icon fe fe-layers"></i> {{ trans('global.menu.myDevices') }}
                    </a>
                    <a class="dropdown-item" href="/app#/profile">
                        <i class="dropdown-icon fe fe-user"></i> {{ trans('global.menu.profile') }}
                    </a>
                    <a class="dropdown-item" class="logout" href="{{route('logout')}}">
                        <i class="dropdown-icon fe fe-log-out"></i> {{ trans('global.menu.logout') }}
                    </a>
                </div>
            @endif
        </div>
    </div>
    <a href="#" class="header-toggler d-lg-none ml-3 ml-lg-0" data-toggle="collapse" data-target="#headerMenuCollapse">
        <span class="header-toggler-icon"></span>
    </a>
</div>