<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6">
                Wszystkie prawa zastrzeżone © 2018 {{ date('Y') > 2018 ? '- '.date('Y') : '' }} <a href="{{ URL::to('/') }}">{{ $_SERVER['HTTP_HOST'] }}</a>
            </div>
            <div class="col-auto col-md-6 text-right">
                Layout na bazie <a href="https://tabler.io" target="_blank">tabler.io</a>
            </div>
        </div>
    </div>
</footer>