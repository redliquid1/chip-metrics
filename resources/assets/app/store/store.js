import Vue from 'vue';
import Vuex from 'vuex';
import user from './modules/user'
import indicators from './modules/indicators'
import measurementGeneric from './modules/measurements/generic'
import devices from './modules/devices'
import charts from './modules/charts'
import faq from './modules/faq'
import staticSites from './modules/staticSites'
import rowGallery from './modules/rowGallery'
import products from './modules/products'
import deviceNames from './modules/deviceNames'
import asyncWait from './modules/async-wait/asyncWait'
import translations from './modules/translations/translations'
import visual from './modules/editors/visual'

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        user, indicators, measurementGeneric, devices, charts, faq, staticSites, rowGallery, products, deviceNames,
        asyncWait, translations, visual
    }
});