import TranslationService from "../../../services/Translations/TranslationService";
import FaqService from "../../../services/FaqService";

const state = {
    datatablesConfiguration: {
        rowClick: (datatables, data) => {

        },
        ajax: (data, callback, settings) => {
            data.module_id = this.a.state.selectedModule;
            this.a.actions.inDatatables(data).then((result) => {
                state.inDatatables = result.data.data;
                callback(result.data.data);
            });
        },
        // order: [[0, 'asc']],
        processing: true,
        serverSide: true,
        dom: "Bfrtip",
        columnDefs: [
            {
                "targets": 0,
                "orderable": false
            }, {
                "targets": 1,
                "orderable": false
            }, {
                "targets": 2,
                "orderable": false
            }, {
                "targets": 3,
                "orderable": false,
                "width": "80px"
            }
        ],
        searchDelay: 500
    },
    data: [],
    modules: [],
    selectedModule: 0,
    current: {},
    inDatatables: {},
    currentModule: {},
    languages: []
};

const actions = {
    data({commit}) {
        return TranslationService.all().then(result => {
            commit('data', result.data.data);
            return result;
        }).catch(() => {
            return false;
        });
    },
    modules({commit}) {
        return TranslationService.modules().then(result => {
            commit('modules', result.data.data);
            return result;
        }).catch(() => {
            return false;
        });
    },
    languages({commit}) {
        return TranslationService.languages().then(result => {
            commit('languages', result.data.data);
            return result;
        }).catch(() => {
            return false;
        });
    },
    setTranslation({commit}, data) {
        return TranslationService.setTranslation(data).then(result => {
            return result.data;
        }).catch(() => {
            return false;
        });
    },
    changeKey({commit}, data) {
        return TranslationService.changeKey(data).then(result => {
            return result.data;
        }).catch(() => {
            return false;
        });
    },
    save({commit}, data) {
        return TranslationService.save(data).then(result => {
            return result.data;
        }).catch(() => {
            return false;
        });
    },
    saveModules({commit}, data) {
        return TranslationService.saveModules(data).then(result => {
            commit('modules', result.data.data);
            return result.data;
        }).catch(() => {
            return false;
        });
    },
    inDatatables(query) {
        return TranslationService.datatables(query).then(result => {
            return result;
        }).catch((e) => {
            return false;
        });
    },
    remove({commit}, translation) {
        return TranslationService.remove(translation).then(result => {
            return result.data;
        }).catch((e) => {
            return e;
        });
    },
    importData({commit}) {
        return TranslationService.importData().then(result => {
            return result.data;
        }).catch((e) => {
            return e;
        });
    },
    exportData({commit}) {
        return TranslationService.exportData().then(result => {
            return result.data;
        }).catch((e) => {
            return e;
        });
    },
};


const mutations = {
    data(state, data) {
        state.data = data;
        for (var locale in data) {
            window.vueApp.$i18n.setLocaleMessage(locale, data[locale])
        }
    },
    current(state, data) {
        state.current = data;
    },
    modules(state, data) {
        state.modules = data;
    },
    languages(state, data) {
        state.languages = data;
    },
    selectedModule(state, data){
        state.selectedModule = data;
    },
    inDatatables(state, data){
        state.inDatatables = data;
    },
    remove(state, data) {
        state.data.splice(state.data.map(e => e.id).indexOf(data.id), 1);
    }
};

const getters = {
    data(state) {
        return state.data;
    },
    current(state) {
        return state.current;
    },
    modules(state) {
        return state.modules;
    },
    languages(state) {
        return state.languages;
    },
    selectedModule(state) {
        return state.selectedModule;
    },
    inDatatables(state) {
        return state.inDatatables;
    },
    datatablesConfiguration(state) {
        return state.datatablesConfiguration;
    }
};

export default {
    state,
    mutations,
    actions,
    getters,
    namespaced: true
}
