import * as moment from 'moment';
import GenericService from '../../../services/Measurements/GenericService';

const state = {
    measurements: {},

    deviceIds: [],
    chartPeriod: 'monthly',
    includeCurrentPeriod: 1,
    periodTime: 4,
    measurementTypeId: null,

    measurementCache: {}
};

const getters = {
    measurements(state) {
        return state.measurements;
    }
};


const actions = {
    getMeasurements({commit, state}, params) {

        var deviceIds = [];
        params.selectedGpios.forEach((e) => {
            deviceIds.push(e.device_id);
        });
        params.deviceIds = deviceIds.filter((v, i, a) => a.indexOf(v) === i);

        // keep only certain fields.
        var params = deepCopy(params);
        params = {
            deviceIds: params.deviceIds,
            chartPeriod: params.chartPeriod,
            includeCurrentPeriod: params.includeCurrentPeriod,
            periodTime: params.periodTime,
            measurementTypeId: params.measurementTypeId
        };

        function getHash(params) {
            var t = JSON.stringify(params.deviceIds) +
                JSON.stringify(params.chartPeriod) +
                JSON.stringify(params.includeCurrentPeriod) +
                JSON.stringify(params.measurementTypeId) +
                JSON.stringify(params.periodTime);
            return t.hashCode();
        }

        var hash = getHash(params);

        if (typeof state.measurementCache[hash] !== 'undefined') {
            return new Promise(function(resolve){
                return resolve(state.measurementCache[hash]);
            });
        }

        state.deviceIds = params.deviceIds;
        state.chartPeriod = params.chartPeriod;
        state.includeCurrentPeriod = params.includeCurrentPeriod;
        state.periodTime = params.periodTime;
        state.measurementTypeId = params.measurementTypeId;

        return GenericService.getMeasurements(params).then(data => {
            commit('storeMeasurements', data.data);
            commit('storeMeasurementsCache', {
                data: data.data,
                hash: hash
            });
            return data.data;
        }).catch(() => {
            return false;
        });
    },

};

const mutations = {
    storeMeasurements(state, data) {
        state.measurements = data;
    },
    storeMeasurementsCache(state, data) {
        state.measurementCache[data.hash] = data.data;
    }
};

export default {
    state,
    mutations,
    actions,
    getters,
    namespaced: true
}