import StaticSitesService from "../../services/StaticSitesService";

const state = {
    data: [],
    current: {}
};

const actions = {
    data({commit}) {
        return StaticSitesService.all().then(result => {
            commit('data', result.data.data);
            return result;
        }).catch(() => {
            return false;
        });
    },
    save({state, commit}) {
        return StaticSitesService.save(state.current).then(result => {
            commit(state.current.id ? 'row' : 'data', result.data.data);
            return result.data;
        }).catch((e) => {
            return e;
        });
    },
    remove({commit}) {
        return StaticSitesService.remove(state.current).then(result => {
            commit('remove', state.current);
            return result.data;
        }).catch((e) => {
            return e;
        });
    },
};


const mutations = {
    data(state, data) {
        if (data) {
            state.data = data;
        }
    },
    current(state, data) {
        state.current = data;
    },
    row(state, data) {
        Object.assign(state.data.find(e => e.id === data.id), data);
    },
    remove(state, data) {
        state.data.splice(state.data.map(e => e.id).indexOf(data.id), 1);
    }
};

const getters = {
    data(state) {
        return state.data;
    },
    current(state) {
        return state.current;
    }
};

export default {
    state,
    mutations,
    actions,
    getters,
    namespaced: true
}