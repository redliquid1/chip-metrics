const state = {
    text: ''
};

const actions = {
    text({commit}, text) {
        commit('text', text);
    }
};

const mutations = {
    text(state, data) {
        state.text = data;
    }
};

const getters = {
    text(state) {
        return state.text;
    }
};

export default {
    state,
    mutations,
    actions,
    getters,
    namespaced: true
}