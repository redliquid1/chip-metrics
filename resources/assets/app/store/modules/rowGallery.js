const state = {
    galleries: {}
};

const actions = {

};


const mutations = {
    galleries(state, data) {
        state.galleries[data.name] = data.data;
        state.galleries = deepCopy(state.galleries); // force computed property to update.
    },
    remove(state, data) {
        state.galleries[data.name].splice(data.index, 1);
        state.galleries = deepCopy(state.galleries); // force computed property to update.
    },
    add(state, data) {
        state.galleries[data.name].push(data.data);
        state.galleries = deepCopy(state.galleries); // force computed property to update.
    }
};

const getters = {
    galleries(state) {
        return state.galleries;
    }
};

export default {
    state,
    mutations,
    actions,
    getters,
    namespaced: true
}