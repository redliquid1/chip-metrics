import DeviceNamesService from "../../services/DeviceNames";
import FaqService from "../../services/FaqService";

const state = {
    data: [],
    current: {}
};

const actions = {
    data({commit}) {
        return DeviceNamesService.getAll().then(result => {
            commit('data', result.data.data);
            return result;
        }).catch(() => {
            return false;
        });
    },
    save({state, commit}) {
        return DeviceNamesService.save(state.current).then(result => {
            commit(state.current.id ? 'row' : 'data', result.data.data);
            return result.data;
        }).catch((e) => {
            return e;
        });
    },
    remove({commit}) {
        return DeviceNamesService.remove(state.current).then(result => {
            commit('remove', state.current);
            return result.data;
        }).catch((e) => {
            return e;
        });
    },
};


const mutations = {
    data(state, data) {
        state.data = data;
    },
    row(state, data) {
        Object.assign(state.data.find(e => e.id === data.id), data);
    },
    current(state, data) {
        state.current = data;
    }
};

const getters = {
    data(state) {
        return state.data;
    },
    current(state) {
        return state.current;
    }
};

export default {
    state,
    mutations,
    actions,
    getters,
    namespaced: true
}