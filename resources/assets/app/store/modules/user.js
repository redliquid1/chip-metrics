import UserService from "../../services/UserService";
import RecordGalleryService from "../../services/RecordGalleryService";

import apis from "../../configs/apis";
import * as moment from 'moment';

const state = {
    user: {},
    register: {},
    token: '',
    refreshToken: null,
    devices: [],
    users: [],
    editUser: {},
    rememberMe: false
};

const actions = {
    login({commit}, credentials) {
        state.rememberMe = credentials.rememberMe;
        return UserService.login(credentials).then(data => {
            commit('storeUser', data.data);
            return true;
        }).catch(() => {
            return false;
        });
    },
    logout({commit}, data) {
        this._vm.$cookie.delete('authToken');

        if (data && data.force) {
            commit('clearUser');
            UserService.logout();
            window.location.href = apis.my.domain;
            return true;
        }
        else {
            return UserService.logout().then(result => {
                commit('clearUser');
                if (result.data.impersonated_from) {
                    commit('storeUser', result.data.impersonated_from);
                }
                return true;
            }).catch(() => {
                commit('clearUser');
                return false;
            });
        }
    },
    register({commit}, data) {
        return UserService.register(data).then(response => {
            commit('storeUser', response.data.data);
            return true;
        }).catch(() => {
            return false;
        });
    },
    checkToken({commit}, token) {
        return UserService.checkToken(token).then(data => {
            return data.data;
        }).catch(() => {
            return false;
        });
    },
    refreshToken({commit, state}) {
        return UserService.refreshToken(state.token).then(response => {
            commit('storeUser', response.data);
            return response.data;
        }).catch(() => {
            return false;
        });
    },
    save({commit}, data) {
        return UserService.save(data).then(response => {
            commit('storeUser', response.data.data);
            return true;
        }).catch(() => {
            return false;
        });
    },
    users({commit}) {
        return UserService.users().then(response => {
            commit('storeUsers', response.data.data);
            return true;
        }).catch(() => {
            return false;
        });
    },
    removeUser({commit}, user) {
        return UserService.deleteUser(user).then(response => {
            return response.data;
        }).catch(() => {
            return false;
        });
    },
    saveUser({commit}, data) {
        return UserService.saveUser(data).then(response => {
            return response.data;
        }).catch(() => {
            return false;
        });
    },
    impersonate({commit}, user) {
        return UserService.impersonate(user).then(response => {
            commit('storeUser', response.data.data);
            return true;
        }).catch(() => {
            return false;
        });
    },
    saveAvatar({commit, state}, gallery) {
        let o = {
            gallery: gallery,
            gallery_name: 'avatar',
            model_name: 'App\\Models\\User',
            model_id: state.user.id,
        };

        return RecordGalleryService.save(o).then(response => {
            commit('storeUserOnly', response.data.data.model);
            return response.data;
        }).catch(() => {
            return false;
        });
    },
};

const mutations = {
    storeUser(state, userData) {
        if (userData) {
            state.user = userData.user;
            if (typeof userData.token !== 'undefined') {
                state.token = userData.token;
            }

            if (typeof state.user.dashboard === "string"){
                state.user.dashboard = JSON.parse(state.user.dashboard);
            }

            localStorage.setItem('user', JSON.stringify(userData.user));
            localStorage.setItem('token', state.token);

            // this._vm.$cookie.set('authToken', userData.token, '1Y');
        }
    },
    clearUser(state) {
        state.user = null;
        state.token = null;
        localStorage.removeItem('user');
        localStorage.removeItem('token');
    },
    setNavigationMenu(state, menu) {
        state.navigation.menu = menu;
    },
    storeUserOnly(state, user) {
        // localStorage.setItem('user', JSON.stringify(user));
        if (typeof user.dashboard === "string"){
            user.dashboard = JSON.parse(user.dashboard);
        }
        state.user = user;
        localStorage.setItem('user', JSON.stringify(user));
    },
    storeUsers(state, users) {
        state.users = users;
    },
    storeEditUser(state, user) {
        state.editUser = user;
    }
};

const getters = {
    user(state) {
        return state.user;
    },
    register(state) {
        return state.register;
    },
    token(state) {
        return state.token;
    },
    isAuthenticated(state) {
        return (state.user && state.user.id);
    },
    navigation(state) {
        return state.navigation;
    },
    refreshToken(state) {
        return state.refreshToken;
    },
    devices(state) {
        return state.devices;
    },
    users(state) {
        return state.users;
    },
    editUser(state) {
        return state.editUser;
    },
    rememberMe(state) {
        return state.rememberMe;
    }
};

export default {
    state,
    mutations,
    actions,
    getters,
    namespaced: true
}