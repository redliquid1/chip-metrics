const state = {
    promises: []
};

const actions = {
    getData({commit}, measurementTypeId) {
        return UserDevicesService.getAllUserGpios(measurementTypeId).then(result => {
            commit('allGpios', result.data.data);
            return result.data;
        }).catch(() => {
            return false;
        });
    }
};


const mutations = {
    currentChart(state, data) {
        state.currentChart = data;
    }
};

const getters = {
    allMeasurementTypes(state) {
        return state.allMeasurementTypes;
    }
};

export default {
    state,
    mutations,
    actions,
    getters,
    namespaced: true
}