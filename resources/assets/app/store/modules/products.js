import ProductsService from "../../services/ProductsService";
import RecordGalleryService from "../../services/RecordGalleryService";

const state = {
    data: [],
    current: {}
};

const actions = {
    data({commit}) {
        return ProductsService.all().then(result => {
            commit('data', result.data.data);
            return result;
        }).catch(() => {
            return false;
        });
    },
    save({state, commit}) {
        return ProductsService.save(state.current).then(result => {
            commit(state.current.id ? 'row' : 'data', result.data.data.data);
            state.current = result.data.data.model;
            return result.data;
        }).catch((e) => {
            return e;
        });
    },
    remove({commit}) {
        return ProductsService.remove(state.current).then(result => {
            commit('remove', state.current);
            return result.data;
        }).catch((e) => {
            return e;
        });
    },
    saveSort({commit}) {
        return ProductsService.saveSort(state.data).then(result => {
            return result.data;
        }).catch((e) => {
            return e;
        });
    },
    saveGallery({commit}, gallery) {
        let o = {
            gallery: gallery ? gallery : [],
            gallery_name: 'gallery',
            model_name: 'App\\Models\\Product',
            model_id: typeof state.current.id === 'undefined' ? false : state.current.id,
        };

        return RecordGalleryService.save(o).then(response => {
            commit('row', response.data.data.model);
            return response.data;
        }).catch(() => {
            return false;
        });
    },
};


const mutations = {
    data(state, data) {
        state.data = data;
    },
    current(state, data) {
        state.current = data;
    },
    row(state, data) {
        Object.assign(state.data.find(e => e.id === data.id), data);
    },
    remove(state, data) {
        state.data.splice(state.data.map(e => e.id).indexOf(data.id), 1);
    }
};

const getters = {
    data(state) {
        return state.data;
    },
    current(state) {
        return state.current;
    }
};

export default {
    state,
    mutations,
    actions,
    getters,
    namespaced: true
}