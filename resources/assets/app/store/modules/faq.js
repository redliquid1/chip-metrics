import FaqService from "../../services/FaqService";

const state = {
    data: [],
    current: {},
};

const actions = {
    data({commit}) {
        return FaqService.getAll().then(result => {
            commit('data', result.data.data);
            return result;
        }).catch(() => {
            return false;
        });
    },
    save({state, commit}) {
        return FaqService.save(state.current).then(result => {
            commit(state.current.id ? 'row' : 'data', result.data.data);
            return result.data;
        }).catch((e) => {
            return e;
        });
    },
    remove({commit}) {
        return FaqService.remove(state.current).then(result => {
            commit('remove', state.current);
            return result.data;
        }).catch((e) => {
            return e;
        });
    },
    saveSort({commit}) {
        return FaqService.saveSort(state.data).then(result => {
            return result.data;
        }).catch((e) => {
            return e;
        });
    }
};


const mutations = {
    data(state, data) {
        if (data) {
            state.data = data;
        }
    },
    current(state, data) {
        state.current = data;
    },
    row(state, data) {
        Object.assign(state.data.find(e => e.id === data.id), data);
    },
    remove(state, data) {
        state.data.splice(state.data.map(e => e.id).indexOf(data.id), 1);
    }
};

const getters = {
    data(state) {
        return state.data;
    },
    current(state) {
        return state.current;
    }
};

export default {
    state,
    mutations,
    actions,
    getters,
    namespaced: true
}