import UserDevicesService from "../../services/UserDevicesService";
import UserMeasurementTypesService from "../../services/UserMeasurementTypesService";
import ChartService from "../../services/ChartService";

const state = {
    currentChart: {
        id: null,
        selectedGpios: [],
        chartPeriod: 'monthly',
        chartType: 'bar',
        includeDeviceName: 1,
        includeCurrentPeriod: 1,
        periodTime: 4
    },
    info: {
        periodTimeDescripton: 'miesięcy',
        includeCurrentPeriodDescription: 'miesiąc'
    },
    allGpios: [],
    allDeviceNames: [],
    userDeviceTypes: [],
    userMeasurementTypes: [],
    allMeasurementTypes: [],
    charts: [],
    layout: [],
    editItemRef: {},
};

const actions = {
    getAllUserGpios({commit}, measurementTypeId) {
        return UserDevicesService.getAllUserGpios(measurementTypeId).then(result => {
            commit('allGpios', result.data.data);
            return result.data;
        }).catch(() => {
            return false;
        });
    },
    allDeviceNames({commit}, measurementType = 0) {
        return UserDevicesService.getAllUserDeviceNames(measurementType).then(result => {
            commit('allDeviceNames', result.data.data);
            return result.data;
        }).catch(() => {
            return false;
        });
    },
    saveChart({commit}, data) {
        return ChartService.saveChart(data).then(result => {
            commit('chart', result.data.data);
            return result.data;
        }).catch((e) => {
            return e;
        });
    },
    removeChart({commit, state}, item) {
        return ChartService.deleteChart(item.chartId).then(result => {
            var layout = deepCopy(state.layout);
            layout = Object.keys(layout).map(function (key) { return layout[key]; });

            var index = layout.findIndex(e => e.chartId == item.chartId);
            layout.splice(index, 1);
            state.layout = layout;

            var user = JSON.parse(localStorage.getItem('user'));
            user.dashboard = JSON.stringify({layout});

            localStorage.setItem('user', JSON.stringify(user));
            return result.data;
        }).catch((e) => {
            return e;
        });
    },
    getCharts({commit}) {
        return ChartService.getCharts().then(result => {
            commit('charts', result.data.data);
            return result.data.data;
        }).catch(() => {
            return false;
        });
    },
    saveLayout({commit, state}, layout) {
        var layoutCopy = deepCopy(layout);
        layoutCopy = Object.keys(layoutCopy).map(function (key) { return layoutCopy[key]; });
        layoutCopy.forEach((e) => {
            delete e.chartData;
            delete e.moved;
        });

        return ChartService.saveLayout(layoutCopy).then(result => {
            return result.data.data;
        }).catch(() => {
            return false;
        });
    },
    userDeviceTypes({commit, state}) {
        return UserDevicesService.getUserDeviceTypes().then(result => {
            commit('userDeviceTypes', result.data.data);
            return result.data;
        }).catch(() => {
            return false;
        });
    },
    userMeasurementTypes({commit, state}) {
        return UserMeasurementTypesService.user().then(result => {
            commit('userMeasurementTypes', result.data.data);
            return result.data;
        }).catch(() => {
            return false;
        });
    },
    allMeasurementTypes({commit}) {
        return UserMeasurementTypesService.all().then(result => {
            commit('allMeasurementTypes', result.data.data);
            return result.data;
        }).catch(() => {
            return false;
        });

    }
};


const mutations = {
    allGpios(state, gpios) {
        if (gpios) {
            state.allGpios = gpios;
        }
    },
    currentChart(state, data) {
        state.currentChart = data;
    },
    userMeasurementTypes(state, data) {
        state.userMeasurementTypes = data;
    },
    allMeasurementTypes(state, data) {
        state.allMeasurementTypes = data;
    },
    userDeviceTypes(state, data) {
        state.userDeviceTypes = data;
    },
    chart(state, data) {
        //'chart' => $this->model,
        //'dashboard_item' => $this->addModelToDashboard()

        var index = 0;
        var found = false;
        if (!data.dashboard_item) {
            for (var i = 0; i < state.charts.length; i++) {
                if (state.charts[i].id == data.chart.id) {
                    index = i;
                    found = true;
                    break;
                }
            }
            if (found) {
                state.charts[i] = data.chart;
            }
        }
        else {
            state.charts.push(data.chart);
        }

        var user = JSON.parse(localStorage.getItem('user'));
        if (typeof user.dashboard == 'string') {
            user.dashboard = JSON.parse(user.dashboard);
        }

        if (data.dashboard_item) {
            user.dashboard.layout.push(data.dashboard_item);
        }

        localStorage.setItem('user', JSON.stringify(user));

        var dashboard = deepCopy(user.dashboard);
        dashboard.layout.forEach(function(e) {
            state.charts.forEach(function(e2) {
                if (e.chartId == e2.id) {
                    e.chartData = JSON.parse(e2.data);
                }
            });
        }.bind(this));

        state.layout = dashboard.layout;
    },
    charts(state, data) {
        if (data) {
            //'charts' => $this->chartRepository->getUserCharts($this->guard->user()->id),
            //'dashboard' => $this->guard->user()->dashboard
            state.charts = data.charts;

            var user = JSON.parse(localStorage.getItem('user'));
            user.dashboard = data.dashboard;
            localStorage.setItem('user', JSON.stringify(user));

            var dashboard = deepCopy(user.dashboard);
            dashboard.layout.forEach(function(e){
                data.charts.forEach(function(e2){
                    if (e2.id == e.chartId) {
                        e.chartData = JSON.parse(e2.data);
                    }
                });
            }.bind(this));

            state.layout = dashboard.layout;
        }
    },
    layout(state, data) {
        state.layout = data;
    },
    editItemRef(state, data) {
        state.editItemRef = data;
    },
    allDeviceNames(state, data) {
        state.allDeviceNames = data;
    }
};

const getters = {
    currentChart(state) {
        return state.currentChart;
    },
    info(state) {
        return state.info;
    },
    allGpios(state) {
        return state.allGpios;
    },
    charts(state) {
        return state.charts;
    },
    layout(state) {
        return state.layout;
    },
    editItemRef(state) {
        return state.editItemRef;
    },
    allDeviceNames(state) {
        return state.allDeviceNames;
    },
    userMeasurementTypes(state) {
        return state.userMeasurementTypes;
    },
    allMeasurementTypes(state) {
        return state.allMeasurementTypes;
    }
};

export default {
    state,
    mutations,
    actions,
    getters,
    namespaced: true
}