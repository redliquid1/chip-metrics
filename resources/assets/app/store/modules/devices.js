import UserDevicesService from "../../services/UserDevicesService";
import DeviceNames from "../../services/DeviceNames";

const state = {
    devices: [],
    gpios: [],
    current: null,
    deviceNames: []
};

const actions = {
    userDevices({commit}) {
        return UserDevicesService.getDevices().then(result => {
            commit('storeDevices', result.data.data);
            return result;
        }).catch(() => {
            return false;
        });
    },
    addDevice({commit}, data) {
        return UserDevicesService.addDevice(data.chipId).then(result => {
            return result.data;
        }).catch(() => {
            return false;
        });
    },
    removeDevice({commit}, data) {
        return UserDevicesService.removeDevice(data.id).then(result => {
            return result.data;
        }).catch(() => {
            return false;
        });
    },
    changeName({commit}, data) {
        return UserDevicesService.changeName(data).then(result => {
            return result.data;
        }).catch(() => {
            return false;
        });
    },
    getGpios({commit}, data) {
        return UserDevicesService.getGpios(data).then(result => {
            commit('storeGpios', result.data.data);
            return result.data;
        }).catch(() => {
            return false;
        });
    },
    saveGpios({commit, state}, deviceId) {
        return UserDevicesService.saveGpios(deviceId, state.gpios).then(result => {
            return result.data;
        }).catch(() => {
            return false;
        });
    },
    exportData({commit, state}, deviceId) {
        return UserDevicesService.exportData(deviceId).then(result => {
            return result;
        }).catch(() => {
            return false;
        });
    },
    deviceNames({commit}) {
        return DeviceNames.getAll().then(result => {
            commit('deviceNames', result.data.data);
            return result.data;
        }).catch(() => {
            return false;
        });
    },
    systemDevices({commit}) {
        return UserDevicesService.systemDevices().then(result => {
            commit('storeDevices', result.data.data);
            return result;
        }).catch(() => {
            return false;
        });
    },
    saveSystemDevice({commit, state}) {
        return UserDevicesService.saveSystemDevice(state.current).then(result => {
            return result.data;
        }).catch(() => {
            return false;
        });
    },
    removeSystemDevice({commit, state}) {
        return UserDevicesService.removeSystemDevice(state.current.id).then(result => {
            return result.data;
        }).catch(() => {
            return false;
        });
    },

};

const mutations = {
    storeDevices(state, devices) {
        if (devices) {
            state.devices = devices;
        }
    },
    storeGpios(state, gpios) {
        if (gpios) {
            state.gpios = gpios;
        }
    },
    current(state, device) {
        state.current = device;
    },
    deviceNames(state, names) {
        state.deviceNames = names;
    }
};

const getters = {
    devices(state) {
        return state.devices;
    },
    gpios(state) {
        return state.gpios;
    },
    current(state) {
        return state.current;
    },
    deviceNames(state) {
        return state.deviceNames;
    },
};

export default {
    state,
    mutations,
    actions,
    getters,
    namespaced: true
}