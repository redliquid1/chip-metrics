import Model from '@/models/Model';
import store from '@/store/store';

export default class Event extends Model {
    static defaultData() {
        return {
            owner_id: store.getters['user/user'].id,
            name: '',
            place: '',
            description: '',
            start_date: null,
            duration: 0,
            duration_value: 0,
            duration_unit: 'minutes',
            max_guests: 0,
            max_registration_date: null,
            is_private: false,
            lat: 52.045598,
            lng: 23.130056,
            zoom: 17
        };
    }
}

