export default {
    defaultDateTimeFormat: 'YYYY-MM-DD HH:mm',
    databaseDateTimeFormat: 'YYYY-MM-DD HH:mm:ss',
    roles: {
        superAdmin: 'super-admin',
        client: 'client',
    },
    units: {
        time: [
            {text: 'minuty', value: 'minutes'},
            {text: 'godziny', value: 'hours'},
            {text: 'dni', value: 'days'},
        ]
    },
    chartPeriods: [
        {
            type: 'yearly',
            label: 'Roczny',
            description: 'years',
            lastValues: Array.from({length: 3}, (v, k) => k + 1),
            single: 'rok'
        },
        {
            type: 'monthly',
            label: 'Miesięczny',
            description: 'months',
            lastValues: Array.from({length: 12}, (v, k) => k + 1),
            single: 'miesiąc'
        },
        {
            type: 'weekly',
            label: 'Tygodniowy',
            description: 'weeks',
            lastValues: Array.from({length: 12}, (v, k) => k + 1),
            single: 'tydzień'
        },
        {
            type: 'daily',
            label: 'Dzienny',
            description: 'days',
            lastValues: Array.from({length: 30}, (v, k) => k + 1),
            single: 'dzień'
        },
        {
            type: 'hourly',
            label: 'Godzinny',
            description: 'hours',
            lastValues: Array.from({length: 48}, (v, k) => k + 1),
            single: 'godzina'
        }
    ],
    chartTypes: [
        {
            type: 'bar',
            label: 'Słupkowy',
        },
        {
            type: 'line',
            label: 'Liniowy',
        },
        {
            type: 'area',
            label: 'Liniowy (wypełniony)',
        },
        {
            type: 'spline',
            label: 'Krzywa',
        },
        {
            type: 'area-spline',
            label: 'Krzywa (wypełniona)',
        },
        {
            type: 'pie',
            label: 'Kołowy',
        },
        {
            type: 'donut',
            label: 'Okrąg',
        },
    ],
    devices: {
        power: {
            measurementTypes: [
                {
                    type: 'power',
                    label: 'Prąd (kWh)'
                },
                {
                    type: 'water',
                    label: 'Woda (L)'
                },
            ],
            tariffs: [
                {
                    type: 'g11',
                    label: 'G11'
                },
                {
                    type: 'g12',
                    label: 'G12'
                },
                {
                    type: 'g12w',
                    label: 'G12w'
                }
            ]
        }
    },
    validators: {
        chipId: {
            'chip-id-ok': 'Chip ID dostępny.',
            'chip-id-not-found': 'Chip ID nie znaleziony w bazie danych.',
            'chip-id-self': 'Chip ID jest już przypisany do twojego konta.',
            'chip-id-already-assigned': 'Chip ID jest już przypisany do konta innego użytkownika.',
        }
    }
}
