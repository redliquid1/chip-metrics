export default {
    my: {
        domain: window.location.protocol + '//' + window.location.host + '/',
        url: window.location.protocol + '//' + window.location.host + '/api/v1/',
        ttl: 60
    },
    gmaps: {
        script: 'https://maps.googleapis.com/maps/api/js?key=',
        key: 'AIzaSyAAKcGAoJFyCfkr9Qia5opswpXVN6WFon8'
    },
    facebook: {
        script: 'https://connect.facebook.net/en_US/sdk.js',
        onLoad: () => {
            FB.init({
                appId            : '180429765781602',
                autoLogAppEvents : true,
                xfbml            : true,
                version          : 'v3.1'
            });
        }
    }
}
