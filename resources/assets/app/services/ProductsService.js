import HTTP from './HTTP'

export default {
    all() {
        return HTTP.get('products');
    },
    save(data){
        if (data.id) {
            return HTTP.put(`products/${data.id}`, data);
        }
        return HTTP.post(`products`, data);
    },
    remove(item) {
        return HTTP.delete(`products/${item.id}`);
    },
    saveSort(data){
        return HTTP.post(`products/save-sort`, {
            sort: data.map(e => e.id)
        });
    }

}