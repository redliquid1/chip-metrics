import axios from 'axios'
import store from '../store/store'
import jquery from 'jquery'
import apis from '../configs/apis'
import router from 'vue-router'
import UserService from '../services/UserService'

const HTTP = axios.create({
    baseURL: apis.my.url,
    paramsSerializer: (params) => {
        return jquery.param(params);
    }
});

HTTP.interceptors.request.use(
    config => {
        const token = store.getters['user/token'];
        if (token) {
            config.headers.Authorization = `Bearer ${token}`;
        }

        //if (!config.params) config.params = {};
        //config.params.XDEBUG_SESSION_START = 1;

        store.dispatch('indicators/setSubmitIndicator', true);
        return config;
    }
);

HTTP.interceptors.response.use((response) => {
    store.dispatch('indicators/setSubmitIndicator', false);
    return response;
}, (error) => {
    store.dispatch('indicators/setSubmitIndicator', false);

    if (error.response.data.status_code == 401 &&
        error.response.data.message.indexOf('expired') !== -1 &&
        store.getters['user/token']) {
        if (store.getters['user/rememberMe']) {
            // try to refresh token
            var previousRequest = error.config;
            return store.dispatch('user/refreshToken').then(function(response) {
                const token = store.getters['user/token'];
                if (token) {
                    previousRequest.headers.Authorization = `Bearer ${token}`;
                }
                store.dispatch('indicators/setSubmitIndicator', true);
                return HTTP.request(previousRequest);
            }, function(error) {
                store.dispatch('user/logout', {force: true});
            });
        }
        else {
            store.dispatch('user/logout', {force: true});
        }
    }

    /*
    if (error.response && error.response.data.status_code == 401) {
        store.commit('user/clearUser');
        router.push({name: 'home'});
    }*/

    return Promise.reject(error);
});

// refresh token every ttl - 100s
/*
setTimeout(() => {
    if (UserService.isLogged()) {
        const timer = setInterval(() => {
            store.dispatch('user/refreshToken', localStorage.getItem('token')).then(response => {
                if (response && response.data.user) {
                    store.commit('user/storeUser', {
                        token: response.data.newToken,
                        user: response.data.user,
                        expiration: response.data.expiration
                    });
                }
                else {
                    localStorage.removeItem('token');
                    localStorage.removeItem('user');
                    router.push({name: 'home'});
                }
            });
        }, apis.my.ttl * 1000 - 10 * 1000);
    }
}, 0);
*/

export default HTTP