import HTTP from './HTTP'

export default {
    all() {
        return HTTP.get('static-sites');
    },
    save(data){
        if (data.id) {
            return HTTP.put(`static-sites/${data.id}`, data);
        }
        return HTTP.post(`static-sites`, data);
    },
    remove(item) {
        return HTTP.delete(`static-sites/${item.id}`);
    },
}