import HTTP from './HTTP'

export default {
    getDevices() {
        return HTTP.get('user/devices');
    },
    isDeviceAvailable(chipId) {
        return HTTP.get('user/devices/is-available', {
            params: {chipId}
        });
    },
    addDevice(chipId) {
        return HTTP.post('user/devices/add', {chipId});
    },
    removeDevice(id) {
        return HTTP.delete(`user/devices/${id}`);
    },
    changeName(data) {
        return HTTP.put(`user/devices/${data.id}`, data);
    },
    getGpios(data) {
        return HTTP.get(`user/devices/gpios/${data.id}`);
    },
    saveGpios(id, data) {
        return HTTP.put(`user/devices/gpios/${id}`, {
            data, id
        });
    },
    getAllUserGpios(measurement_type_id) {
        return HTTP.get('user/devices/gpios/all', {
            params: {
                measurement_type_id: measurement_type_id
            }
        });
    },
    exportData(id) {
        return HTTP.get(`user/devices/${id}/export-data`);
    },
    systemDevices() {
        return HTTP.get('/user/system-devices');
    },
    saveSystemDevice(device) {
        if (device.id) {
            return HTTP.put(`/user/system-devices/${device.id}`, device);
        }
        return HTTP.post(`/user/system-devices`, device);
    },
    removeSystemDevice(id) {
        return HTTP.delete(`user/system-devices/${id}`);
    },
    getAllUserDeviceNames() {
        return HTTP.get('user/device-names');
    },
    getUserDeviceTypes() {
        return HTTP.get('user/device-types');
    }

}