import HTTP from '../HTTP'
import store from './../../store/store'

export default {
    all() {
        return HTTP.get(`translations/all`);
    },
    datatables(query) {
        return HTTP.get('translations/datatables', {
            params: query
        });
    },
    modules() {
        return HTTP.get(`translations/modules`);
    },
    languages() {
        return HTTP.get(`translations/languages`);
    },
    setTranslation(data) {
        return HTTP.post(`translations/set-translation`, {data});
    },
    changeKey(data) {
        return HTTP.post(`translations/change-key`, {data});
    },
    save(data) {
        return HTTP.post(`translations/save`, {data});
    },
    saveModules(data) {
        return HTTP.post(`translations/save-modules`, {data});
    },
    keyExist(key, moduleId) {
        let params = {key, moduleId};
        params.ignoreId = store.getters['translations/current'].id ? store.getters['translations/current'].id : null;
        return HTTP.post(`translations/key-exist`, params);
    },
    remove(item) {
        return HTTP.delete(`translations/${item.id}`);
    },
    exportData() {
        return HTTP.get(`translations/export-data`);
    },
    importData() {
        return HTTP.get(`translations/import-data`);
    },

}
