import HTTP from './HTTP'

export default {
    getAll() {
        return HTTP.get('faq');
    },
    save(data){
        if (data.id) {
            return HTTP.put(`faq/${data.id}`, data);
        }
        return HTTP.post(`faq`, data);
    },
    remove(item) {
        return HTTP.delete(`faq/${item.id}`);
    },
    saveSort(data){
        return HTTP.post(`faq/save-sort`, {
            sort: data.map(e => e.id)
        });
    }

}