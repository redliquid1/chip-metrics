import HTTP from './HTTP'

export default {
    all() {
        return HTTP.get('measurement-types');
    },
    user() {
        return HTTP.get('measurement-types/user');
    }

}