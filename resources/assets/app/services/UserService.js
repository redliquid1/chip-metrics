import HTTP from './HTTP'
import store from './../store/store'
import constances from './../configs/constances'

export default {
    login(credentials) {
        return HTTP.post('user/login', credentials);
    },
    logout() {
        return HTTP.get('user/logout');
    },
    register(data) {
        return HTTP.post('user/register', data);
    },
    isEmailUnique(email, includeMe) {
        return HTTP.post('user/is-email-unique', {email, includeMe});
    },
    checkToken(token) {
        return HTTP.get(`user/check-token/${token}`);
    },
    refreshToken(token) {
        return HTTP.get(`user/refresh-token/${token}`);
    },
    isLogged() {
        return !!store.getters['user/isAuthenticated'];
    },
    save(data) {
        return HTTP.put('user', data);
    },
    loginViaSocialMedia(socialMedia, user) {
        return HTTP.post('user/login/via-social-media', {
            socialMedia, user
        });
    },
    deleteUser(user) {
        return HTTP.delete(`user/users/${user.id}`);
    },
    users() {
        return HTTP.get('user/users');
    },
    saveUser(data) {
        if (data.id) {
            return HTTP.post(`user/users/${data.id}`, data);
        }
        return HTTP.put(`user/users`, data);
    },
    impersonate(user) {
        return HTTP.get(`user/users/impersonate/${user.id}`);
    },
    hasRole(role) {
        if (!store.getters['user/user'].user_roles) {
            return false;
        }
        return store.getters['user/user'].user_roles.indexOf(role) == -1 ? false : true;
    },
    isSuperAdmin() {
        return this.hasRole(constances.roles.superAdmin);
    }
}