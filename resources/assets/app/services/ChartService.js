import HTTP from './HTTP'

export default {
    saveChart(data) {
        var copyData = deepCopy(data);
        if (copyData.id) {
            return HTTP.put(`user/charts/${copyData.id}`, copyData);
        }

        return HTTP.post('user/charts', copyData);
    },
    getCharts() {
        return HTTP.get('user/charts');
    },
    deleteChart(id) {
        return HTTP.delete(`user/charts/${id}`);
    },
    saveLayout(layout){
        return HTTP.put(`user/charts/save-layout`, {layout});
    }
}