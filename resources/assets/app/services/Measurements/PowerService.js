import HTTP from '../HTTP'
import store from '../../store/store'

export default {
    getPower(params) {
        params.deviceIds = params.deviceIds.join(',');
        return HTTP.get('user/measurement/power', {
            params: params
        });
    }
}