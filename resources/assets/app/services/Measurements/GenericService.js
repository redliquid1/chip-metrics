import HTTP from '../HTTP'
import store from '../../store/store'

export default {
    getMeasurements(params) {
        params.deviceIds = params.deviceIds.join(',');
        return HTTP.get('user/measurement/generic', {
            params: params
        });
    }
}