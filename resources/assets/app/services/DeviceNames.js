import HTTP from './HTTP'

export default {
    getAll() {
        return HTTP.get('/device-names');
    },
    save(data){
        if (data.id) {
            return HTTP.put(`device-names/${data.id}`, data);
        }
        return HTTP.post(`device-names`, data);
    },
    remove(item) {
        return HTTP.delete(`device-names/${item.id}`);
    },
}