import HTTP from './HTTP'

export default {
    all() {
        return HTTP.get('/measurement-types');
    }
}