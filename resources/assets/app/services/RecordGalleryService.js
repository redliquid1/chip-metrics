import HTTP from './HTTP'

export default {
    save(data) {
        var f = [];
        data.gallery.forEach(e => {
            if (typeof e.model_id !== 'undefined') {
                f.push({
                    id: e.id,
                    model_id: e.model_id
                });
            }
            else if (typeof e.file.file !== 'undefined') {
                f.push(e.file.file);
            }
        });

        function getFiles(files) {
            return Promise.all(files.map(file => {
                if (typeof file.model_id !== "undefined") {
                    return file;
                }
                return getFile(file);
            }));
        }

        //take a single JavaScript File object
        function getFile(file) {
            var reader = new FileReader();
            return new Promise((resolve, reject) => {
                reader.onerror = () => {
                    reader.abort();
                    reject(new Error("Error parsing file"));
                };

                reader.onload = function () {
                    resolve({
                        file: reader.result,
                        fileName: file.name
                    });
                };
                reader.readAsDataURL(file);
            });
        }

        return getFiles(f).then(function(e) {
            data.gallery = e;
            return HTTP.post(`record-gallery/save`, data);
        });
    }
}