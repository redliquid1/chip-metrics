import pl_global from './pl/global'
import pl_login from './pl/login'
import pl_registration from './pl/registration'
import pl_dashboard from './pl/dashboard'
import pl_devices from './pl/devices'
import pl_users from './pl/users'
import pl_faq from './pl/faq'
import pl_staticSites from './pl/static-sites'
import pl_products from './pl/products'
import pl_charts from './pl/charts'
import pl_deviceNames from './pl/device-names'

export default {
    pl: {
        global: pl_global,
        login: pl_login,
        registration: pl_registration,
        dashboard: pl_dashboard,
        devices: pl_devices,
        users: pl_users,
        faq: pl_faq,
        staticSites: pl_staticSites,
        products: pl_products,
        charts: pl_charts,
        deviceNames: pl_deviceNames,
    }
}