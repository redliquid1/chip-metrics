export default {
    yes: 'Tak',
    no: 'Nie',
    close: 'Zamknij',
    open: 'Otwórz',
    save: 'Zapisz',
    actions: 'Akcje',
    cancel: 'Anuluj',
    menu: {
        adminArea: 'Strefa admina',
        systemDevices: 'Urządzenia systemu',
        users: 'Użytkownicy',
        faq: 'FAQ',
        staticSites: 'Strony statyczne',
        products: 'Produkty',
        deviceNames: 'Typy produktów',
        clientArea: 'Strefa klienta',
        myDashboard: 'Pulpit',
        myDevices: 'Moje urządzenia',
        profile: 'Profil',
        logout: 'Wyloguj',
        translations: 'Tłumaczenia'
    },
    role: {
        admin: 'Admin',
        client: 'Client',
    },
    top: {
        helloGuest: 'Witaj Gościu!'
    }
}