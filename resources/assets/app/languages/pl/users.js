export default {
    users: 'Użytkownicy',
    addUser: 'Dodaj użytkownika',
    table: {
        id: 'ID',
        name: 'Nick',
        email: 'Email',
    },
    contextMenu: {
        edit: 'Edycja użytkownika',
        impersonate: 'Zaloguj jako',
        remove: 'Usuń'
    },
    modal: {
        addUser: 'Dodawanie użytkownika',
        editUser: 'Edycja użytkownika',
        password: 'Hasło',
        repeatPassword: 'Powtórz hasło',
        role: 'Rola'
    },
    noUsers: 'Nie znaleziono użytkowników',
    deleteModal: {
        title: 'Usuwanie użytkownika',
        content: 'Wszystkie dane powiązane z użytkownikiem zostaną usunięte. Czy na pewno chcesz usunąć ',
        submit: 'Tak, usuń użytkownika'
    },
    userDeleted: 'Użytkownik usunięty.',
    userNotDeleted: 'Użytkownik nie został usunięty poprawnie.',
    userSaved: 'Użytkownik został zapisany.',
    userNotSaved: 'Użytkownik nie został zapisany poprawnie.',

    profile: {
        name: 'Profil',
    }
}