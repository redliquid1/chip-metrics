export default {
    name: 'Strony statyczne',
    add: 'Dodaj stronę',
    table: {
        name: 'Nazwa',
        description: 'Początek strony'
    },
    context: {
        edit: 'Edytuj',
        remove: 'Usuń',
    },
    noData: 'Nie znaleziono stron.',
    removeModal: {
        title: 'Usuwanie strony',
        content: 'Czy jesteś pewien, że chcesz usuąć ',
        submit: 'Tak, usuń.'
    },
    removed: 'Usunięto pozycję.',
    removeError: 'Nieudane usuwanie strony.',
    saved: 'Zapisano stronę.',
    dataModal: {
        titleAdd: 'Dodawanie strony',
        titleEdit: 'Edycja strony',
        name: 'Nazwa',
        content: 'Treść',
    }
}