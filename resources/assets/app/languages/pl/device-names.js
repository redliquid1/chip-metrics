export default {
    name: 'Typy produktów',
    add: 'Dodaj typ produktu',
    table: {
        id: 'Identyfikator',
        name: 'Nazwa'
    },
    context: {
        edit: 'Edytuj',
        remove: 'Usuń',
    },
    noData: 'Nie znaleziono typów produktów.',
    removeModal: {
        title: 'Usuwanie typu produktu',
        content: 'Czy jesteś pewien, że chcesz usunąć ',
        submit: 'Tak, usuń.'
    },
    removed: 'Usunięto typ produktu',
    removeError: 'Nieudane usuwanie typu produktu',
    saved: 'Zapisano typ.',
    dataModal: {
        titleAdd: 'Dodawanie typu produktu',
        titleEdit: 'Edycja typu produktu',
        name: 'Nazwa',
        id: 'Identyfikator',
        delivers: 'Dostarcza'
    }
}