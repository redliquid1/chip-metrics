export default {
    myDevices: 'Moje urządzenia',
    addDevice: 'Dodaj urządzenie',
    editDevice: 'Edycja urządzenia',
    table: {
        id: 'ID',
        name: 'Nazwa',
        type: 'Typ',
        last: 'Ostatni pomiar'
    },
    contextMenu: {
        changeName: 'Zmień nazwę',
        configureInputs: 'Konfiguracja portów',
        exportData: 'Eksport danych',
        editDevice: 'Edycja urządzenia',
        removeDevice: 'Usuń urządzenie',
    },
    noDevices: 'Nie znaleziono urządzeń. Aby dodać urządzenie kliknij przycisk "Dodaj urządzenie"',
    removeModal: {
        title: 'Usuwanie urządzenia',
        content: 'Wszystkie pomiary z tego urządzenia zostaną usunięte. Czy jesteś pewien, że chcesz usuąć',
        submit: 'Tak, usuń urządzenie'
    },
    deviceRemoved: 'Usunięto urządzenie',
    deviceRemoveError: 'Nieudane usuwanie urządzenia', // ' \'Something went wrong with removing the device.\'',
    deviceSaved: 'Zapisano urządzenie',
    addModal: {
        title: 'Dodaj urządzenie'
    },
    chipID: 'Chip ID',
    deviceAdded: 'Dodano urządzenie',
    changeDeviceName: 'Zmień nazwę urządzenia',
    delivers: {
        delivers: 'Urządzenie dostarcza',
        kwh: 'kWh',
        temperature: "C"
    },
    system: {
        devices: 'Urządzenia w systemie',
        user: 'Użytkownik',
    },
    gpios: {
        configureInputs: 'Konfiguracja wejść dla ',
        input: 'Wejście',
        name: 'Nazwa',
        measurementType: 'Typ',
        tariff: 'Taryfa',
        color: 'Kolor',
        ppuDescription: 'jak dużo impulsów na jedną jednostkę Twoje urządzenie generuje, np. <strong>1000</strong> na kWh, <strong>1</strong> na litr, <strong>0.1</strong> na litr'
    }
}