export default {
    login: 'Logowanie',
    email: 'Email',
    emailPlaceholder: 'Wpisz swój email',
    password: 'Hasło',
    passwordPlaceholder: 'Wpisz swoje hasło',
    rememberMe: 'Zapamiętaj mnie',
    loginButton: 'Zaloguj',
    dontHaveAccount: 'Nie masz jeszcze konta?',
    registerNow: 'Zarejestruj się',
    viaFacebook: 'Zaloguj się przez Facebooka',
    httpsEnv: 'Umieść aplikację w protokole HTTPS, żeby zalogować się przez Facebooka', //'You need to place your application in HTTPS environment to login via Facebook';

    loggedSuccessfully: 'Zalogowano pomyślnie',
    incorrectUserData: 'Nieprawidłowe dane uwierzytelniające'
}