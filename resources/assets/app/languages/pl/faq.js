export default {
    name: 'FAQ',
    add: 'Dodaj pozycję',
    table: {
        name: 'Nazwa',
        description: 'Początek opisu'
    },
    context: {
        edit: 'Edytuj',
        remove: 'Usuń',
    },
    noData: 'Nie znaleziono wpisów w FAQ.',
    removeModal: {
        title: 'Usuwanie pozycji',
        content: 'Czy jesteś pewien, że chcesz usuąć ',
        submit: 'Tak, usuń.'
    },
    removed: 'Usunięto pozycję w FAQ',
    removeError: 'Nieudane usuwanie pozycji FAQ',
    saved: 'Zapisano pozycję.',
    dataModal: {
        titleAdd: 'Dodawanie pozycji',
        titleEdit: 'Edycja pozycji',
        name: 'Nazwa',
        description: 'Opis',
    }
}