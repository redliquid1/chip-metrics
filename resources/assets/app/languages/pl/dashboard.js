export default {
    addChart: 'Dodaj wykres',
    noDevices: {
        noDevices: 'Nie masz dodanych żadnych urządzeń.',
        wannaAdd: 'Chesz jakieś dodać?',
    },
    chartModal: {
        add: 'Dodaj wykres',
        edit: 'Edycja wykresu',
        deviceType: 'Typ urządzenia',
        measurementType: 'Typ pomiaru',
        chooseDevice: 'Wybierz urządzenie',
        availableInputs: 'Dostępne porty',
        chartName: 'Nazwa wykresu',
        chartNamePlaceholder: 'Wpisz nazwę wykresu',
        chartType: 'Typ wykresu',
        period: 'Zakres czasu',
        last: 'Ostanie',
        includeCurrent: 'Dołącz ostatni',
        includeDevice: 'Dołącz nazwę urządzenia w legendzie wykresu',
        preview: 'Podgląd wykresu',
        chartSaved: 'Wykres zapisany',
        chooseMeasurementType: 'Wybierz typ pomiaru, aby wyświetlić dostępne porty.'
    }
}
