export default {
    createAccount: 'Tworzenie nowego konta',
    name: 'Nick',
    namePlaceholder: 'Wpisz swój nick',
    email: 'Email',
    emailPlaceholder: 'Wpisz swój email',
    password: 'Hasło',
    passwordPlaceholder: 'Wpisz swoje hasło',
    passwordRepeat: 'Powtórz hasło',
    passwordRepeatPlaceholder: 'Powtórz swoje hasło',
    terms: {
        accept: 'Akceptuję',
        terms: 'regulamin'
    },
    newAccountButton: 'Utwórz nowe konto',
    alreadyHaveAccount: 'Masz już konto?',
    login: 'Zaloguj się',
    registaredAndLoggedIn: 'Zarejestrowałeś się i zostałeś automatycznie zalogowany',
    incorrectUserData: 'Nieprawidłowe dane uwierzytelniające'
}