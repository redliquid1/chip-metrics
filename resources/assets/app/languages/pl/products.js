export default {
    name: 'Produkty',
    add: 'Dodaj produkt',
    table: {
        name: 'Nazwa',
        description: 'Początek opisu'
    },
    context: {
        edit: 'Edytuj',
        remove: 'Usuń',
    },
    noData: 'Nie znaleziono produktów.',
    removeModal: {
        title: 'Usuwanie produktu',
        content: 'Czy jesteś pewien, że chcesz usunąć ',
        submit: 'Tak, usuń.'
    },
    removed: 'Usunięto produkt',
    removeError: 'Nieudane usuwanie produktu',
    saved: 'Zapisano product.',
    dataModal: {
        titleAdd: 'Dodawanie produktu',
        titleEdit: 'Edycja produktu',
        name: 'Nazwa',
        price: 'Cena',
        allegroLink: 'Link do Allegro',
        description: 'Opis',
        gallery: 'Galeria',
    }
}