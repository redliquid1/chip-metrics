import jquery from 'jquery'
require('bootstrap');

setTimeout(function(){
    window.jQuery = jquery;
}, 0);

window.deepCopy = function(object) {
    return jquery.extend(true, {}, object);
    // return JSON.parse(JSON.stringify(object));
}
window.objectToArray = function(object) {
    return Object.keys(object).map(function (key) { return object[key]; });
}

// https://werxltd.com/wp/2010/05/13/javascript-implementation-of-javas-string-hashcode-method/
String.prototype.hashCode = function() {
    var hash = 0, i, chr;
    if (this.length === 0) return hash;
    for (i = 0; i < this.length; i++) {
        chr   = this.charCodeAt(i);
        hash  = ((hash << 5) - hash) + chr;
        hash |= 0;
    }
    return Math.abs(hash).toString(16);
};

import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store/store'
import VeeValidate from 'vee-validate'
import { Validator } from 'vee-validate';

import Snotify from 'vue-snotify'
// eslint-disable-next-line
import EmailUnique from './validators/EmailUnique'
// eslint-disable-next-line
import ChipIdAvailable from './validators/ChipIdAvailable'
// eslint-disable-next-line
import KeyExist from './validators/KeyExist'

import "vue-snotify/styles/material.css"
import "@fortawesome/fontawesome-free/css/all.css"
import "datatables.net-dt/css/jquery.dataTables.css"
//import "bootstrap-datepicker/dist/css/bootstrap-datepicker.standalone.css"
//import "bootstrap4-datetimepicker/build/css/bootstrap-datetimepicker.min.css"
//import "cropperjs/dist/cropper.min.css"
import "c3/c3.min.css"
import VuexRouterSync from 'vuex-router-sync'
import VModal from 'vue-js-modal'
// import vueUpload from '@websanova/vue-upload';

import apis from './configs/apis';

Vue.config.productionTip = false;

VuexRouterSync.sync(store, router);
Vue.use(VeeValidate);
Vue.use(Snotify);
Vue.use(require('vue-moment'));
Vue.use(VModal, { dialog: true });

var VueCookie = require('vue-cookie');
Vue.use(VueCookie);

import VueI18n from 'vue-i18n';
Vue.use(VueI18n);

// load scripts
const scripts = [
    {
        url: apis.gmaps.script + apis.gmaps.key,
    }, {
        url: apis.facebook.script,
        onLoad: apis.facebook.onLoad
    }
];


scripts.forEach((item) => {
    let script = document.createElement('script');
    script.async = true;
    script.setAttribute('src', item.url);
    if (item.onLoad) {
        script.onload = item.onLoad;
    }
    document.head.appendChild(script);
});


// check if user is logged
if (localStorage.getItem('token') && localStorage.getItem('user')) {
    try{
        store.commit('user/storeUser', {
            token: localStorage.getItem('token'),
            user: JSON.parse(localStorage.getItem('user'))
        });
    }
    catch(e) {
        localStorage.removeItem('token');
        localStorage.removeItem('user');
    }
}

const i18n = new VueI18n({
    locale: 'pl',
    messages: {}
});


var _st = require('striptags');
Vue.filter('striptags', function (value) {
    return _st(value);
});

Vue.filter('truncate', function (str, maxLen) {
    if (str.length <= maxLen) return str;
    return str.substr(0, str.lastIndexOf(' ', maxLen)) + '...';
});

require('./libraries/veevalidate-locales.js');

window.vueApp = new Vue({
    i18n,
    router,
    store,
    render: h => h(App)
}).$mount('#app');
