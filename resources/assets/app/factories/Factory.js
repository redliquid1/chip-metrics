export const Factory = {
    registered: [],

    register(className, c) {
        if (!(Factory.registeredTypes.has(className))) {
            Factory._registeredTypes.add(className, c);
        }
    },

    create(className, ...options) {
        if (!Factory.registeredTypes.has(className)) {
            console.error('No registered class called ' + className);
            return null;
        }
        let c = this.registeredTypes.get(className);
        let instance = new c(...options);
        return instance;
    }
};