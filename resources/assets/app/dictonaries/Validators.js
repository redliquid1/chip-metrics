import { Validator } from 'vee-validate';

const dictionary = {
  custom: {
    birthDate: {
      required: 'The birth date field is required.'
    }
  }
};

// Validator.localize('en', dictionary);
