import { Validator } from 'vee-validate';

var context = require.context("vee-validate/dist/locale/", true, /\.js$/);
var files = [];
context.keys().forEach(function (key) {
    var o = context(key);
    Validator.localize(o.name, o);
});