import {Validator} from 'vee-validate';
import UserService from '../services/UserService';

Validator.extend('email-unique', {
    validate: (value) => {
        return new Promise((resolve) => {
            UserService.isEmailUnique(value).then((response) => {
                if (response.data.exists) {
                    return resolve({
                        valid: false,
                        data: {
                            message: `This email is already taken.`
                        }
                    });
                }
                return resolve({
                    valid: true
                });
            });
        });
    },
    getMessage: (field, params, data) => {
        return data.message;
    }
});