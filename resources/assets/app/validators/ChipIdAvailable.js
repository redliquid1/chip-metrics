import {Validator} from 'vee-validate';
import constances from '../configs/constances'
import UserDevicesService from "../services/UserDevicesService";

const chipIdAvailableValidator = {
    validate: (value) => {
        return new Promise((resolve) => {
            UserDevicesService.isDeviceAvailable(value).then((response) => {
                if (response.data.data != "ok") {
                    return resolve({
                        valid: false,
                        data: {
                            message: constances.validators.chipId['chip-id-' + response.data.data]
                        }
                    });
                }
                return resolve({
                    valid: true
                });
            });
        });
    },
    getMessage: (field, params, data) => {
        return data.message;
    }
};

Validator.extend('chip-id-available', chipIdAvailableValidator);