import {Validator} from 'vee-validate';
import TranslationService from "../services/Translations/TranslationService";

let handle = null;
const keyExistValidator = {
    validate: (value, complex) => {
        return new Promise((resolve) => {
            if (handle) {
                clearTimeout(handle);
            }
            handle = setTimeout(() => {
                TranslationService.keyExist(value, complex.module).then((response) => {
                    if (response.data.data) {
                        return resolve({
                            valid: false,
                            data: {
                                message: 'key-exist'
                            }
                        });
                    }
                    return resolve({
                        valid: true
                    });
                });
            }, 500);
        });
    },
    getMessage: (field, params, data) => {
        return data.message;
    }
};

Validator.extend('key-exist', keyExistValidator);