import Vue from 'vue'
import Router from 'vue-router'
import Dashboard from './views/user/dashboard/Dashboard'
import Login from './views/Login.vue'
import Register from './views/Register.vue'

import UserContainer from './views/user/Container.vue'
import UserProfile from './views/user/profile/Profile.vue'
import Users from './views/user/users/Users.vue'
import UserMyDevices from './views/user/my-devices/MyDevices.vue'
import UserSystemDevices from './views/user/system-devices/SystemDevices.vue'
import UserFaq from './views/user/faq/Faq.vue'
import UserStaticSites from './views/user/static-sites/StaticSites.vue'
import UserProducts from './views/user/products/Products.vue'
import UserDeviceNames from './views/user/device-names/DeviceNames.vue'
import UserTranslations from './views/user/translations/Translations'

Vue.use(Router);

export default new Router({
    base: '/app/',
    routes: [
        {path: '/', name: 'dashboard', component: Dashboard},
        {path: '/login', name: 'login', component: Login},
        {path: '/register', name: 'register', component: Register},
        {path: '/user', name: 'user.main', component: UserContainer,
            children: [
                {path: '/profile', name: 'user.profile', component: UserProfile},
                {path: '/users', name: 'user.users', component: Users},
                {path: '/my-devices', name: 'user.my-devices', component: UserMyDevices},
                {path: '/system-devices', name: 'user.system-devices', component: UserSystemDevices},
                {path: '/faq', name: 'user.faq', component: UserFaq},
                {path: '/static-sites', name: 'user.static-sites', component: UserStaticSites},
                {path: '/products', name: 'user.products', component: UserProducts},
                {path: '/device-names', name: 'user.device-names', component: UserDeviceNames},
                {path: '/translations', name: 'user.translations', component: UserTranslations},
            ]
        },
    ]
})
