window.$ = window.jQuery = require('jquery');
require('bootstrap');
const fancybox = require('@fancyapps/fancybox');

$(document).ready(function(){
    $('.logout').click(function(){
        localStorage.removeItem('user');
        localStorage.removeItem('token');
    });
});
