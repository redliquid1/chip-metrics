import bus from "./bus";

require('jquery');
require('bootstrap');

import Vue from 'vue'
import User from './User/User.vue'

new Vue({
    render: h => h(User)
}).$mount('#user-container');


// require('spark');

/*'core': 'assets/js/core',
    'jquery': 'assets/js/vendors/jquery-3.2.1.min',
    'bootstrap': 'assets/js/vendors/bootstrap.bundle.min',
    'sparkline': 'assets/js/vendors/jquery.sparkline.min',
    'selectize': 'assets/js/vendors/selectize.min',
    'tablesorter': 'assets/js/vendors/jquery.tablesorter.min',
    'vector-map': 'assets/js/vendors/jquery-jvectormap-2.0.3.min',
    'vector-map-de': 'assets/js/vendors/jquery-jvectormap-de-merc',
    'vector-map-world': 'assets/js/vendors/jquery-jvectormap-world-mill',
    'circle-progress': 'assets/js/vendors/circle-progress.min',
    */
