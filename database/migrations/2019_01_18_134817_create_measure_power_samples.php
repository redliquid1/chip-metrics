<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMeasurePowerSamples extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('measure_power_samples', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->mediumInteger('gpio1')->default(0);
            $table->mediumInteger('gpio2')->default(0);
            $table->mediumInteger('gpio3')->default(0);
            $table->mediumInteger('gpio4')->default(0);
            $table->mediumInteger('gpio5')->default(0);
            $table->mediumInteger('gpio6')->default(0);
            $table->dateTime('time_added')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('measure_power_samples');
    }
}
