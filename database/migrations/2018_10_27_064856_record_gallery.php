<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RecordGallery extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('record_gallery', function (Blueprint $table) {
            $table->increments('id');
            $table->string('gallery_name');
            $table->string('model_name');
            $table->integer('model_id');
            $table->integer('sort')->default(0);
            $table->string('file_name');
            $table->string('title');
            $table->string('description');
            $table->text('data');
            $table->timestamps();

            $table->index(['gallery_name', 'model_name', 'model_id']);
            $table->index('sort');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('record_gallery');
    }
}
