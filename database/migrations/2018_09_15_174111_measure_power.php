<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MeasurePower extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('measure_power', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('device_id')->unsigned();
            $table->foreign('device_id')->references('id')->on('devices')->onDelete('cascade')->onUpdate('cascade');
            $table->mediumInteger('gpio1')->default(0);
            $table->mediumInteger('gpio2')->default(0);
            $table->mediumInteger('gpio3')->default(0);
            $table->mediumInteger('gpio4')->default(0);
            $table->mediumInteger('gpio5')->default(0);
            $table->mediumInteger('gpio6')->default(0);
            $table->dateTime('time_added')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('measure_power');
    }
}
