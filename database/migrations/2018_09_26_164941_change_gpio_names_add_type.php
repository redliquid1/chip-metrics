<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeGpioNamesAddType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('device_gpio_names', function (Blueprint $table) {
            $table->string('type')->default('power')->after('ppu');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('device_gpio_names', function (Blueprint $table) {
            $table->dropColumn('power');
        });
    }
}
