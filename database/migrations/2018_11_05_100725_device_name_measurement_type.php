<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeviceNameMeasurementType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('device_name_measurement_type', function (Blueprint $table) {
            $table->unsignedInteger('device_name_id');
            $table->unsignedInteger('measurement_type_id');

            $table->foreign('device_name_id')->references('id')->on('device_names')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('measurement_type_id')->references('id')->on('measurement_type')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('device_name_measurement_type');
    }
}
