<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeGpioNamesAddPpu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('device_gpio_names', function (Blueprint $table) {
            $table->string('ppu')->default('1000')->after('color');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('device_gpio_names', function (Blueprint $table) {
            $table->dropColumn('ppu');
        });
    }
}
