<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeGpioNamesAddTariff extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('device_gpio_names', function (Blueprint $table) {
            $table->string('tariff')->default('g11')->after('type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('device_gpio_names', function (Blueprint $table) {
            $table->dropColumn('tariff');
        });
    }
}
