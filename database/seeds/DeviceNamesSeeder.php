<?php

use App\Models\Device;
use App\Models\DeviceName;
use App\Models\User;
use Illuminate\Database\Seeder;

class DeviceNamesSeeder extends Seeder
{
    /**
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => 3,
                'identifier' => 'power_meter',
                'name' => 'Power meter'
            ],
            [
                'id' => 4,
                'identifier' => 'temperature_meter',
                'name' => 'Temperature meter (DS18B20)'
            ],
            [
                'id' => 5,
                'identifier' => 'temperature_humidity_meter',
                'name' => 'Temperature and humidity meter (DHT11/DHT22/AM2303)'
            ],
        ];

        foreach($data as $row) {
            $device = DeviceName::find($row['id']);
            if (!$device) {
                \DB::table('device_names')->insert($row);
            }
        }
    }
}
