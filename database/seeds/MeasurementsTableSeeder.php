<?php

use App\Models\User;
use App\Models\Device;
use App\Models\DeviceName;
use Illuminate\Database\Seeder;

class MeasurementsTableSeeder extends Seeder
{
    /** @var User */
    protected $user;

    /**
     * @return void
     */
    public function run()
    {
        $this->user = User::first();

        $this->createDevice($this->createDeviceName());
    }

    /**
     * Create device name.
     *
     * @return DeviceNames
     */
    protected function createDeviceName()
    {
        $data = new DeviceName([
            'identifier' => 'power_meter',
            'name' => 'Power meter',
        ]);
        $data->save();
        return $data;
    }

    /**
     * Create device.
     *
     * @param DeviceName $deviceName
     * @return \App\Models\Device
     */
    protected function createDevice($deviceName)
    {
        $data = new Device();
        $data->user_id = $this->user->id;
        $data->device_name_id = $deviceName->id;
        $data->chip_id = '12341234';
        return $data->saveOrFail();
    }

    /**
     * @return $this
     */
    protected function createAdminUser()
    {
        (new User([
            'name' => 'redliquid1',
            'email' => 'redliquid1@gmail.com',
            'password' => bcrypt('admin'),
        ]))->saveOrFail();

        return $this;
    }

}
