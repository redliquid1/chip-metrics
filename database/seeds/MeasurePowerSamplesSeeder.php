<?php

use App\Models\Device;
use App\Models\DeviceName;
use App\Models\User;
use Illuminate\Database\Seeder;

class MeasurePowerSamplesSeeder extends Seeder
{
    /**
     * @return void
     */
    public function run()
    {
        $startTime = \Carbon\Carbon::now()->subDays(500);
        $now = \Carbon\Carbon::now();

        $multipliers = [];
        for ($j = 1; $j < 8; $j++) {
            $multipliers[] = mt_rand() / mt_getrandmax();
        }

        $rows = 0;

        \DB::beginTransaction();
        while($startTime->timestamp < $now->timestamp) {
            $data = [];
            for ($j = 1; $j < 7; $j++) {
                $data['gpio'.$j] = (int)(rand(30, 50) * $multipliers[$j]);
            }
            $data['time_added'] = $startTime; // ->timestamp;

            $model = new \App\Models\MeasurePowerSample();
            $model->fill($data);
            $model->save();

            $startTime->addSecond(rand(50, 70));
            if ($rows++ > 10000) {
                $rows = 0;
                \DB::commit();
                \DB::beginTransaction();
            }
        }
        \DB::commit();

    }
}
