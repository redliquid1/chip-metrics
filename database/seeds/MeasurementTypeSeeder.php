<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class MeasurementTypeSeeder extends Seeder
{
    /**
     * @return void
     */
    public function run()
    {
        $data = [
            ['type' => 'kWh', 'title' => 'Pomiar prądu', 'active' => 1],
            ['type' => 'oC', 'title' => 'Pomiar temperatury', 'active' => 0],
            ['type' => 'Stan logiczny', 'title' => 'Pomiar stanu logicznego', 'active' => 0],
        ];

        foreach($data as $row) {
            \App\Models\MeasurementType::create($row);
        }
    }
}
