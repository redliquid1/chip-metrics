<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(MeasurementsTableSeeder::class);
        $this->call(MeasurementsPowerTableSeeder::class);
        $this->call(RolesAndPermissionsSeeder::class);
        $this->call(MeasurementTypeSeeder::class);
        // $this->call(DeviceNamesSeeder::class);
        $this->call(MeasurePowerSamplesSeeder::class);
    }
}
