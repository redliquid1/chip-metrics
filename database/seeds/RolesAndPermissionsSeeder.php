<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolesAndPermissionsSeeder extends Seeder
{
    public function run()
    {
        // Reset cached roles and permissions
        app()['cache']->forget('spatie.permission.cache');

        // create permissions
        Permission::create(['name' => 'show devices']);

        // create roles and assign created permissions

        Role::create(['name' => 'client']);
        // $role->givePermissionTo('edit articles');

        $role = Role::create(['name' => 'super-admin']);
        $role->givePermissionTo(Permission::all());

        $user = \App\Models\User::where('email', 'redliquid1@gmail.com')->first();
        $user->assignRole('super-admin');
    }
}