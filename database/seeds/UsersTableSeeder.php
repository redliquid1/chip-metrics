<?php

// @dummy

use App\Models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * @return void
     */
    public function run()
    {
        $this->createAdminUser();
    }

    /**
     * @return $this
     */
    protected function createAdminUser()
    {
        (new User([
            'name' => 'redliquid1',
            'email' => 'redliquid1@gmail.com',
            'password' => bcrypt('admin'),
        ]))->saveOrFail();

        return $this;
    }

}
