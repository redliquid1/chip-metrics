<?php

use App\Models\Device;
use App\Models\DeviceName;
use App\Models\User;
use Illuminate\Database\Seeder;

class MeasurementsPowerTableSeeder extends Seeder
{
    /** @var User */
    protected $user;

    /**
     * @return void
     */
    public function run()
    {
        $this->user = User::first();

        $deviceName = DeviceName::where('identifier', 'power_meter')->first();
        $device = Device::where('device_name_id', $deviceName->id)->first();

        $numberOfRecords = 10000;
        $startTime = \Carbon\Carbon::now()->subDays(75);

        $now = \Carbon\Carbon::now();
        for ($i = 0; $i < $numberOfRecords; $i++) {
            $data = [];
            for ($j = 1; $j < 7; $j++) {
                $data['gpio'.$j] = rand(30, 50);
            }
            $data['time_added'] = $startTime; // ->timestamp;

            $model = new \App\Models\MeasurePower();
            $model->fill($data);
            $model->user_id = $this->user->id;
            $model->device_id = $device->id;
            $model->save();

            $startTime->addMinutes(rand(10, 20));
            if ($startTime->gt($now)) {
                break;
            }
        }
    }

    /**
     * Create device name.
     *
     * @return DeviceNames
     */
    protected function createDeviceName()
    {
        $data = new DeviceName([
            'identifier' => 'power_meter',
            'name' => 'Power meter',
        ]);
        $data->save();
        return $data;
    }

    /**
     * Create device.
     *
     * @param DeviceName $deviceName
     * @return \App\Models\Device
     */
    protected function createDevice($deviceName)
    {
        $data = new Device();
        $data->user_id = $this->user->id;
        $data->device_name_id = $deviceName->id;
        $data->chip_id = '12341234';
        return $data->saveOrFail();
    }

    /**
     * @return $this
     */
    protected function createAdminUser()
    {
        (new User([
            'name' => 'redliquid1',
            'email' => 'redliquid1@gmail.com',
            'password' => bcrypt('admin'),
        ]))->saveOrFail();

        return $this;
    }

}
